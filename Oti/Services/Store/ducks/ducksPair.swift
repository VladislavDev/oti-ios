//
//  ducksPair.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// MARK: - Actions
// Get pair actions
struct StartGetPairListAction: Action {}
struct SuccessGetPairListAction: Action { var payload: PSMessage? }
struct ErrorGetPairListAction: Action {}
func getPairListRequestAction(cb: (([PSGroupsDatas]?) -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            dispatch(ErrorGetPairListAction())
            return
        }
        
        PairSettingsRequests.getPairListRequest(
            userId: userId,
            cbWhenStartRequest: { dispatch(StartGetPairListAction()) },
            cbWhenErrorRequest: { dispatch(ErrorGetPairListAction()) },
            cbWhenSuccessRequest: { data in
                dispatch(SuccessGetPairListAction(payload: data))
                
                guard let cb = cb else { return }
                cb(data?.resolveRequest)
            })
    }
}

// Add new pair
struct StartAddNewPairAction: Action {}
struct SuccessAddNewPairAction: Action {}
struct ErrorAddNewPairAction: Action {}
func addNewPairRequestAction(recipientEmail: String, cb: (() -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            dispatch(ErrorAddNewPairAction())
            return
        }
        
        PairSettingsRequests.addNewPairRequest(
            senderId: userId,
            recipientEmail: recipientEmail,
            cbWhenStartRequest: { dispatch(StartAddNewPairAction()) },
            cbWhenErrorRequest: { dispatch(ErrorAddNewPairAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessAddNewPairAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// Resolve group
struct StartResolveGroupAction: Action {}
struct SuccessResolveGroupAction: Action {}
struct ErrorResolveGroupAction: Action {}
func resolveGroupRequestAction(groupId: Int, cb: (() -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        PairSettingsRequests.resolveGroupRequest(
            groupId: groupId,
            cbWhenStartRequest: { dispatch(StartResolveGroupAction()) },
            cbWhenErrorRequest: { dispatch(ErrorResolveGroupAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessResolveGroupAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// Delete group
struct StartDeleteGroupAction: Action {}
struct SuccessDeleteGroupAction: Action {}
struct ErrorDeleteGroupAction: Action {}
func deleteGroupRequestAction(groupId: Int, cb: (() -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        PairSettingsRequests.deleteGroupRequest(
            groupId: groupId,
            cbWhenStartRequest: { dispatch(StartDeleteGroupAction()) },
            cbWhenErrorRequest: { dispatch(ErrorDeleteGroupAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessDeleteGroupAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// MARK: - State
struct PairState: StateType {
    // Pair list
    var pairList = PSMessage(
        resolveRequest: [],
        senderReuqest: [],
        recipientsRequests: []
    )
    
    // Get pairs list
    var isStartGetPairList = false
    var isSuccessGetPairList = false
    var isErrorGetPairList = false
    
    // Add new pair
    var isStartAddingNewPair = false
    var isSuccessAddingNewPair = false
    var isErrorAddingNewPair = false
    
    // Resolve group
    var isStartResolveGroup = false
    var isSuccessResolveGroup = false
    var isErrorResolveGroup = false
    
    // Delete group
    var isStartDeleteGroup = false
    var isSuccessDeleteGroup = false
    var isErrorDeleteGroup = false
}

// MARK: - Reducer
func pairReducer(action: Action, state: PairState?) -> PairState {
    var state = state ?? PairState()
    
    switch action {
    // Get pair
    case _ as StartGetPairListAction:
        state.isStartGetPairList = true
        state.isSuccessGetPairList = false
        state.isErrorGetPairList = false
        
    case let action as SuccessGetPairListAction:
        state.isStartGetPairList = false
        state.isSuccessGetPairList = true
        state.isErrorGetPairList = false
        state.pairList = action.payload ?? PSMessage(
            resolveRequest: [],
            senderReuqest: [],
            recipientsRequests: []
        )
        
    case _ as ErrorGetPairListAction:
        state.isStartGetPairList = false
        state.isSuccessGetPairList = false
        state.isErrorGetPairList = true
        state.pairList = PSMessage(
            resolveRequest: [],
            senderReuqest: [],
            recipientsRequests: []
        )
        
    // Add pair
    case _ as StartAddNewPairAction:
        state.isStartAddingNewPair = true
        state.isSuccessAddingNewPair = false
        state.isErrorAddingNewPair = false
        
    case _ as SuccessAddNewPairAction:
        state.isStartAddingNewPair = false
        state.isSuccessAddingNewPair = true
        state.isErrorAddingNewPair = false
        
    case _ as ErrorAddNewPairAction:
        state.isStartAddingNewPair = false
        state.isSuccessAddingNewPair = false
        state.isErrorAddingNewPair = true
        
    // Resolve group
    case _ as StartResolveGroupAction:
        state.isStartResolveGroup = true
        state.isSuccessResolveGroup = false
        state.isErrorResolveGroup = false
        
    case _ as SuccessResolveGroupAction:
        state.isStartResolveGroup = false
        state.isSuccessResolveGroup = true
        state.isErrorResolveGroup = false
        
    case _ as ErrorResolveGroupAction:
        state.isStartResolveGroup = false
        state.isSuccessResolveGroup = false
        state.isErrorResolveGroup = true
        
    // Delete group
    case _ as StartDeleteGroupAction:
        state.isStartDeleteGroup = true
        state.isSuccessDeleteGroup = false
        state.isErrorDeleteGroup = false
        
    case _ as SuccessDeleteGroupAction:
        state.isStartDeleteGroup = false
        state.isSuccessDeleteGroup = true
        state.isErrorDeleteGroup = false
        
    case _ as ErrorDeleteGroupAction:
        state.isStartDeleteGroup = false
        state.isSuccessDeleteGroup = false
        state.isErrorDeleteGroup = true
        
        
    default: break
    }
    
    return state
}
