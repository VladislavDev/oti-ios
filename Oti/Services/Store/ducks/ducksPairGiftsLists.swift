//
//  ducksPairGiftsLists.swift
//  Oti
//
//  Created by Vlad on 9/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

import ReSwift
import ReSwiftThunk

// MARK: - Actions
struct StartGetPairGiftsListsAction: Action {}
struct SuccessGetPairGiftsListsAction: Action {
    var type: GiftType
    var payload: [GiftItemProtocol?]
}
struct ErrorGetPairGiftsListsAction: Action {}
func getPairGiftsListsRequestAction(cb: (() -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard
            let userId = TokenService.shared.getAuthUserData().userId,
            let resolveRequest = getState()?.pairState.pairList.resolveRequest,
            resolveRequest.count > 0
        else { return }
        
        let senderId = resolveRequest[0].sender
        let recipientId = resolveRequest[0].recipient
        
        // Choosing the correct ID pair,
        // because the sender could be the current user
        var pairId: Int? = nil
        
        if Int(userId) == senderId {
            pairId = recipientId
        } else {
            pairId = senderId
        }
        
        guard let unwrapPairId = pairId else { return }
        let pairIdStr = String(unwrapPairId)
        
        // Get gifts and unwanted gifts lists
        GiftsListsRequests.getGiftsListRequest(
            userId: pairIdStr,
            modelType: DesiredGiftsListResponse.self,
            url: URLs.getDesiredGiftsList,
            cbWhenStartRequest: { dispatch(StartGetPairGiftsListsAction()) },
            cbWhenErrorRequest: { dispatch(ErrorGetPairGiftsListsAction()) },
            cbWhenSuccessRequest: { desiredResponse in
                
                GiftsListsRequests.getGiftsListRequest(
                    userId: pairIdStr,
                    modelType: UnwantedGiftsListResponse.self,
                    url: URLs.getUnwantedGiftsList,
                    cbWhenStartRequest: nil,
                    cbWhenErrorRequest: { dispatch(ErrorGetPairGiftsListsAction()) },
                    cbWhenSuccessRequest: { unwantedResponse in
                        dispatch(SuccessGetPairGiftsListsAction(
                            type: .desired,
                            payload: desiredResponse.message)
                        )
                        dispatch(SuccessGetPairGiftsListsAction(
                            type: .unwanted,
                            payload: unwantedResponse.message)
                        )
                        
                        guard let cb = cb else { return }
                        cb()
                    }
                )
            }
        )
    }
}

// MARK: - State
struct PairGiftsListsState: StateType {
    var desiredPairGifstList: [DesiredGiftItem] = []
    var unwantedPairGifstLists: [UnwantedGiftItem] = []
    
    var isStartGetPairGiftsLists = false
    var isSuccessGetPairGiftsListsAction = false
    var isErrorGetPairGiftsListsAction = false
}

// MARK: - Reducer
func pairGiftsReducer(action: Action, state: PairGiftsListsState?) -> PairGiftsListsState  {
    var state = state ?? PairGiftsListsState()

    switch action {
    // Get pair lists gifts
    case _ as StartGetPairGiftsListsAction:
        state.isStartGetPairGiftsLists = true
        state.isSuccessGetPairGiftsListsAction = false
        state.isErrorGetPairGiftsListsAction = false
        
    case let action as SuccessGetPairGiftsListsAction:
        state.isStartGetPairGiftsLists = false
        state.isSuccessGetPairGiftsListsAction = true
        state.isErrorGetPairGiftsListsAction = false
        
        let desiredPairGiftsList = action.payload as? [DesiredGiftItem]
        if
            let unwrapedData = desiredPairGiftsList,
            action.type == .desired {
            state.desiredPairGifstList = unwrapedData
        }
        
        let unwantedPairGiftsList = action.payload as? [UnwantedGiftItem]
        if
            let unwrapedData = unwantedPairGiftsList,
            action.type == .unwanted {
            state.unwantedPairGifstLists = unwrapedData
        }
        
    case _ as ErrorGetPairGiftsListsAction:
        state.isStartGetPairGiftsLists = false
        state.isSuccessGetPairGiftsListsAction = false
        state.isErrorGetPairGiftsListsAction = true
    
    default: break
    }
    
    return state
}
