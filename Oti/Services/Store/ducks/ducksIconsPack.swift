//
//  ducksIconsPack.swift
//  Oti
//
//  Created by Vlad on 9/24/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// MARK: - Actions
struct StartGetIconsPackAction: Action {}
struct SuccessGetIconsPackAction: Action { var payload: [String] }
struct ErrorGetIconsPackAction: Action {}

func getIconsPackRequestAction(
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        CreateNewEventRequests.getIconPack(
            cbWhenStartRequest: { dispatch(StartGetIconsPackAction()) },
            cbWhenErrorRequest: { dispatch(ErrorGetIconsPackAction()) },
            cbWhenSuccessRequest: { iconsNames in
                dispatch(SuccessGetIconsPackAction(payload: iconsNames))
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// MARK: - State
struct IconsPackState: StateType {
    var iconsNamesList: [String] = []
    
    var isStartGettingIconsNames = false
    var isSuccessGettingIconsNames = false
    var isErrorGettingIconsNames = false
}

// MARK: - Reducer
func iconsPackReducer(action: Action, state: IconsPackState?) -> IconsPackState  {
    var state = state ?? IconsPackState()
    
    switch action {
    case _ as StartGetIconsPackAction:
        state.isStartGettingIconsNames = true
        state.isSuccessGettingIconsNames = false
        state.isErrorGettingIconsNames = false
        
        state.iconsNamesList = []
        
    case let action as SuccessGetIconsPackAction:
        state.isStartGettingIconsNames = false
        state.isSuccessGettingIconsNames = true
        state.isErrorGettingIconsNames = false
        
        state.iconsNamesList = action.payload
        
    case _ as ErrorGetIconsPackAction:
        state.isStartGettingIconsNames = false
        state.isSuccessGettingIconsNames = false
        state.isErrorGettingIconsNames = true
        
    default: break
    }
    
    return state
}
