//
//  ducksGifts.swift
//  Oti
//
//  Created by Vlad on 9/3/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// MARK: - Actions
struct StartGetGiftsListsAction: Action {}
struct SuccessGetGiftsListsAction: Action {
    var type: GiftType
    var payload: [GiftItemProtocol?]
}
struct ErrorGetGiftsListsAction: Action {}
func getGiftsListsRequestAction(cb: (() -> ())? = nil) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            dispatch(ErrorGetGiftsListsAction())
            return
        }
        
        GiftsListsRequests.getGiftsListRequest(
            userId: userId,
            modelType: DesiredGiftsListResponse.self,
            url: URLs.getDesiredGiftsList,
            cbWhenStartRequest: { dispatch(StartGetGiftsListsAction()) },
            cbWhenErrorRequest: { dispatch(ErrorGetGiftsListsAction()) },
            cbWhenSuccessRequest: { desiredResponse in
                
                GiftsListsRequests.getGiftsListRequest(
                    userId: userId,
                    modelType: UnwantedGiftsListResponse.self,
                    url: URLs.getUnwantedGiftsList,
                    cbWhenStartRequest: nil,
                    cbWhenErrorRequest: { dispatch(ErrorGetGiftsListsAction()) },
                    cbWhenSuccessRequest: { unwantedResponse in
                        dispatch(SuccessGetGiftsListsAction(
                            type: .desired,
                            payload: desiredResponse.message)
                        )
                        dispatch(SuccessGetGiftsListsAction(
                            type: .unwanted,
                            payload: unwantedResponse.message)
                        )
                        
                        guard let cb = cb else { return }
                        cb()
                    }
                )
            }
        )
    }
}

struct StartAddingGiftAction: Action {}
struct SuccessAddingGiftAction: Action {}
struct ErrorAddingGiftsAction: Action {}
func addingGiftRequestAction(
    giftType: GiftType,
    title: String,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        GiftsListsRequests.addGiftRequest(
            giftType: giftType,
            title: title,
            cbWhenStartRequest: { dispatch(StartAddingGiftAction()) },
            cbWhenErrorRequest: { dispatch(ErrorAddingGiftsAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessAddingGiftAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

struct StartDeletionGiftAction: Action {}
struct SuccessDeletionGiftAction: Action {}
struct ErrorDeletionGiftsAction: Action {}
func deletionGiftRequestAction(
    giftType: GiftType,
    giftId: Int,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        GiftsListsRequests.deletionGiftRequest(
            giftType: giftType,
            giftId: giftId,
            cbWhenStartRequest: { dispatch(StartDeletionGiftAction()) },
            cbWhenErrorRequest: { dispatch(ErrorDeletionGiftsAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessDeletionGiftAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// MARK: - State
struct GiftsListsState: StateType {
    var desiredGifstList: [DesiredGiftItem] = []
    var unwantedGifstLists: [UnwantedGiftItem] = []
    
    var isStartGetGiftsLists = false
    var isSuccessGetGiftsListsAction = false
    var isErrorGetGiftsListsAction = false
    
    var isStartAddingGiftAction = false
    var isSuccessAddingGiftAction = false
    var isErrorAddingGiftsAction = false
    
    var isStartDeletionGiftAction = false
    var isSuccessDeletionGiftAction = false
    var isErrorDeletionGiftsAction = false
}

// MARK: - Reducer
func giftsReducer(action: Action, state: GiftsListsState?) -> GiftsListsState  {
    var state = state ?? GiftsListsState()
    
    switch action {
    // Get lists gifts
    case _ as StartGetGiftsListsAction:
        state.isStartGetGiftsLists = true
        state.isSuccessGetGiftsListsAction = false
        state.isErrorGetGiftsListsAction = false
        
    case let action as SuccessGetGiftsListsAction:
        state.isStartGetGiftsLists = false
        state.isSuccessGetGiftsListsAction = true
        state.isErrorGetGiftsListsAction = false
        
        let desiredGiftsList = action.payload as? [DesiredGiftItem]
        if
            let unwrapedData = desiredGiftsList,
            action.type == .desired {
            state.desiredGifstList = unwrapedData
        }
        
        let unwantedGiftsList = action.payload as? [UnwantedGiftItem]
        if
            let unwrapedData = unwantedGiftsList,
            action.type == .unwanted {
            state.unwantedGifstLists = unwrapedData
        }
        
    case _ as ErrorGetGiftsListsAction:
        state.isStartGetGiftsLists = false
        state.isSuccessGetGiftsListsAction = false
        state.isErrorGetGiftsListsAction = true
        
    // Adding gift
    case _ as StartAddingGiftAction:
        state.isStartAddingGiftAction = true
        state.isSuccessAddingGiftAction = false
        state.isErrorAddingGiftsAction = false
        
    case _ as SuccessAddingGiftAction:
        state.isStartAddingGiftAction = false
        state.isSuccessAddingGiftAction = true
        state.isErrorAddingGiftsAction = false
        
    case _ as ErrorAddingGiftsAction:
        state.isStartAddingGiftAction = false
        state.isSuccessAddingGiftAction = false
        state.isErrorAddingGiftsAction = true
    
    // Deletion gift
    case _ as StartDeletionGiftAction:
        state.isStartDeletionGiftAction = true
        state.isSuccessDeletionGiftAction = false
        state.isErrorDeletionGiftsAction = false
        
    case _ as SuccessDeletionGiftAction:
        state.isStartDeletionGiftAction = false
        state.isSuccessDeletionGiftAction = true
        state.isErrorDeletionGiftsAction = false
        
    case _ as ErrorDeletionGiftsAction:
        state.isStartDeletionGiftAction = false
        state.isSuccessDeletionGiftAction = false
        state.isErrorDeletionGiftsAction = true
    
    default: break
    }
    
    return state
}
