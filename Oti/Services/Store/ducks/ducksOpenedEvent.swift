//
//  ducksOpenedEvent.swift
//  Oti
//
//  Created by Vlad on 9/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

import ReSwift
import ReSwiftThunk

struct OpenedEvent {
    var month: String
    var day: String
    var rawEventValues: Event?
}

// MARK: - Actions
struct SetDataOpenedEventAction: Action {
    var payload: OpenedEvent
}

// MARK: - State
struct OpenedEventState: StateType {
    var openedEvent: OpenedEvent?
}

// MARK: - Reducer
func openedEventReducer(action: Action, state: OpenedEventState?) -> OpenedEventState  {
    var state = state ?? OpenedEventState()
    
    switch action {
    case let action as SetDataOpenedEventAction:
        state.openedEvent = action.payload

    default: break
    }
    
    return state
}
