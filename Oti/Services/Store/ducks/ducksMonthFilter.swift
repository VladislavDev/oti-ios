//
//  ducksMonthFilter.swift
//  Oti
//
//  Created by Vlad on 9/15/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

import ReSwift
import ReSwiftThunk

// MARK: - Actions
struct SetNewValueIntoMonthFilterAction: Action {
    var year: String
    var monthIdx: String
    var month: String
}

// MARK: - State
struct MonthFilterState: StateType {
    var year: String = WorkWithDate.getFormettedDate(format: .year)
    var monthIdx: String = WorkWithDate.getFormettedDate(format: .monthCount)
    var month: String = WorkWithDate.getCurrentLimitedMonth()
}

// MARK: - Reducer
func monthFilterReducer(action: Action, state: MonthFilterState?) -> MonthFilterState  {
    var state = state ?? MonthFilterState()

    switch action {
    // Get pair lists gifts
    case let action as SetNewValueIntoMonthFilterAction:
        state.year = action.year
        state.monthIdx = action.monthIdx
        state.month = TextAttributes.limitingMonth(month: action.month.lowercased().capitalized)
    
    default: break
    }
    
    return state
}
