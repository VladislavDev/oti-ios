//
//  ducksEvents.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// MARK: - Actions
struct StartGetEventsListAction: Action {}
struct SuccessGetEventsListAction: Action { var payload: [Event] }
struct ErrorGetEventsListAction: Action {}
func getEventsListRequestAction(
    year: String,
    month: String,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        EventsListRequests.getEventsListsRequest(
            year: year,
            month: month,
            cbWhenStartRequest: { dispatch(StartGetEventsListAction()) },
            cbWhenErrorRequest: { dispatch(ErrorGetEventsListAction()) },
            cbWhenSuccessRequest: { eventsList in
                dispatch(SuccessGetEventsListAction(payload: eventsList))
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

struct StartAddEventAction: Action {}
struct SuccessAddEventAction: Action {}
struct ErrorAddEventAction: Action {}
func addEventRequestAction(
    date: String,
    title: String,
    watching: String,
    icon: String,
    reminder: String,
    notify: String,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        CreateNewEventRequests.createNewEventRequest(
            date: date,
            title: title,
            watching: watching,
            icon: icon,
            reminder: reminder,
            notify: notify,
            cbWhenStartRequest: { dispatch(StartAddEventAction()) },
            cbWhenErrorRequest: { dispatch(ErrorAddEventAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessAddEventAction())

                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

struct StartChangeEventAction: Action {}
struct SuccessChangeEventAction: Action {}
struct ErrorChangeEventAction: Action {}
func changeEventRequestAction(
    eventId: Int,
    date: String,
    title: String,
    watching: String,
    icon: String,
    reminder: String,
    notify: String,
    notifyId: String,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        ChangeEventRequests.changeEventRequest(
            eventId: eventId,
            date: date,
            title: title,
            watching: watching,
            icon: icon,
            reminder: reminder,
            notify: notify,
            notifyId: notifyId,
            cbWhenStartRequest: { dispatch(StartChangeEventAction()) },
            cbWhenErrorRequest: { dispatch(ErrorChangeEventAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessChangeEventAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

struct StartDeleteEventAction: Action {}
struct SuccessDeleteEventAction: Action {}
struct ErrorDeleteEventAction: Action {}
func deleteEventRequestAction(
    eventId: Int,
    notifyId: String?,
    cb: (() -> ())? = nil
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        ChangeEventRequests.deleteEventRequest(
            eventId: eventId,
            notifyId: notifyId,
            cbWhenStartRequest: { dispatch(StartDeleteEventAction()) },
            cbWhenErrorRequest: { dispatch(ErrorDeleteEventAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessDeleteEventAction())
                
                guard let cb = cb else { return }
                cb()
            }
        )
    }
}

// MARK: - State
struct EventsListsState: StateType {
    var todayEventsList: [Event] = []
    var nextEventsList: [Event] = []
    var prevEventsList: [Event] = []
    
    var isStartGettingEventsList = false
    var isSuccessGettingEventsList = false
    var isErrorGettingEventsList = false
    
    var isStartAddingEvent = false
    var isSuccessAddingEvent = false
    var isErrorAddingEvent = false
    
    var isStartChangeEvent = false
    var isSuccessChangeEvent = false
    var isErrorChangeEvent = false
    
    var isStartDeletionEvent = false
    var isSuccessDeletionEvent = false
    var isErrorDeletionEvent = false
}

// MARK: - Reducer
func eventsReducer(action: Action, state: EventsListsState?) -> EventsListsState  {
    var state = state ?? EventsListsState()
    
    switch action {
    // Getting events lists
    case _ as StartGetEventsListAction:
        state.isStartGettingEventsList = true
        state.isSuccessGettingEventsList = false
        state.isErrorGettingEventsList = false
        
        state.todayEventsList = []
        state.nextEventsList = []
        state.prevEventsList = []
        
    case let action as SuccessGetEventsListAction:
        state.isStartGettingEventsList = false
        state.isSuccessGettingEventsList = true
        state.isErrorGettingEventsList = false
        
        let eventsList = action.payload
        
        // Current date string
        let currentDateStr = WorkWithDate.getFormettedDate(
            format: .fullFromBackend,
            date: WorkWithDate.getCurrentDate()
        )
        
        // Normalize data
        for event in eventsList {
            // Parse date from backend iso string
            if let isoDateFromEvent = WorkWithDate.transformStrDateToDate(dateStr: event.dateEvent) {
                // Transform iso date to yyy-MM-dd string
                let eventDateStr = WorkWithDate.getFormettedDate(
                    format: .fullFromBackend,
                    date: isoDateFromEvent
                )
                
                let eventDateStrArr = eventDateStr.split(separator: "-")
                let currentDateStrArr = currentDateStr.split(separator: "-")
                
                let eventDateDayStr = eventDateStrArr[2]
                let currentDateDayStr = currentDateStrArr[2]
                
                // Append today events
                if eventDateDayStr == currentDateDayStr {
                    state.todayEventsList.append(event)
                    
                // Append next events
                } else if eventDateDayStr > currentDateDayStr {
                    state.nextEventsList.append(event)
                    
                // Append prev events
                } else if eventDateDayStr < currentDateDayStr {
                    state.prevEventsList.append(event)
                }
            }
        }
        
    case _ as ErrorGetEventsListAction:
        state.isStartGettingEventsList = false
        state.isSuccessGettingEventsList = false
        state.isErrorGettingEventsList = true
        
    // Adding event
    case _ as StartAddEventAction:
        state.isStartAddingEvent = true
        state.isSuccessAddingEvent = false
        state.isErrorAddingEvent = false
        
    case _ as SuccessAddEventAction:
        state.isStartAddingEvent = false
        state.isSuccessAddingEvent = true
        state.isErrorAddingEvent = false
        
    case _ as ErrorAddEventAction:
        state.isStartAddingEvent = false
        state.isSuccessAddingEvent = false
        state.isErrorAddingEvent = true
        
    // Change event
    case _ as StartChangeEventAction:
        state.isStartChangeEvent = true
        state.isSuccessChangeEvent = false
        state.isErrorChangeEvent = false
        
    case _ as SuccessChangeEventAction:
        state.isStartChangeEvent = false
        state.isSuccessChangeEvent = true
        state.isErrorChangeEvent = false
        
    case _ as ErrorChangeEventAction:
        state.isStartChangeEvent = false
        state.isSuccessChangeEvent = false
        state.isErrorChangeEvent = true
        
    // Deletion event
    case _ as StartDeleteEventAction:
        state.isStartDeletionEvent = true
        state.isSuccessDeletionEvent = false
        state.isErrorDeletionEvent = false
        
    case _ as SuccessDeleteEventAction:
        state.isStartDeletionEvent = false
        state.isSuccessDeletionEvent = true
        state.isErrorDeletionEvent = false
        
    case _ as ErrorDeleteEventAction:
        state.isStartDeletionEvent = false
        state.isSuccessDeletionEvent = false
        state.isErrorDeletionEvent = true
        
    default: break
    }
    
    return state
}
