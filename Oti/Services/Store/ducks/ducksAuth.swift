//
//  ducksAuth.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// MARK: - Actions
// Login actions
struct StartLoginAction: Action {}
struct SuccessLoginAction: Action {}
struct ErrorLoginAction: Action {}
func loginRequestAction(email: String, pass: String) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        LoginRequests.loginRequest(
            email: email,
            pass: pass,
            cbWhenStartRequest: { dispatch(StartLoginAction()) },
            cbWhenErrorRequest: { dispatch(ErrorLoginAction()) },
            cbWhenSuccessRequest: { _ in
                dispatch(SuccessLoginAction())
                
                UIApplication.setRootView(ModuleBuilder.createAppAfterLogin())
            }
        )
    }
}

// Create acc actions
struct StartCreateAccAction: Action {}
struct SuccessCreateAccAction: Action {}
struct ErrorCreateAccAction: Action {}
func createAccRequestAction(email: String, pass: String) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        // Create acc
        CreateAccRequests.createAccRequest(
            email: email,
            pass: pass,
            cbWhenStartRequest: { dispatch(StartCreateAccAction()) },
            cbWhenErrorRequest: { dispatch(ErrorCreateAccAction()) },
            cbWhenSuccessRequest: {
                // Login
                LoginRequests.loginRequest(
                    email: email,
                    pass: pass,
                    cbWhenStartRequest: nil,
                    cbWhenErrorRequest: { dispatch(ErrorCreateAccAction()) },
                    cbWhenSuccessRequest: {_ in
                        dispatch(SuccessCreateAccAction())
                        UIApplication.self.setRootView(ModuleBuilder.createAppAfterLogin())
                    }
                )
            }
        )
    }
}

// Forgot pass actions
struct StartReqForgotPassAction: Action {}
struct SuccessReqForgotPassAction: Action {}
struct ErrorReqForgotPassAction: Action {}
func forgotPassRequestAction(email: String, cb: @escaping () -> ()) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        ForgotPassRequests.forgotPassRequest(
            email: email,
            cbWhenStartRequest: { dispatch(StartReqForgotPassAction()) },
            cbWhenErrorRequest: { dispatch(ErrorReqForgotPassAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessReqForgotPassAction())
                cb()
            }
        )
    }
}

// Change pass actions
struct StartReqChangePassAction: Action {}
struct SuccessChangePassAction: Action {}
struct ErrorChangePassAction: Action {}
func changePassAction(
    userId: String,
    oldPass: String,
    newPass: String,
    cb: @escaping () -> ()
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        ChangePassRequests.changePassRequest(
            userId: userId,
            oldPass: oldPass,
            newPass: newPass,
            cbWhenStartRequest: { dispatch(StartReqChangePassAction()) },
            cbWhenErrorRequest: { dispatch(ErrorChangePassAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessChangePassAction())
                cb()
            }
        )
    }
}

// Email us actions
struct StartReqEmailUsAction: Action {}
struct SuccessReqEmailUsAction: Action {}
struct ErrorReqEmailUsAction: Action {}
func emailUsReqAction(
    subject: String,
    text: String,
    cb: @escaping () -> ()
) -> Thunk<AppState> {
    return Thunk<AppState> { dispatch, getState in
        guard let userEmail = TokenService.shared.getAuthUserData().email else {
            dispatch(ErrorReqEmailUsAction())
            return
        }
        
        EmailUsRequests.emailUsRequest(
            from: userEmail,
            subject: subject,
            text: text,
            cbWhenStartRequest: { dispatch(StartReqEmailUsAction()) },
            cbWhenErrorRequest: { dispatch(ErrorReqEmailUsAction()) },
            cbWhenSuccessRequest: {
                dispatch(SuccessReqEmailUsAction())
                cb()
            }
        )
    }
}

// MARK: - State
struct AuthState: StateType {
    var isStartLoginRequest = false
    var isSuccessLoginRequest = false
    var isErrorLoginRequest = false
    
    var isStartCreateAccRequest = false
    var isSuccessCreateAccRequest = false
    var isErrorCreateAccRequest = false
    
    var isStartForgotPassRequest = false
    var isSuccessForgotPassRequest = false
    var isErrorForgotPassRequest = false
    
    var isStartChangePassRequest = false
    var isSuccessChangePassRequest = false
    var isErrorChangePassRequest = false
    
    var isStartEmailUsRequest = false
    var isSuccessEmailUsRequest = false
    var isErrorEmailUsRequest = false
}

// MARK: - Reducer
func authReducer(action: Action, state: AuthState?) -> AuthState {
    var state = state ?? AuthState()
    
    switch action {
    // Login
    case _ as StartLoginAction:
        state.isStartLoginRequest = true
        state.isSuccessLoginRequest = false
        state.isErrorLoginRequest = false
        
    case _ as SuccessLoginAction:
        state.isStartLoginRequest = false
        state.isSuccessLoginRequest = true
        state.isErrorLoginRequest = false
        
    case _ as ErrorLoginAction:
        state.isStartLoginRequest = false
        state.isSuccessLoginRequest = false
        state.isErrorLoginRequest = true
        
    // Create acc
    case _ as StartCreateAccAction:
        state.isStartCreateAccRequest = true
        state.isSuccessCreateAccRequest = false
        state.isErrorCreateAccRequest = false
        
    case _ as SuccessCreateAccAction:
        state.isStartCreateAccRequest = false
        state.isSuccessCreateAccRequest = true
        state.isErrorCreateAccRequest = false
        
    case _ as ErrorCreateAccAction:
        state.isStartCreateAccRequest = false
        state.isSuccessCreateAccRequest = false
        state.isErrorCreateAccRequest = true
        
    // Forgot pass
    case _ as StartReqForgotPassAction:
        state.isStartForgotPassRequest = true
        state.isSuccessForgotPassRequest = false
        state.isErrorForgotPassRequest = false
        
    case _ as SuccessReqForgotPassAction:
        state.isStartForgotPassRequest = false
        state.isSuccessForgotPassRequest = true
        state.isErrorForgotPassRequest = false
        
    case _ as ErrorReqForgotPassAction:
        state.isStartForgotPassRequest = false
        state.isSuccessForgotPassRequest = false
        state.isErrorForgotPassRequest = true
        
    // Change pass
    case _ as StartReqChangePassAction:
        state.isStartChangePassRequest = true
        state.isSuccessChangePassRequest = false
        state.isErrorChangePassRequest = false
        
    case _ as SuccessChangePassAction:
        state.isStartChangePassRequest = false
        state.isSuccessChangePassRequest = true
        state.isErrorChangePassRequest = false
        
    case _ as ErrorChangePassAction:
        state.isStartChangePassRequest = false
        state.isSuccessChangePassRequest = false
        state.isErrorChangePassRequest = true
        
    // Email us
    case _ as StartReqEmailUsAction:
        state.isStartEmailUsRequest = true
        state.isSuccessEmailUsRequest = false
        state.isErrorEmailUsRequest = false
        
    case _ as SuccessReqEmailUsAction:
        state.isStartEmailUsRequest = false
        state.isSuccessEmailUsRequest = true
        state.isErrorEmailUsRequest = false
        
    case _ as ErrorReqEmailUsAction:
        state.isStartEmailUsRequest = false
        state.isSuccessEmailUsRequest = false
        state.isErrorEmailUsRequest = true
        
    default: break
    }
    
    return state
}
