//
//  Middlewares.swift
//  Oti
//
//  Created by Vlad on 8/28/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift
import ReSwiftThunk

// Thunk
let thunkMiddleware: Middleware<AppState> = createThunkMiddleware()

// Print to console
let logMiddleware: Middleware<AppState> = { dispatch, getState in
    return { next in
        return { action in
            print("--- RESWIFT ---")
            print("")
            print("Dispatching - \(action)")
            
            let result: Void = next(action)
            
            guard let state = getState() else { return next(action) }
            
            print("Next state: \(state)")
            print("")
            
            return result
        }
    }
}
