//
//  Store.swift
//  Oti
//
//  Created by Vlad on 8/28/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import ReSwift

// Global state
struct AppState: StateType {
    var authState: AuthState
    var pairState: PairState
    var giftsListsState: GiftsListsState
    var eventsListsState: EventsListsState
    var pairGiftsListsState: PairGiftsListsState
    var monthFilterState: MonthFilterState
    var openedEventState: OpenedEventState
    var iconsPackNames: IconsPackState
}

// Global reducer
fileprivate func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        authState: authReducer(action: action, state: state?.authState),
        pairState: pairReducer(action: action, state: state?.pairState),
        giftsListsState: giftsReducer(action: action, state: state?.giftsListsState),
        eventsListsState: eventsReducer(action: action, state: state?.eventsListsState),
        pairGiftsListsState: pairGiftsReducer(action: action, state: state?.pairGiftsListsState),
        monthFilterState: monthFilterReducer(action: action, state: state?.monthFilterState),
        openedEventState: openedEventReducer(action: action, state: state?.openedEventState),
        iconsPackNames: iconsPackReducer(action: action, state: state?.iconsPackNames)
    )
}

// Store
let store = Store(
    reducer: appReducer,
    state: nil,
    middleware: [thunkMiddleware], // logMiddleware
    automaticallySkipsRepeats: true
)
