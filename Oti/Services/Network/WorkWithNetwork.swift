//
//  WorkWithNetwork.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

enum RequestType: String {
    case GET, POST, PUT, DELETE
}

class WorkWithNetwork {
    
    // Singleton
    static let shared = WorkWithNetwork()
    private init() {}
    
    // MARK: - Work winh session
    private func session(url: URLRequest, cb: @escaping cbDataWithErrorTypeAliace) {        
        if CheckNWConnection.getConnectionStatus() == .unsatisfied {
            Snackbar.shared.showSnackbar(
                title: NSLocalizedString(
                    "Потеряно соединение",
                    comment: "Потеряно соединение"
                ),
                type: .warning
            )

            cb(nil, true)
            return
        }
        
        let config = URLSessionConfiguration.default
        config.waitsForConnectivity = true
        
        let s = URLSession.init(configuration: config)
        
        s.dataTask(with: url) { (data, _, error) in
//            print("ONE: ", String(decoding: data!, as: UTF8.self), error)
            
            let errAnotherApi = WorkWithNetworkErrors.checkAnotherApiError(error: error)
            let errMyApi = WorkWithNetworkErrors.checkMyAPIError(data: data)
            let recoveryErr = WorkWithNetworkErrors.checkRecoveryPassErr(data: data)
            
            // If data is not nil and no errors, send data
            if let dataUnwrap = data, !errAnotherApi, !errMyApi, !recoveryErr {
                cb(dataUnwrap, false)
                
            // If data is nil or have some errors, send bool error
            } else {
                cb(nil, true)
            }
        }.resume()
    }
    
    // MARK: - Requests
    private func getRequest(url: URL, cb: @escaping cbDataWithErrorTypeAliace) {
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.GET.rawValue
        
        // ading bearer token if exist
        if let token = TokenService.shared.getAuthUserData().token {
            request.addValue("bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        session(url: request, cb: cb)
    }
    
    private func postRequest(url: URL, body: [String: Any], cb: @escaping cbDataWithErrorTypeAliace) {
        var request = URLRequest(url: url)
        request.httpMethod = RequestType.POST.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // ading bearer token if exist
        if let token = TokenService.shared.getAuthUserData().token {
            request.addValue("bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        guard
            let httpBody = try? JSONSerialization.data(
                withJSONObject: body,
                options: []
            )
        else { return }
        
        request.httpBody = httpBody
        
        session(url: request, cb: cb)
    }
    
    private func putRequest() {
        
    }
    
    private func deleteRequest() {
        
    }
    
    // MARK: - Convenient request method
    func request(
        urlStr: String,
        type: RequestType,
        body: [String: Any] = [:],
        cb: @escaping cbDataWithErrorTypeAliace
    ) {
        let url = URL(string: urlStr)
        guard let urlUnwrap = url else { return }
        
        switch type {
        case .GET: getRequest(url: urlUnwrap, cb: cb)
        case .POST: postRequest(url: urlUnwrap, body: body, cb: cb)
        case .PUT: putRequest()
        case .DELETE: deleteRequest()
        }
    }
}
