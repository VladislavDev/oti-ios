//
//  URLs.swift
//  Oti
//
//  Created by Vlad on 8/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class URLs {
    private init() {}
    
    static let api = "https://your-dates.ru"
    
    // Auth
    static let login = "\(URLs.api)/auth/login"
    static let createAcc = "\(URLs.api)/auth/reg"
    static let restorePass = "\(URLs.api)/auth/recoveryPass"
    static let deleteAcc = "\(URLs.api)/user/deleteUser"
    static let emailUs = "\(URLs.api)/user/emailToUs"
    static let changePass = "\(URLs.api)/user/changePass"
    
    // Pair
    static let getPairList = "\(URLs.api)/groups/checkPendingConnections"
    static let addNewPair = "\(URLs.api)/groups/sendRequestConnectionWithUser"
    static let resolveGroup = "\(URLs.api)/groups/resolveConnection"
    static let deleteGroup = "\(URLs.api)/groups/deleteConnectionWithUser"
    
    // Gifts
    static let getDesiredGiftsList = "\(URLs.api)/wishes/wishList"
    static let addDesiredGift = "\(URLs.api)/wishes/addWish"
    static let deleteDesiredGit = "\(URLs.api)/wishes/removeWish"
    
    static let getUnwantedGiftsList = "\(URLs.api)/stops/getStopList"
    static let addUnwantedGift = "\(URLs.api)/stops/addStopItem"
    static let deleteUnwantedGift = "\(URLs.api)/stops/deleteStopItem"
    
    // Events
    static let getAllEventsLists = "\(URLs.api)/events/eventList"
    static let getEventsLists = "\(URLs.api)/events/getEventListWithMonthFilter"
    static let addEvent = "\(URLs.api)/events/addEvent"
    static let changeEvent = "\(URLs.api)/events/changeEvent"
    static let deleteEvent = "\(URLs.api)/events/deleteEvent"
    
    // Icons
    static let iconsPack = "\(URLs.api)/events/getNamesIconsPack2"
}
