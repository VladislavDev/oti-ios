//
//  WorkWithNetworkErrors.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class WorkWithNetworkErrors {
    // My API response
    struct ResponseData: Decodable {
        var message: String
        var status: Int
    }

    // Recovery Pass Response Err
    struct ResponseDataRecoveryPassErr: Decodable {
        var message: MessageRecoveryPassErr
        var status: Int
    }

    struct MessageRecoveryPassErr: Decodable {
        var command: String
        var service: String
    }
    
    // for another API
    static func checkAnotherApiError(error: Error?) -> Bool {
        if let _ = error {
            DispatchQueue.main.async {
                Snackbar.shared.showSnackbar(title: "Error", type: .error)
            }
            return true
        }
        return false
    }
    
    // for My API
    static func checkMyAPIError(data: Data?) -> Bool {
        guard let dataUnwrap = data else { return true }
        
        let checkResp = try? JSONDecoder().decode(ResponseData.self, from: dataUnwrap)
        
        if let resp = checkResp {
            if (200...299).contains(resp.status) {
                // Show success message
                DispatchQueue.main.async {
                    Snackbar.shared.showSnackbar(
                        title: TranslatesMyApiErrors.translateMyApiMessages(errStr: resp.message),
                        type: .success
                    )
                }
                return false
            } else {
                // Show error message
                DispatchQueue.main.async {
                    Snackbar.shared.showSnackbar(
                        title: TranslatesMyApiErrors.translateMyApiMessages(errStr: resp.message),
                        type: .error
                    )
                }
                
                // Logout if token expired
                if resp.message == "Время сессии истекло" {
                    DispatchQueue.main.async {
                        let loginModule = CustomNavController(rootViewController: ModuleBuilder.createLoginModule())
                        
                        TokenService.shared.removeAuthUserData()
                        
                        UIApplication.setRootView(loginModule)
                    }
                }
                
                return true
            }
        }
        return false
    }
    
    // for recovery pass
    static func checkRecoveryPassErr(data: Data?) -> Bool {
        guard let dataUnwrap = data else { return true }
        
        let checkResp = try? JSONDecoder().decode(ResponseDataRecoveryPassErr.self, from: dataUnwrap)
        
        if let resp = checkResp {
            if (200...299).contains(resp.status) {
                return false
            } else {
                // Show error message
                DispatchQueue.main.async {
                    Snackbar.shared.showSnackbar(
                        title: NSLocalizedString(
                            "Ошибка. Мы работаем над этим",
                            comment: "Ошибка. Мы работаем над этим"
                        ),
                        type: .error
                    )
                }
                return true
            }
        }
        return false
    }
}
