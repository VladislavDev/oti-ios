//
//  TranslatesMyApiErrors.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class TranslatesMyApiErrors {
    // I tried the shorter version. But when unloading localizations,
    // data from variables was not inserted. I had to do not nicely.
    static func translateMyApiMessages(errStr: String) -> String {
        switch errStr {
        case "Событие успешно создано":
        return NSLocalizedString("Событие успешно создано", comment: "Событие успешно создано")
            
        case "Событие успешно изменено":
        return NSLocalizedString("Событие успешно изменено", comment: "Событие успешно изменено")
            
        case "Событие удалено":
        return NSLocalizedString("Событие удалено", comment: "Событие удалено")
             
        case "Вы уже состоите в группе":
        return NSLocalizedString(
            "Вы уже состоите в группе",
            comment: "Вы уже состоите в группе"
        )
            
        case "Вы или пользователь состоите в группе":
        return NSLocalizedString(
            "Вы или пользователь состоите в группе",
            comment: "Вы или пользователь состоите в группе"
        )
            
        case "Вы уже отправили запрос":
        return NSLocalizedString("Вы уже отправили запрос", comment: "Вы уже отправили запрос")
            
        case "Пользователя с таким email не существует":
        return NSLocalizedString(
            "Пользователя с таким email не существует",
            comment: "Пользователя с таким email не существует"
        )
            
        case "Самого себя добавлять нельзя":
        return NSLocalizedString("Самого себя добавлять нельзя", comment: "Самого себя добавлять нельзя")
            
        case "Пользователь уже состоит в группе":
        return NSLocalizedString("Пользователь уже состоит в группе", comment: "Пользователь уже состоит в группе")
            
        case "Запрос отправлен":
        return NSLocalizedString("Запрос отправлен", comment: "Запрос отправлен")
            
        case "Запрос успешно удален":
        return NSLocalizedString("Запрос успешно удален", comment: "Запрос успешно удален")
            
        case "Запрос успешно принят":
        return NSLocalizedString("Запрос успешно принят", comment: "Запрос успешно принят")
            
        case "Добавлено в стоп лист":
        return NSLocalizedString("Добавлено в стоп лист", comment: "Добавлено в стоп лист")
            
        case "Удалено":
        return NSLocalizedString("Удалено", comment: "Удалено")
            
        case "Вы успешно зарегистрированы":
        return NSLocalizedString("Вы успешно зарегистрированы", comment: "Вы успешно зарегистрированы")
            
        case "Пользователь с таким email уже существует":
        return NSLocalizedString(
            "Пользователь с таким email уже существует",
            comment: "Пользователь с таким email уже существует"
        )
            
        case "Email или пароль неверен":
        return NSLocalizedString("Email или пароль неверен", comment: "Email или пароль неверен")
            
        case "Ваш аккаунт успешно удален":
        return NSLocalizedString("Ваш аккаунт успешно удален", comment: "Ваш аккаунт успешно удален")
            
        case "Пароль успешно изменен":
        return NSLocalizedString("Пароль успешно изменен", comment: "Пароль успешно изменен")
            
        case "Текущий пароль и \"старый\" пароль не совпадают":
        return NSLocalizedString(
            "Текущий пароль и старый пароль не совпадают",
            comment: "Текущий пароль и старый пароль не совпадают"
        )
            
        case "Пользователь с таким email не существует":
        return NSLocalizedString(
            "Пользователь с таким email не существует",
            comment: "Пользователь с таким email не существует"
        )
            
        case "Ошибка. Что-то пошло не так.":
        return NSLocalizedString("Ошибка. Что-то пошло не так.", comment: "Ошибка. Что-то пошло не так.")
            
        case "Желание создано":
        return NSLocalizedString("Желание создано", comment: "Желание создано")
            
        case "Желание удалено":
        return NSLocalizedString("Желание удалено", comment: "Желание удалено")
            
        case "Время сессии истекло":
        return NSLocalizedString(
            "Время сессии истекло",
            comment: "Время сессии истекло"
        )
            
        case "Сообщение успешно отправлено":
        return NSLocalizedString(
            "Сообщение успешно отправлено",
            comment: "Сообщение успешно отправлено"
        )
            
        case "Этот пользователь уже отправил вам запрос":
        return NSLocalizedString(
            "Этот пользователь уже отправил вам запрос",
            comment: "Этот пользователь уже отправил вам запрос"
        )
            
        default: return errStr
        }
    }
}
