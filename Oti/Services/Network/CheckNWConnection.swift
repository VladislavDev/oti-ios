//
//  CheckNWConnection.swift
//  Oti
//
//  Created by Vlad on 9/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Network
import Foundation

final class CheckNWConnection {
    private static let monitor = NWPathMonitor()
    private static let queue = DispatchQueue.global()
    private static var connectionStatus: NWPath.Status = .unsatisfied
    private static var showingAfterCounter = 0
    
    static func startCheckNetworkConnection() {
        monitor.pathUpdateHandler = { pathHandler in
            if pathHandler.status == .satisfied && showingAfterCounter > 0 {
                DispatchQueue.main.async {
                    Snackbar.shared.showSnackbar(
                        title: NSLocalizedString(
                            "Соединение восстановлено",
                            comment: "Соединение восстановлено"
                        ),
                        type: .restored
                    )
                }
            }
            
            showingAfterCounter += 1
            
            if pathHandler.status == .unsatisfied {
                DispatchQueue.main.async {
                    Snackbar.shared.showSnackbar(
                        title: NSLocalizedString(
                            "Потеряно соединение",
                            comment: "Потеряно соединение"
                        ),
                        type: .warning
                    )
                }
            }
            
            connectionStatus = pathHandler.status
        }
        
        monitor.start(queue: queue)
    }
    
    static func getConnectionStatus() -> NWPath.Status {
        return connectionStatus
    }
}
