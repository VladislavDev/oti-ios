//
//  RatingService.swift
//  Oti
//
//  Created by Vlad on 10/1/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import StoreKit

// How to use it:
// If we wont show rate controller
// we need change Build number (in preferences, not this file).
// When user opened app three times (with 1 Build number)
// will be opened rate controller.

final class RatingService {
    static let countNameKey = "showingCount"
    static let openedAppCnt = UserDefaults.standard.integer(forKey: countNameKey)
    
    static let lastBunleVersionKey = "lastBundleVersion"
    static let lastRatedVersion = UserDefaults.standard.string(forKey: lastBunleVersionKey)
    
    static let infoDictionaryKey = kCFBundleVersionKey as String
    static let afterTime = DispatchTime.now() + 2.0
    
    static let currentOpenedAppCnt = UserDefaults.standard.integer(forKey: countNameKey)
    static let equalCount = 3
    
    static func incrementCount() {        
        if currentOpenedAppCnt < equalCount {
            UserDefaults.standard.setValue(currentOpenedAppCnt + 1, forKey: countNameKey)
        }
    }
    
    static func resetOpenedCount() {
        UserDefaults.standard.setValue(1, forKey: countNameKey)
    }
    
    static func showRatingController() {
        guard
            let currentVersion = Bundle.main.object(
                forInfoDictionaryKey: infoDictionaryKey
            ) as? String
        else {
            fatalError("Expected to find a bundle version in the info dictionary")
        }
        
        if openedAppCnt == equalCount && currentVersion != lastRatedVersion {
            DispatchQueue.main.asyncAfter(deadline: afterTime) {

                // Show rating controller
                SKStoreReviewController.requestReview()
                
                // Set last reted current version
                UserDefaults.standard.set(currentVersion, forKey: lastBunleVersionKey)
                
                // Reset opened count
                resetOpenedCount()
            }
        }
        
//        print("currentOpenedAppCnt: ", currentOpenedAppCnt)
//        print("lastRatedVersion: ", lastRatedVersion)
//        print("currentVersion: ", currentVersion)
    }
}
