//
//  LocalNotifications.swift
//  Oti
//
//  Created by Vlad on 9/26/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation
import UserNotifications
import BackgroundTasks

final class LocalNotifications {
    static var shared = LocalNotifications()
    private init() {}
    
    private let notificationCenter = UNUserNotificationCenter.current()
    
    func requestAuth() {
        notificationCenter.requestAuthorization(
            options: [.alert, .sound, .badge],
            completionHandler: { (isAuth, err) in
            }
        )
    }
    
    // MARK: Prepare and add local notification    
    func addNotify(
        message: String,
        uuid: String,
        repitType: RepetitionCycle,
        notifyDate: String
    ) {
        // MARK: Setup content
        let content = UNMutableNotificationContent()
        
        content.title = NSLocalizedString(
            "Oti напоминает", comment: "Oti напоминает"
        )
        
        content.body = message
        content.sound = .default
        content.badge = 1
        
        // MARK: Setup date
        let notifyDateArr = notifyDate.split(separator: "-")
        let notifyYear = Int(notifyDateArr[0])
        let notifyMonth = Int(notifyDateArr[1])
        let notifyDay = Int(notifyDateArr[2])
        
        var date = DateComponents()
        
        switch repitType {
        case .year:
            date.year = notifyYear
            date.month = notifyMonth
            date.day = notifyDay
            date.hour = 11
            
        case .month:
            date.month = notifyMonth
            date.day = notifyDay
            date.hour = 11
            
        case .off: return
        }
        
        // MARK: Setup trigget
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: date,
            repeats: true
        )
        
        // MARK: Setup request
        let request = UNNotificationRequest(identifier: uuid, content: content, trigger: trigger)
        
        // MARK: Add request
        notificationCenter.add(request) { err in
            if let err = err {
                print("NOTIFY ERR: \(err.localizedDescription)")
            }
        }
        
//        notificationCenter.getPendingNotificationRequests { notif in
//            print("RES: ", notif)
//            print(" ")
//            print(" ")
//        }
    }
    
    func removeNotify(notifyId: String) {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [notifyId])
        notificationCenter.removeDeliveredNotifications(withIdentifiers: [notifyId])
    }
    
    func removeAllNotify() {
        notificationCenter.removeAllPendingNotificationRequests()
        notificationCenter.removeAllDeliveredNotifications()
    }
}
