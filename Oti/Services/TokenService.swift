//
//  TokenService.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation
import KeychainSwift

class TokenService {
    static let shared = TokenService()
    private init() {}
    
    private let keychain = KeychainSwift()
    
    private let userTokenKey = "USER_TOKEN"
    private let userIdKey = "USER_ID"
    private let userEmailKey = "USER_EMAIL"
    
    private let userDefaults = UserDefaults.standard
    
    func saveAuthUserData(token: String, userId: Int, email: String) {
        keychain.set(token, forKey: userTokenKey)
        keychain.set(String(userId), forKey: userIdKey)
        keychain.set(email, forKey: userEmailKey)
    }
    
    func getAuthUserData() -> (token: String?, userId: String?, email: String?) {
        let token = keychain.get(userTokenKey)
        let userId = keychain.get(userIdKey)
        let email = keychain.get(userEmailKey)
        
        return (token, userId, email)
    }
    
    func isUserLogin() -> Bool {
        if let _ = keychain.get(userTokenKey) { return true }
        return false
    }
    
    func removeAuthUserData() {
        keychain.clear()
    }
}
