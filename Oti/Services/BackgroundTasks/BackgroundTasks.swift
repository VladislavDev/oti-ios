//
//  BackgroundTasks.swift
//  Oti
//
//  Created by Vlad on 9/28/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation
import BackgroundTasks

final class BackgroundTasks {
    static let taskId = "com.vd.oti.refresh"
    static let queue = DispatchQueue(label: "BG.TASK.Q")
    
    // Register task when app launch
    static func registerBGTask() {
        LocalNotifications.shared.removeAllNotify()
        BGTaskScheduler.shared.cancelAllTaskRequests()
        
        BGTaskScheduler.shared.register(
            forTaskWithIdentifier: taskId,
            using: queue
        ) { task in
            updateAllNotifications(bgTask: task as! BGAppRefreshTask)
        }
    }
    
    // Call registered task when app closed or in background
    static func scheduleAppRefresh() {
        let fourHours: TimeInterval = 14400
        
        guard let _ = TokenService.shared.getAuthUserData().token else { return }

        let request = BGAppRefreshTaskRequest(identifier: taskId)
        request.earliestBeginDate = Date(timeIntervalSinceNow: fourHours)

        do {
            try BGTaskScheduler.shared.submit(request)
            
            BGTaskScheduler.shared.getPendingTaskRequests { req in
                print("PENDING: ", req)
            }
        } catch {
            print("Could not schedule app refresh: \(error)")
        }
    }
    
    // What to do task
    static func updateAllNotifications(bgTask: BGAppRefreshTask) {
        LocalNotifications.shared.removeAllNotify()
        
        scheduleAppRefresh()
        
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        
        guard let operation = operationRequest(bgTask: bgTask) else {
            bgTask.setTaskCompleted(success: false)
            return
        }
        
        bgTask.expirationHandler = {
            operationQueue.cancelAllOperations()
        }
        
        operation.completionBlock = {
            bgTask.setTaskCompleted(success: !operation.isCancelled)
        }
        
        operationQueue.addOperations([operation], waitUntilFinished: false)
    }
    
    static func operationRequest(bgTask: BGAppRefreshTask? = nil) -> Operation? {        
        return RequestForBGTask(urlStr: URLs.getAllEventsLists) { (data, resp, err) in
            // Check err
            if let _ = err {
                bgTask?.setTaskCompleted(success: false)
                return
            }
            
            // Parse and decode data
            guard let data = data else {
                bgTask?.setTaskCompleted(success: false)
                return
            }

            let res = try? JSONDecoder().decode(
                EventsListsResponse.self,
                from: data
            )
            
            guard let resUnwrap = res else {
                bgTask?.setTaskCompleted(success: false)
                return
            }
            
            // MARK: Set new notifications from events
            let eventsList = resUnwrap.message
            
            for event in eventsList {
                guard
                    let notifyInA = event.notifyInA,
                    let uuid = event.notifyId
                else { continue }
                
                // Decrement date for notify
                let decrementedDateStr = WorkWithDate.decrementStrDate(
                    selectedDateStr: event.dateEvent,
                    minusFromDateStr: notifyInA
                )
                
                // Create notify with decremented date
                LocalNotifications.shared.addNotify(
                    message: event.title,
                    uuid: uuid,
                    repitType: RepetitionCycle.allCases[RepetitionCycle.getIdx(str: event.remindEvery)],
                    notifyDate: decrementedDateStr
                )
            }
            
            bgTask?.setTaskCompleted(success: true)
        }
    }
}
 
