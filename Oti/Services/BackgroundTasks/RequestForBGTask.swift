//
//  RequestForBGTask.swift
//  Oti
//
//  Created by Vlad on 9/29/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

typealias RequestForBGTaskCompletion = ((Data?, URLResponse?, Error?) -> Void)?

final class RequestForBGTask: AsyncOperation {
    private let urlRequest: URLRequest
    private let completion: RequestForBGTaskCompletion
    private var task: URLSessionDataTask?
    
    init(
        urlRequest: URLRequest,
        completion: RequestForBGTaskCompletion = nil) {
        
        self.urlRequest = urlRequest
        self.completion = completion
        
        super.init()
    }
    
    convenience init?(
        urlStr: String,
        completion: RequestForBGTaskCompletion = nil) {
        
        guard let urlUnwrap = URL(string: urlStr) else { return nil }
        
        var urlRequest = URLRequest(url: urlUnwrap)
        urlRequest.httpMethod = RequestType.POST.rawValue
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // ading bearer token if exist
        if let token = TokenService.shared.getAuthUserData().token {
            urlRequest.addValue("bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        guard let userId = TokenService.shared.getAuthUserData().userId else { return nil }
        
        // -1 just placeholder. MySQL won't find anything by this number.
        // The decision to add -1 was made to simplify the code. Consider it a dirty hack
        var groupId: Int = -1
        let resolveReq = store.state.pairState.pairList.resolveRequest
        let resolveReqCnt = resolveReq?.count
        if let resolveReqCnt = resolveReqCnt, resolveReqCnt > 0 {
            if let id = resolveReq?[0].group_id {
                groupId = id
            }
        }
        
        guard
            let httpBody = try? JSONSerialization.data(
                withJSONObject: [
                    "userId": userId,
                    "groupId": groupId
                ],
                options: []
            )
        else { return nil }
        
        urlRequest.httpBody = httpBody
        
        self.init(urlRequest: urlRequest, completion: completion)
    }
    
    override func main() {
        let config = URLSessionConfiguration.default
        config.waitsForConnectivity = true
        
        let s = URLSession.init(configuration: config)
        
        task = s.dataTask(with: urlRequest) { [unowned self] (data, response, error) in
            defer { self.state = .finished }
            
            guard !self.isCancelled else { return }
            
            if let completion = self.completion {
                completion(data, response, error)
                return
            }
        }
        
        task?.resume()
    }
    
    override func cancel() {
      super.cancel()
      task?.cancel()
    }
}
