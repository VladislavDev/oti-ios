//
//  SceneDelegate.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        window = UIWindow()
        
        // MARK: - Setup local notifications settings
        LocalNotifications.shared.requestAuth()
        
        // MARK: - Setup check network connection
        CheckNWConnection.startCheckNetworkConnection()
        
        // MARK: - Setup window settings
        SceneSettings.setupWindow(window: window!, scene: scene)
        
        // MARK: - Setup snackbar settings
        SceneSettings.setupSnackbar(window: window!)
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func sceneWillResignActive(_ scene: UIScene) {        
        let operationQueue = OperationQueue()
        let updateNotifyOperation = BackgroundTasks.operationRequest()
        
        var id: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
        
        id = UIApplication.shared.beginBackgroundTask(withName: "com.begin.task") {
            updateNotifyOperation?.cancel()
            UIApplication.shared.endBackgroundTask(id)
        }

        updateNotifyOperation?.completionBlock = {
            UIApplication.shared.endBackgroundTask(id)
        }

        if let updateNotifyOperation = updateNotifyOperation {
            operationQueue.addOperation(updateNotifyOperation)
        }
        
        BackgroundTasks.scheduleAppRefresh()
    }
}

