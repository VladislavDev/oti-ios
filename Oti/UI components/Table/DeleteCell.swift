//
//  DeleteCell.swift
//  Oti
//
//  Created by Vlad on 8/31/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class DeleteCell: AbstractClassTableViewCell {
    var funcWhenPressed: (() -> ())? = nil {
        didSet { setupDeleteBtn() }
    }
        
    private var deleteBtn = PositiveNegativeBtn(
        btnTitle: NSLocalizedString("Удалить", comment: "Удалить"),
        positive: false
    )

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        
        setupDeleteBtn()
    }
    
    private func setupDeleteBtn() {
        let contentView = self.contentView
        
        contentView.addSubview(deleteBtn)
        
        NSLayoutConstraint.activate([
            deleteBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            deleteBtn.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor)
        ])
                
        deleteBtn.funcWhenPressed = funcWhenPressed
    }
}
