//
//  AddCell.swift
//  Oti
//
//  Created by Vlad on 8/31/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class AddCell: AbstractClassTableViewCell {
    var funcWhenPressed: funcWhenPressedTypeAlias = nil {
        didSet { setupAddBtn() }
    }
        
    private var addBtn = PositiveNegativeBtn(
        btnTitle: NSLocalizedString("Добавить", comment: "Добавить"),
        positive: true
    )

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        
        setupAddBtn()
    }
    
    private func setupAddBtn() {
        let contentView = self.contentView
        
        contentView.addSubview(addBtn)
        
        NSLayoutConstraint.activate([
            addBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            addBtn.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor)
        ])
        
        addBtn.funcWhenPressed = funcWhenPressed
    }
}
