//
//  BasicCell.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class BasicCell: AbstractClassTableViewCell {
    var isShowDisclosure = false {
        didSet { setup() }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        
        if isShowDisclosure {
            self.accessoryType = .disclosureIndicator
        }
    }
}
