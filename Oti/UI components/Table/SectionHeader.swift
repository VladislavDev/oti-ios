//
//  SectionHeader.swift
//  Oti
//
//  Created by Vlad on 9/5/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class SectionHeader: UIView {
    var withButton = false {
        didSet { setup() }
    }
    
    var titleLabel = LabelView(
        title: "No title",
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .bold,
        aligment: .left
    )
    
    var button: UIButton = {
        let b = UIButton(type: .system)
        b.setImage(UIImage(systemName: "plus"), for: .normal)
        b.imageView?.contentMode = .scaleAspectFit
        b.tintColor = Colors.blue
        
        b.translatesAutoresizingMaskIntoConstraints = false
        
        b.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            b.heightAnchor.constraint(equalToConstant: 15.96),
            b.widthAnchor.constraint(equalToConstant: 40)
        ])
        
        return b
    }()
    
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.directionalLayoutMargins.top,
            leading: 16,
            bottom: self.directionalLayoutMargins.bottom,
            trailing: 14
        )
        
        setupTitleLabel()
        
        if withButton {
            setupButton()
        }
    }
    
    private func setupTitleLabel() {
        self.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(
                equalTo: self.centerYAnchor
            ),
            titleLabel.leadingAnchor.constraint(
                equalTo: self.layoutMarginsGuide.leadingAnchor
            ),
        ])
    }
    
    private func setupButton() {
        self.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.centerYAnchor.constraint(
                equalTo: self.centerYAnchor
            ),
            button.trailingAnchor.constraint(
                equalTo: self.layoutMarginsGuide.trailingAnchor,
                constant: 2
            ),
        ])
    }
    
    @objc private func handleTap() {
        Animation.animate(layer: button.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}
