//
//  CellWithTrash.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class CellWithTrash: AbstractClassTableViewCell {
    
    // MARK: - Basic settings
    var funcWhenIconPressed: funcWhenPressedTypeAlias = nil
    
    private var iconBtn: UIButton = {
        let b = UIButton(type: .system)
        
        b.translatesAutoresizingMaskIntoConstraints = false
        
        b.setImage(UIImage(named: "trash"), for: .normal)
        b.tintColor = Colors.red
        b.imageView?.contentMode = .scaleAspectFit
        
        return b
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupIconBtn()
    }
    
    private func setupIconBtn() {        
        self.addSubview(iconBtn)
        
        iconBtn.addTarget(self, action: #selector(pressIconBtn), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            iconBtn.heightAnchor.constraint(equalToConstant: 18),
            iconBtn.widthAnchor.constraint(equalToConstant: 40),
            iconBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            iconBtn.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 10),
            iconBtn.trailingAnchor.constraint(
                equalTo: self.layoutMarginsGuide.trailingAnchor,
                constant: 4
            )
        ])
    }
    
    // MARK: - Actions
    @objc private func pressIconBtn() {
        Animation.animate(layer: self.contentView.layer) { [unowned self] in
            // cb
            guard let fn = self.funcWhenIconPressed else { return }
            fn()
        }
    }
}
