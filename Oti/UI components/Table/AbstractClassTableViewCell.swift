//
//  AbstractClassTableViewCell.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class AbstractClassTableViewCell: UITableViewCell {
    // MARK: - Basic settings
    var titleItem = "No title" {
        didSet { setupTitleLabel() }
    }
    
    var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.body, weight: .medium)
        l.textColor = Colors.black
        
        return l
    }()
    
    private var lineView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = Colors.grayHard
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupTitleLabel()
        setupLineView()
    }
    
    private func setupSelfView() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.directionalLayoutMargins.top,
            leading: 16,
            bottom: self.directionalLayoutMargins.bottom,
            trailing: 16
        )
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 52)
        ])
    }
    
    private func setupTitleLabel() {
        titleLabel.text = titleItem
        
        contentView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
        ])
    }
    
    private func setupLineView() {
        contentView.addSubview(lineView)
        
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
        ])
    }
}
