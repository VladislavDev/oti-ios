//
//  CancelCell.swift
//  Oti
//
//  Created by Vlad on 8/31/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CancelCell: AbstractClassTableViewCell {
    var funcWhenPressed: funcWhenPressedTypeAlias = nil {
        didSet { setupButtons() }
    }
    
    private var cancelBtn = PositiveNegativeBtn(
        btnTitle: NSLocalizedString("Отмена", comment: "Отмена"),
        positive: false
    )

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        
        setupButtons()
    }
    
    private func setupButtons() {
        let contentView = self.contentView

        cancelBtn.funcWhenPressed = funcWhenPressed
        
        contentView.addSubview(cancelBtn)
        
        NSLayoutConstraint.activate([
            cancelBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            cancelBtn.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
        ])
    }
}
