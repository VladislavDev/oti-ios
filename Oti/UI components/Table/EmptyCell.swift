//
//  EmptyCell.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class EmptyCell: UITableViewCell {
    // MARK: - Basic settings
    var titleItem = "No title" {
        didSet { setupTitleLabel() }
    }
    
    private var titleLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.body, weight: .regular)
        l.textColor = Colors.grayHard
        
        return l
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupTitleLabel()
    }
    
    private func setupSelfView() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }
    
    private func setupTitleLabel() {
        titleLabel.text = titleItem
        
        self.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
        ])
    }
}
