//
//  ResolveRejectCell.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class ResolveRejectCell: AbstractClassTableViewCell {
    var confirmFunc: funcWhenPressedTypeAlias = nil
    var deleteFunc: funcWhenPressedTypeAlias = nil
    
    private var confirmBtn: UIButton = {
        let b =  UIButton()
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(systemName: "checkmark"), for: .normal)
        b.contentMode = .scaleAspectFit
        b.tintColor = Colors.blue
        
        return b
    }()
    
    private var deleteBtn: UIButton = {
        let b =  UIButton()
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(systemName: "xmark"), for: .normal)
        b.contentMode = .scaleAspectFit
        b.tintColor = Colors.red
        
        return b
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.selectionStyle = .none
        
        setupButtons()
    }
    
    private func setupButtons() {
        let contentView = self.contentView
        
        confirmBtn.addTarget(self, action: #selector(tapOnSuccessBtn), for: .touchUpInside)
        deleteBtn.addTarget(self, action: #selector(tapOnDeleteBtn), for: .touchUpInside)
        
        contentView.addSubview(confirmBtn)
        contentView.addSubview(deleteBtn)
        
        NSLayoutConstraint.activate([
            deleteBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            deleteBtn.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            deleteBtn.heightAnchor.constraint(equalTo: deleteBtn.widthAnchor),
            deleteBtn.widthAnchor.constraint(equalToConstant: 24),
            
            confirmBtn.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            confirmBtn.heightAnchor.constraint(equalTo: confirmBtn.widthAnchor),
            confirmBtn.widthAnchor.constraint(equalToConstant: 24),
            confirmBtn.trailingAnchor.constraint(
                equalTo: deleteBtn.leadingAnchor,
                constant: -10
            )
        ])
    }
    
    @objc private func tapOnSuccessBtn() {
        Animation.animate(layer: self.confirmBtn.layer) { [unowned self] in
            guard let cb = self.confirmFunc else { return }
            cb()
        }
    }
    
    @objc private func tapOnDeleteBtn() {
        Animation.animate(layer: self.deleteBtn.layer) { [unowned self] in
            guard let cb = self.deleteFunc else { return }
            cb()
        }
    }
}
