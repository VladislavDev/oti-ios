//
//  AnotherComponents.swift
//  Oti
//
//  Created by Vlad on 9/22/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class AnotherComponents {
    static func createTabBarItem(title: String, tag: Int) -> UITabBarItem {
        let t = UITabBarItem(
            title: title,
            image: nil,
            tag: tag
        )
        
        t.titlePositionAdjustment = UIOffset.init(horizontal: CGFloat.zero, vertical: -15)
        t.setTitleTextAttributes(
            [.font: UIFont.systemFont(ofSize: FontSizes.preBody, weight: .semibold)],
            for: .normal
        )
        
        return t
    }
}
