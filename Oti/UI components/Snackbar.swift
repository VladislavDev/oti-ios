//
//  Snackbar.swift
//  Oti
//
//  Created by Vlad on 8/8/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class Snackbar: UIView {
    enum SnackbarType {
        case success, error, warning, restored
    }
    
    static var shared = Snackbar()

    // MARK: - Basic settings
    private var label: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.body, weight: .regular)
        l.textColor = .white
        
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupLabel()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.zPosition = 999
        
        Effects.addBigShadow(layer: self.layer)
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 100),
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
    }
    
    private func setupLabel() {
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 25),
        ])
    }
    
    private func configureSnackbarStyle(type: SnackbarType) {
        switch type {
        case .success:
            self.backgroundColor = Colors.green
        case .error:
            self.backgroundColor = Colors.red
        case .warning:
            self.backgroundColor = Colors.orange
        case .restored:
            self.backgroundColor = Colors.black
        }
    }
    
    // MARK: - Outside actions
    func showSnackbar(title: String = "No title", type: SnackbarType = .success) {        
        label.text = title
        configureSnackbarStyle(type: type)
        Animation.showSnackbar(layer: self.layer)
    }
}
