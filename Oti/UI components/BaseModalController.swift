//
//  BaseModalController.swift
//  Oti
//
//  Created by Vlad on 8/29/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class BaseModalController: UIViewController {
    private var grabberView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.layer.cornerRadius = 2.5
        v.backgroundColor = Colors.grayHard
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 5),
            v.widthAnchor.constraint(equalToConstant: 36)
        ])
        
        return v
    }()
    
    let titleLabel = LabelView(
        title: "No title",
        color: Colors.black,
        fontSize: FontSizes.title,
        weight: .bold,
        aligment: .center,
        numberOfLines: 0
    )
    
    let subtitleLabel = LabelView(
        title: "No subtitle",
        color: Colors.black,
        fontSize: FontSizes.preBody,
        weight: .regular,
        aligment: .center,
        numberOfLines: 0
    )
    
    var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
        
        return v
    }()
    
    var buttonWithShadow: RectBtnWithShadow? {
        didSet { setupButton() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupGrabberView()
        setupWrapperView()
        setupTitleLabelView()
        setupSubtitleLabelView()
        setupButton()
    }
    
    private func setupSelfView() {        
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(Effects.addBlure(cgRect: self.view.bounds))
        
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 32,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 32
        )
        
        dismissKey()
    }
    
    private func setupGrabberView() {
        self.view.addSubview(grabberView)
        
        NSLayoutConstraint.activate([
            grabberView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            grabberView.topAnchor.constraint(
                equalTo: self.view.topAnchor,
                constant: 5.5
            )
        ])
    }
    
    private func setupWrapperView() {
        self.view.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.centerYAnchor.constraint(
                equalTo: self.view.centerYAnchor,
                constant: -70
            ),
            wrapperView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            wrapperView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupTitleLabelView() {
        wrapperView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: wrapperView.centerXAnchor),
            titleLabel.widthAnchor.constraint(equalTo: wrapperView.widthAnchor),
            titleLabel.topAnchor.constraint(equalTo: wrapperView.topAnchor)
        ])
    }
    
    private func setupSubtitleLabelView() {
        wrapperView.addSubview(subtitleLabel)
        
        NSLayoutConstraint.activate([
            subtitleLabel.centerXAnchor.constraint(equalTo: wrapperView.centerXAnchor),
            subtitleLabel.widthAnchor.constraint(equalTo: wrapperView.widthAnchor),
            subtitleLabel.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: 12
            )
        ])
    }
    
    private func setupButton() {
        guard let buttonWithShadow = buttonWithShadow else { return }
        
        self.view.addSubview(buttonWithShadow)
        
        NSLayoutConstraint.activate([
            buttonWithShadow.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            buttonWithShadow.topAnchor.constraint(
                equalTo: wrapperView.bottomAnchor,
                constant: 32
            )
        ])
    }
}
