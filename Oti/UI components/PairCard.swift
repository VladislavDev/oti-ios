//
//  PairCard.swift
//  Oti
//
//  Created by Vlad on 9/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class PairCard: UIView {
    
    // MARK: - Create UI elements
    private let imgView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        
        i.contentMode = .top
        Effects.addSmallShadow(layer: i.layer)
        
        i.image = UIImage(named: "pairHeart")
        
        NSLayoutConstraint.activate([
            i.heightAnchor.constraint(equalToConstant: 56),
            i.widthAnchor.constraint(equalTo: i.heightAnchor)
        ])
        
        return i
    }()
    
    private var textWrapper: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    private var titleLabel = LabelView(
        title: "",
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .bold,
        aligment: .left,
        numberOfLines: 1
    )
    
    private var subTitleLabel = LabelView(
        title: NSLocalizedString(
            "Хочет эти подарки",
            comment: "Хочет эти подарки"
        ),
        color: Colors.grayHard,
        fontSize: FontSizes.preBody,
        weight: .regular,
        aligment: .left,
        numberOfLines: 1
    )
    
    // MARK: - Inits
    init(pairName: String) {
        super.init(frame: CGRect())
        
        self.titleLabel.text = pairName
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        setupSelf()
        setupImgView()
        setupTextWrapper()
        setupTitleLabel()
        setupSubTitleLabel()
    }
    
    // MARK: - UI elements ancors
    private func setupSelf() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.layer.cornerRadius = 20
        self.backgroundColor = .white
        Effects.addSmallShadow(layer: self.layer)
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 15,
            leading: 24,
            bottom: 15,
            trailing: 24
        )
    }
    
    private func setupImgView() {
        self.addSubview(imgView)
        
        NSLayoutConstraint.activate([
            imgView.centerYAnchor.constraint(
                equalTo: self.centerYAnchor
            ),
            imgView.leadingAnchor.constraint(
                equalTo: self.layoutMarginsGuide.leadingAnchor
            )
        ])
    }
    
    private func setupTextWrapper() {
        self.addSubview(textWrapper)
        
        NSLayoutConstraint.activate([
            textWrapper.centerYAnchor.constraint(
                equalTo: self.centerYAnchor
            ),
            textWrapper.heightAnchor.constraint(
                equalToConstant: 38
            ),
            textWrapper.leadingAnchor.constraint(
                equalTo: imgView.trailingAnchor,
                constant: 15
            ),
            textWrapper.trailingAnchor.constraint(
                equalTo: self.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupTitleLabel() {
        textWrapper.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(
                equalTo: textWrapper.topAnchor
            ),
            titleLabel.leadingAnchor.constraint(
                equalTo: textWrapper.leadingAnchor
            ),
            titleLabel.trailingAnchor.constraint(
                equalTo: textWrapper.trailingAnchor
            )
        ])
    }
    
    private func setupSubTitleLabel() {
        textWrapper.addSubview(subTitleLabel)
        
        NSLayoutConstraint.activate([
            subTitleLabel.bottomAnchor.constraint(
                equalTo: textWrapper.bottomAnchor
            ),
            subTitleLabel.leadingAnchor.constraint(
                equalTo: textWrapper.leadingAnchor
            ),
            subTitleLabel.trailingAnchor.constraint(
                equalTo: textWrapper.trailingAnchor
            )
        ])
    }
}
