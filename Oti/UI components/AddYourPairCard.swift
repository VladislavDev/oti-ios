//
//  AddYourPairCard.swift
//  Oti
//
//  Created by Vlad on 9/22/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class AddYourPairCard: UIView {
    private let imgView: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFill
        v.image = UIImage(named: "pairHeart")
        
        Effects.addSmallShadow(layer: v.layer)
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 56),
            v.widthAnchor.constraint(equalTo: v.heightAnchor)
        ])
        
        return v
    }()
    
    private let labelView = LabelView(
        title: NSLocalizedString("Добавьте вашу пару", comment: "Добавьте вашу пару"),
        fontSize: FontSizes.body,
        weight: .bold,
        aligment: .left
    )
    
    var button = RectBtnWithShadow(
        btnTitle: NSLocalizedString("Добавить", comment: "Добавить")
    )
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelf()
        setupImgView()
        setupLabelView()
        setupButton()
    }
    
    private func setupSelf() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 20
        
        Effects.addSmallShadow(layer: self.layer)
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 119)
        ])
    }
    
    private func setupImgView() {
        self.addSubview(imgView)
        
        NSLayoutConstraint.activate([
            imgView.centerXAnchor.constraint(
                equalTo: self.centerXAnchor
            ),
            imgView.bottomAnchor.constraint(
                equalTo: self.topAnchor,
                constant: 27
            )
        ])
    }
    
    private func setupLabelView() {
        self.addSubview(labelView)
        
        NSLayoutConstraint.activate([
            labelView.centerXAnchor.constraint(
                equalTo: self.centerXAnchor
            ),
            labelView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: 30
            )
        ])
    }
    
    private func setupButton() {
        self.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(
                equalTo: self.centerXAnchor
            ),
            button.topAnchor.constraint(
                equalTo: labelView.bottomAnchor,
                constant: 13
            )
        ])
    }
}
