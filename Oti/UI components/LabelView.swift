//
//  LabelView.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class LabelView: UILabel {
    init(
        title: String,
        color: UIColor = Colors.black,
        fontSize: CGFloat,
        weight: UIFont.Weight,
        aligment: NSTextAlignment,
        numberOfLines: Int = 1
    ) {
        super.init(frame: CGRect())
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.text = title
        self.textColor = color
        self.font = .systemFont(ofSize: fontSize, weight: weight)
        self.textAlignment = aligment
        self.numberOfLines = numberOfLines
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
