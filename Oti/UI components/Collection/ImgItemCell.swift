//
//  ImgItemCell.swift
//  Oti
//
//  Created by Vlad on 9/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class ImgItemCell: UICollectionViewCell {
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.backgroundColor = .white
        
        v.layer.cornerRadius = 20
        v.clipsToBounds = true
        
        return v
    }()
    
    private var iconView: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.image = UIImage(named: "selectedImg")
        
        Effects.addMidShadow(layer: v.layer)
        
        return v
    }()
    
    var illustrationView: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFill
        
        Effects.addMidShadow(layer: v.layer)
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 57),
            v.widthAnchor.constraint(equalTo: v.heightAnchor)
        ])
        
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        setDeselected()
        
        illustrationView.image = nil
    }
    
    private func setup() {
        setupSelf()
        setupWrapperView()
        setupIllustrationView()
        setupIconView()
    }
    
    private func setupSelf() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 20
        
        Effects.addSmallShadow(layer: self.layer)
    }
    
    private func setupWrapperView() {
        self.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.topAnchor.constraint(equalTo: self.topAnchor),
            wrapperView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            wrapperView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            wrapperView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    private func setupIconView() {
        self.addSubview(iconView)
        
        iconView.isHidden = true
        
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: -8
            ),
            iconView.trailingAnchor.constraint(
                equalTo: self.trailingAnchor,
                constant: 8
            )
        ])
    }
    
    private func setupIllustrationView() {
        wrapperView.addSubview(illustrationView)
        
        NSLayoutConstraint.activate([
            illustrationView.centerYAnchor.constraint(equalTo: wrapperView.centerYAnchor),
            illustrationView.centerXAnchor.constraint(equalTo: wrapperView.centerXAnchor),
        ])
    }
    
    func setSelected() {
        iconView.isHidden = false
        wrapperView.layer.borderWidth = 0.5
        wrapperView.layer.borderColor = Colors.blue.cgColor
    }
    
    func setDeselected() {
        iconView.isHidden = true
        wrapperView.layer.borderWidth = 0
        wrapperView.layer.borderColor = .none
    }
}
