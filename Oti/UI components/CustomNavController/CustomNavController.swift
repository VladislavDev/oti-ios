
//
//  CustomNavController.swift
//  Oti
//
//  Created by Vlad on 8/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class CustomNavController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // Setting the status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        // Get current vc
        if let topVC = viewControllers.last {
            if topVC.preferredStatusBarStyle.rawValue == 0 {
                return .lightContent
            } else {
                return topVC.preferredStatusBarStyle
            }
        }
        return .default
    }
    
    private func setup() {
        setupSelfView()
    }
    
    private func setupSelfView() {
        let tabBar = UINavigationBar.appearance()
        
        // Remove border
        tabBar.shadowImage = UIImage()
        
        // Set max opacity
        tabBar.isTranslucent = false
        
        // Set navbar bg color
        tabBar.barTintColor = Colors.blue
        
        // Color for buttons
        tabBar.tintColor = .white
        
        // Color for title
        TextAttributes.changeTabBarTitleColor(tabBar: tabBar, color: .white)
    }
}
