//
//  AdditionToNavBar.swift
//  Oti
//
//  Created by Vlad on 8/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class AdditionToNavBar: UIView {
    private weak var parentView: UIView!
    private var title: String?
    var titleLabel = LabelView(
        title: "",
        color: .white,
        fontSize: FontSizes.subhead,
        weight: .bold,
        aligment: .center
    )
    
    init(
        parentViewForGetConstraint: UIView,
        title: String?
    ) {
        super.init(frame: CGRect())
        
        self.parentView = parentViewForGetConstraint
        self.title = title
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        settingInParent()
        setupSelfView()
        setupTitleLabel()
    }
    
    // It is necessary so that not to set constraints in each component (DRY)
    private func settingInParent() {
        // Add self view into parent subview
        parentView.addSubview(self)
        
        // Setting constraints that view into parent view
        NSLayoutConstraint.activate([
            self.centerXAnchor.constraint(equalTo: parentView.centerXAnchor),
            // HACK. For more rounded corners as in design
            self.topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor, constant: -70),
            // HAСK. To hide white spaces on the sides of a component
            self.widthAnchor.constraint(equalTo: parentView.widthAnchor, constant: 5),
        ])
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundColor = Colors.blue
        self.layer.cornerRadius = 75
        // Corner radius only bottom left and bottom right sides
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        Effects.addColorizeShadow(layer: self.layer, shadowColor: Colors.forBlueShadow)
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 142),
        ])
    }
    
    private func setupTitleLabel() {
        guard let title = title else { return }
        
        titleLabel.text = title
        
        self.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -27),
            titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
}
