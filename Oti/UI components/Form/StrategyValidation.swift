//
//  StrategyValidation.swift
//  Oti
//
//  Created by Vlad on 9/2/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

protocol ValidationProtocol {
    var message: String { get set }
    func getRegex() -> String
}

// Algs
class EmailRegex: ValidationProtocol {
    var message = NSLocalizedString(
        "Некорректный емейл",
        comment: "Некорректный емейл"
    )
    func getRegex() -> String {
        return "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    }
}

class PasswordRegex: ValidationProtocol {
    var message = NSLocalizedString(
        "Длинна должна от 5 до 8, буквы и символы",
        comment: "Длинна должна от 5 до 8, буквы и символы"
    )
    func getRegex() -> String {
        return #"^(?=[\S\s]{4,8}$)[\S\s]*"#
    }
}

class EventTitleRegex: ValidationProtocol {
    var message = NSLocalizedString(
        "Длинна должна быть от 2 до 40 символов",
        comment: "Длинна должна быть от 2 до 40 символов"
    )
    func getRegex() -> String {
        return #"^(?=[\S\s]{1,40}$)[\S\s]*"#
    }
}

class StandartTitleRegex: ValidationProtocol {
    var message = NSLocalizedString(
        "Длинна должна быть от 3 до 20 символов",
        comment: "Длинна должна быть от 3 до 20 символов"
    )
    func getRegex() -> String {
        return #"^(?=[\S\s]{2,20}$)[\S\s]*"#
    }
}

class TexMessageRegex: ValidationProtocol {
    var message = NSLocalizedString(
        "Длинна должна быть от 10 до 40 символов",
        comment: "Длинна должна быть от 10 до 40 символов"
    )
    func getRegex() -> String {
        return #"^(?=[\S\s]{9,40}$)[\S\s]*"#
    }
}

// Client
class Validation {
    var algorithm: ValidationProtocol!
    
    func validate(text: String) -> (res: Bool, message: String?) {
        if text.isEmpty {
            return (
                false,
                NSLocalizedString(
                    "Поле не должно быть пустым",
                    comment: "Поле не должно быть пустым"
                )
            )
        }
        
        if text.containsEmoji() {
            return (
                false,
                NSLocalizedString(
                    "Текст содержит эмоджи",
                    comment: "Текст содержит эмоджи"
                )
            )
        }
        
        let res = try? NSRegularExpression(pattern: algorithm.getRegex(), options: [])
            .firstMatch(
                in: text,
                options: [],
                range: NSRange(location: 0, length: text.count - 1)
            )
        
        if let _ = res { return (true, nil) }
        return (false, algorithm.message)
    }
    
    func setAlgoritm(alg: ValidationProtocol) {
        self.algorithm = alg
    }
}

// Families algs
class EmailValidations: Validation {
    override init() {
        super.init()
        
        algorithm = EmailRegex()
    }
}

class PasswordValidations: Validation {
    override init() {
        super.init()
        
        algorithm = PasswordRegex()
    }
}

class TextValidations: Validation {
    override init() {
        super.init()
        
        algorithm = EventTitleRegex()
    }
}

