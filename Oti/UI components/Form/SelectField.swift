//
//  SelectField.swift
//  Oti
//
//  Created by Vlad on 8/15/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class SelectField: UIView {
    
    // MARK: - Basic settings
    private var title: String!
    private var dataList: [String]!
    private var initialValue: String?
    var initialIdx: Int? {
        didSet {
            setupSelectedLabel()
            setupIconView()
        }
    }
    private var menuBtnsList: [UIButton] = []
    
    private var titleLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = Colors.black
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .bold)
        
        return l
    }()
    
    private var stackView: UIStackView = {
        let s =  UIStackView()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .horizontal
        
        return s
    }()
    
    private var selectedLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = Colors.blue
        l.font = .systemFont(ofSize: FontSizes.body, weight: .regular)
        
        return l
    }()
    
    private var iconView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "selectArrow")
        i.contentMode = .scaleAspectFit
        
        return i
    }()
    
    private var lineView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = Colors.grayMid
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        return v
    }()
    
    private var menuView: UIView = {
        let padding: CGFloat = 10
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 12
        v.backgroundColor = .white
        v.isHidden = true
        
        v.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: padding,
            leading: padding,
            bottom: padding,
            trailing: padding
        )
        
        Effects.addMidShadow(layer: v.layer)
        
        return v
    }()
    
    private var contentMenuStackView: UIStackView = {
        let s = UIStackView()
        
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .vertical
        s.distribution = .fillProportionally
        
        return s
    }()
    
    private func createMenuItemBtn(title: String) -> UIButton {
        let b = UIButton()
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle(title, for: .normal)
        b.setTitleColor(.black, for: .normal)
        b.contentHorizontalAlignment = .leading
        
        return b
    }
    
    init(
        title: String,
        dataList: [String],
        initialValue: String? = nil,
        initialIdx: Int?
    ) {
        super.init(frame: CGRect())
        
        self.title = title
        self.dataList = dataList
        self.initialValue = initialValue
        self.initialIdx = initialIdx
        
        for title in dataList {
            let btn = createMenuItemBtn(title: title)
            btn.addTarget(self, action: #selector(actionMenuItem(item:)), for: .touchUpInside)
            
            self.menuBtnsList.append(btn)
        }
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupTextLabel()
        setupSelectedLabel()
        setupIconView()
        setupStackView()
        setupLineView()
        setupMenuView()
        setupContentMenuView()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 39),
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
    }
    
    private func setupTextLabel() {
        titleLabel.text = title.uppercased()
        
        self.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            titleLabel.heightAnchor.constraint(equalToConstant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    private func setupSelectedLabel() {
        if let selectedValue = initialValue {
            selectedLabel.text = selectedValue
        } else if let initialIdx = initialIdx {
            selectedLabel.text = dataList[initialIdx]
        } else {
            selectedLabel.text = dataList.first
        }
        
        stackView.addArrangedSubview(selectedLabel)
    }
    
    private func setupIconView() {
        stackView.addArrangedSubview(iconView)
    }
    
    private func setupStackView() {
        self.addSubview(stackView)
        
        let tapGesure = UITapGestureRecognizer(target: self, action: #selector(toggleMenu))
        stackView.addGestureRecognizer(tapGesure)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: 3
            ),
            stackView.bottomAnchor.constraint(
                equalTo: self.bottomAnchor
            ),
            stackView.leadingAnchor.constraint(
                equalTo: self.leadingAnchor
            ),
            stackView.trailingAnchor.constraint(
                equalTo: self.trailingAnchor,
                constant: -7
            ),
        ])
    }
    
    private func setupLineView() {
        self.addSubview(lineView)
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 5),
            lineView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    private func setupMenuView() {
        self.addSubview(menuView)
        
        NSLayoutConstraint.activate([
            menuView.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
            menuView.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
    }
    
    private func setupContentMenuView() {
        menuView.addSubview(contentMenuStackView)
        
        for btn in menuBtnsList {
            contentMenuStackView.addArrangedSubview(btn)
        }
        
        NSLayoutConstraint.activate([
            contentMenuStackView.topAnchor.constraint(equalTo: menuView.layoutMarginsGuide.topAnchor),
            contentMenuStackView.bottomAnchor.constraint(equalTo: menuView.layoutMarginsGuide.bottomAnchor),
            contentMenuStackView.leadingAnchor.constraint(equalTo: menuView.layoutMarginsGuide.leadingAnchor),
            contentMenuStackView.trailingAnchor.constraint(equalTo: menuView.layoutMarginsGuide.trailingAnchor)
        ])
    }
    
    func getIdxBtn() -> Int {
        let idx = menuBtnsList.firstIndex { btn -> Bool in
            btn.titleLabel?.text == selectedLabel.text
        }
        
        guard let idxUnwrap = idx else { return 0 }
        
        return Int(idxUnwrap)
    }
    
    // MARK: - Actions
    @objc private func toggleMenu() {
        func toggle() {
            self.menuView.isHidden = !self.menuView.isHidden
        }
        
        let isShowMenu = self.menuView.isHidden
        
        // Menu animations
        if isShowMenu { toggle() }
        
        Animation.showSelectMenu(elem: menuView, isShowMenu: isShowMenu) {
            if !isShowMenu { toggle() }
        }
        
        // Icon animations
        Animation.rotate(image: iconView, isShowMenu: isShowMenu)
    }
    
    @objc private func actionMenuItem(item: UIButton) {
        selectedLabel.text = item.titleLabel?.text
        
        toggleMenu()
    }
    
    // Catching a tap event outside the scope
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        // If menuView is visible
        if !menuView.isHidden {
            // For each button
            for btn in menuBtnsList {
                // Get an object at the point
                let pointForTargetView: CGPoint = btn.convert(point, from: self)
                
                // Button contain the specified point
                if btn.bounds.contains(pointForTargetView) {
                    // return that button
                    return btn
                }
            }
        }

        return super.hitTest(point, with: event)
    }
}
