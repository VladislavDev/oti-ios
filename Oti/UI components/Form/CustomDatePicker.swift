//
//  CustomDatePicker.swift
//  Oti
//
//  Created by Vlad on 8/16/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class CustomDatePicker: UIView {
    
    // MARK: - Basic settings
    private var title: String!
    
    private var titleLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = Colors.black
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .bold)
        
        return l
    }()
    
     var dateField: UITextField = {
        let b = UITextField()
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.textColor = Colors.blue
        b.font = .systemFont(ofSize: FontSizes.body, weight: .regular)
        b.contentHorizontalAlignment = .leading
        
        return b
    }()
    
    private var lineView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = Colors.grayMid
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        return v
    }()
    
    private var toolBar: UIToolbar = {
        // Set frame needed to avoid error in Xcode 11.6
        let t = UIToolbar(frame: CGRect(x: 0, y: 0, width: 200, height: 35))
        t.sizeToFit()
        
        let doneBtn = UIBarButtonItem(
            barButtonSystemItem: .done,
            target: self,
            action: #selector(doneDatePicker)
        )
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        t.setItems([flexSpace, doneBtn], animated: true)
        
        return t
    }()
    
    let datePicker: UIDatePicker = {
        let d = UIDatePicker()
        
        if #available(iOS 13.4, *) {
            d.preferredDatePickerStyle = .wheels
        }
        
        d.locale = WorkWithLang.getCurrentLocale()
        d.datePickerMode = .date
        d.minimumDate = WorkWithDate.rangeDate(minimum: true)
        d.maximumDate = WorkWithDate.rangeDate(minimum: false)
        
        return d
    }()
    
    init(
        title: String,
        date: String = WorkWithDate.getFormettedDate(format: .full)
    ) {
        super.init(frame: CGRect())
        
        self.title = title
        self.dateField.text = date.capitalized
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupTitleLabel()
        setupDateField()
        setupLineView()
        setupDatePickerSettings()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 37)
        ])
    }
    
    private func setupTitleLabel() {
        titleLabel.text = title.uppercased()
        
        self.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 12),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    private func setupDateField() {
        self.addSubview(dateField)
        
        dateField.inputView = datePicker
        dateField.inputAccessoryView = toolBar
        
        NSLayoutConstraint.activate([
            dateField.topAnchor.constraint(equalTo: titleLabel.topAnchor, constant: 13),
            dateField.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            dateField.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            dateField.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    private func setupLineView() {
        self.addSubview(lineView)
        
        NSLayoutConstraint.activate([
            lineView.topAnchor.constraint(equalTo: self.bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    private func setupDatePickerSettings() {
        datePicker.addTarget(self, action: #selector(changeDatePicker), for: .valueChanged)
    }
    
    func getDate() -> Date {
        return datePicker.date
    }
    
    // MARK: - Actions
    @objc private func changeDatePicker() {
        dateField.text = WorkWithDate.getFormettedDate(
            format: .full,
            date: datePicker.date
        ).capitalized
    }
    
    @objc private func doneDatePicker() {
        self.endEditing(true)
    }
}
