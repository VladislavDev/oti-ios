//
//  OverrideTextField.swift
//  Oti
//
//  Created by Vlad on 8/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class BaseTextField: UITextField, UITextFieldDelegate, TextFieldProtocol {
    
    // MARK: - Base settings
    private var valType: Validation?
    
    private var errorLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = Colors.red
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .regular)
        l.isHidden = true
        
        return l
    }()
    
    private let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
    
    required init(
        placeholder: String = "No placeholder",
        textContentType: UITextContentType = .emailAddress,
        keyboardType: UIKeyboardType = .emailAddress,
        validationType: Validation? = nil,
        secure: Bool = false
    ) {
        super.init(frame: CGRect())
        
        self.delegate = self
        self.placeholder = placeholder
        self.textContentType = textContentType
        self.keyboardType = keyboardType
        self.clearButtonMode = .whileEditing
        self.isSecureTextEntry = secure
        valType = validationType
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    private func setup() {
        setupSelfView()
        setupErrorLabel()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.textColor = Colors.black
    }
    
    // MARK: - Customize text field behavior
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setClearIcon(iconName: "clearForLight")
    }
    
    private func setClearIcon(iconName: String) {
        for view in subviews where view is UIButton {
            (view as! UIButton).setImage(UIImage(named: iconName), for: .normal)
        }
    }
    
    // MARK: - Validation
    private func setupErrorLabel() {
        self.addSubview(errorLabel)
        
        NSLayoutConstraint.activate([
            errorLabel.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
            errorLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        ])
    }
    
    func validateField() -> Bool {
        guard let valType = valType else {
            return true
        }
        
        let validationRes = valType.validate(text: self.text ?? "")
            
        if validationRes.res {
            errorLabel.isHidden = true
            return true
        } else {
            errorLabel.isHidden = false
            errorLabel.text = validationRes.message
            return false
        }
    }
}
