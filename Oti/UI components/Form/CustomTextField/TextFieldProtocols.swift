//
//  TextFieldProtocols.swift
//  Oti
//
//  Created by Vlad on 8/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// For SOLID OCP, LSP and DIP
protocol TextFieldProtocol: UITextField {
    init(
        placeholder: String,
        textContentType: UITextContentType,
        keyboardType: UIKeyboardType,
        validationType: Validation?,
        secure: Bool
    )
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    func validateField() -> Bool
}
