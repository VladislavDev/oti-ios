//
//  CustomTextField.swift
//  Oti
//
//  Created by Vlad on 8/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// The wrapper, that adds additional styles
final class WrappedTextField: UIView {
    var textField: TextFieldProtocol!
    
    private var lineView: UIView = {
        let l = UIView()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.backgroundColor = Colors.grayMid
        
        NSLayoutConstraint.activate([
            l.heightAnchor.constraint(equalToConstant: 0.55)
        ])
        
        return l
    }()
    
    init(
        textField: TextFieldProtocol
    ) {
        super.init(frame: CGRect())
        
        self.textField = textField
        
        setup()
    }
    
    init(
        placeholder: String = "No placeholder",
        textContentType: UITextContentType = .emailAddress,
        keyboardType: UIKeyboardType = .emailAddress,
        validationType: Validation?,
        secure: Bool = false
    ) {
        super.init(frame: CGRect())
        
        self.textField = BaseTextField(
            placeholder: placeholder,
            textContentType: textContentType,
            keyboardType: keyboardType,
            validationType: validationType,
            secure: secure
        )
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupTextField()
        setupLineView()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 11,
            leading: 0,
            bottom: 11,
            trailing: 0
        )

        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 36)
        ])
    }
    
    private func setupTextField() {
        guard let textField = textField else { return }
        
        addSubview(textField)
        
        NSLayoutConstraint.activate([
            textField.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            textField.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
        ])
    }
    
    private func setupLineView() {
        addSubview(lineView)
        
        lineView.backgroundColor = Colors.grayMid
        
        NSLayoutConstraint.activate([
            lineView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            lineView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            lineView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    func getText() -> String? {
        return textField.text
    }
    
    func validateField() -> Bool {
        return textField.validateField()
    }
}
