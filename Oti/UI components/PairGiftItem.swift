//
//  GiftItem.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class PairGiftItem: UIView {
    enum ItemType {
        case notEmpty, empty
    }
    
    private var labelView = LabelView(
        title: "",
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .medium,
        aligment: .left,
        numberOfLines: 1
    )
    
    private var line: UIView = {
        let l = UIView()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.backgroundColor = Colors.grayMid
        
        NSLayoutConstraint.activate([
            l.heightAnchor.constraint(equalToConstant: 0.5)
        ])
        
        return l
    }()
    
    init(titleItem: String, type: ItemType = .notEmpty) {
        super.init(frame: CGRect())
        
        labelView.text = titleItem
        
        switch type {
        case .notEmpty: labelView.textColor = Colors.black
        case .empty: labelView.textColor = Colors.grayMid
        }
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupLabelView()
        setupLine()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 8,
            leading: 0,
            bottom: 8,
            trailing: 15
        )
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 52),
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
    }
    
    private func setupLabelView() {
        self.addSubview(labelView)
        
        NSLayoutConstraint.activate([
            labelView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            labelView.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            labelView.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
        ])
    }
    
    private func setupLine() {
        self.addSubview(line)
        
        NSLayoutConstraint.activate([
            line.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            line.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            line.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
}
