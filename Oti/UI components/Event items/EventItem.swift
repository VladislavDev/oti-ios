//
//  EventItem.swift
//  Oti
//
//  Created by Vlad on 8/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class EventItem: UICollectionViewCell, EventItemProtocol {

    // MARK: - Basic settings
    var bgColorFrom: UIColor?
    var bgColorTo: UIColor?
    var shadowColor: UIColor?
    var imgName: String = "heart.png"
    
    private var cornderRadius: CGFloat = 20
    
    private var imgView: UIImageView = {
        let i = UIImageView()
        let height: CGFloat = 42
        
        i.contentMode = .scaleAspectFill
        i.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            i.heightAnchor.constraint(equalToConstant: height),
            i.widthAnchor.constraint(equalToConstant: height + 10)
        ])
        
        Effects.addSmallShadow(layer: i.layer)
        
        return i
    }()
    
    var dateLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .semibold)
        l.textColor = .white
        l.textAlignment = .right
        
        return l
    }()
    
    var titleLabel: UILabel = {
        let t = UILabel()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.textColor = .white
        t.font = .systemFont(ofSize: FontSizes.body, weight: .heavy)
        t.numberOfLines = 2
        
        return t
    }()
    
    var seeAllLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .regular)
        
        return l
    }()
    
    var shadowView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    // MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Basic settings
    func setup() {
        setupSelfView()
        setupImgView()
        setupDate()
        setupTitleLabel()
        setupSeeAllLabel()
        setupShadowView()
    }
    
    private func setupSelfView() {
        self.contentView.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 16,
            leading: 14,
            bottom: 13,
            trailing: 14
        )
        
        self.layer.cornerRadius = cornderRadius
    }
    
    private func setupImgView() {
        if !elementIsContainsSubview(element: imgView) {
            self.contentView.addSubview(imgView)
            
            NSLayoutConstraint.activate([
                imgView.topAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.topAnchor
                ),
                imgView.leadingAnchor.constraint(
                    equalTo: self.contentView.leadingAnchor,
                    constant: 5
                )
            ])
        }
        
        // Add network request
        if let url = URL(string: "\(URLs.api)/static/images_pack2/\(imgName)") {
            imgView.sd_setImage(
                with: url,
                placeholderImage: UIImage(named: "defaultEventImg")
            )
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imgView.image = nil
    }
    
    private func setupDate() {
        if !elementIsContainsSubview(element: dateLabel) {
            self.contentView.addSubview(dateLabel)
            
            NSLayoutConstraint.activate([
                dateLabel.topAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.topAnchor,
                    constant: -3
                ),
                dateLabel.leadingAnchor.constraint(
                    lessThanOrEqualTo: imgView.trailingAnchor
                ),
                dateLabel.trailingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.trailingAnchor
                )
            ])
        }
    }
    
    private func setupTitleLabel() {
        if !elementIsContainsSubview(element: titleLabel) {
            self.contentView.addSubview(titleLabel)
            
            NSLayoutConstraint.activate([
                titleLabel.bottomAnchor.constraint(
                    equalTo: self.contentView.bottomAnchor,
                    constant: -35
                ),
                titleLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor
                ),
                titleLabel.trailingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.trailingAnchor
                ),
            ])
        }
    }
    
    private func setupSeeAllLabel() {
        if !elementIsContainsSubview(element: seeAllLabel) {
            self.contentView.addSubview(seeAllLabel)
            
            NSLayoutConstraint.activate([
                seeAllLabel.bottomAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.bottomAnchor
                ),
                seeAllLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor,
                    constant: 1
                ),
                seeAllLabel.trailingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.trailingAnchor
                )
            ])
        }
    }
    
    private func setupShadowView() {
        if !elementIsContainsSubview(element: shadowView) {
            self.contentView.addSubview(shadowView)
            self.contentView.sendSubviewToBack(shadowView)
            
            shadowView.backgroundColor = .white
            
            NSLayoutConstraint.activate([
                shadowView.heightAnchor.constraint(
                    equalTo: self.contentView.heightAnchor,
                    multiplier: 1/4
                ),
                shadowView.leadingAnchor.constraint(
                    equalTo: self.contentView.leadingAnchor,
                    constant: 12
                ),
                shadowView.trailingAnchor.constraint(
                    equalTo: self.contentView.trailingAnchor,
                    constant: -12
                ),
                shadowView.bottomAnchor.constraint(
                    equalTo: self.contentView.bottomAnchor,
                    constant: -3
                )
            ])
        }
    }
    
    private func elementIsContainsSubview(element: UIView?) -> Bool {
        guard let element = element else { return false }
        return contentView.subviews.contains(element)
    }
}
