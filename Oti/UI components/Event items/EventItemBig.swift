//
//  EventItemBig.swift
//  Oti
//
//  Created by Vlad on 9/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

import UIKit
import SDWebImage

final class EventItemBig: UICollectionViewCell, EventItemProtocol {
    // MARK: - Basic settings
    var bgColorFrom: UIColor?
    var bgColorTo: UIColor?
    var shadowColor: UIColor?
    var imgName: String = "heart.png"
    
    private var cornderRadius: CGFloat = 20
    
    private var imgView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFill
        
        Effects.addSmallShadow(layer: i.layer)
        
        return i
    }()
    
    var dateLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.preBody, weight: .bold)
        l.textColor = .white
        l.textAlignment = .left
        
        return l
    }()
    
    var titleLabel: UILabel = {
        let t = UILabel()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.textColor = .white
        t.font = .systemFont(ofSize: FontSizes.subhead, weight: .bold)
        t.numberOfLines = 2
        
        return t
    }()
    
    var seeAllLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .regular)
        
        return l
    }()
    
    var shadowView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    // MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Basic settings
    func setup() {
        setupSelfView()
        setupDate()
        setupTitleLabel()
        setupImgView()
        setupSeeAllLabel()
        setupShadowView()
    }
    
    private func setupSelfView() {
        self.contentView.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 18,
            leading: 19,
            bottom: 17,
            trailing: 14
        )
        
        self.layer.cornerRadius = cornderRadius
    }
    
    private func setupImgView() {
        if !elementIsContainsSubview(element: imgView) {
            self.contentView.addSubview(imgView)
            
            NSLayoutConstraint.activate([
                imgView.topAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.topAnchor,
                    constant: 2
                ),
                imgView.bottomAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.bottomAnchor
                ),
                imgView.leadingAnchor.constraint(
                    equalTo: titleLabel.trailingAnchor,
                    constant: 10
                ),
                imgView.trailingAnchor.constraint(
                    equalTo: self.contentView.trailingAnchor
                )
            ])
        }
        
        if let url = URL(string: "\(URLs.api)/static/images_pack2/\(imgName)") {
            imgView.sd_setImage(
                with: url,
                placeholderImage: UIImage(named: "defaultEventImg")
            )
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imgView.image = nil
    }
    
    private func setupDate() {
        if !elementIsContainsSubview(element: dateLabel) {
            self.contentView.addSubview(dateLabel)
            
            NSLayoutConstraint.activate([
                dateLabel.topAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.topAnchor,
                    constant: -3
                ),
                dateLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor
                ),
            ])
        }
    }
    
    private func setupTitleLabel() {
        if !elementIsContainsSubview(element: titleLabel) {
            self.contentView.addSubview(titleLabel)
            
            NSLayoutConstraint.activate([
                titleLabel.widthAnchor.constraint(
                    equalTo: self.contentView.widthAnchor,
                    multiplier: 2.1/4
                ),
                titleLabel.bottomAnchor.constraint(
                    equalTo: self.contentView.bottomAnchor,
                    constant: -41
                ),
                titleLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor
                ),
            ])
        }
    }
    
    private func setupSeeAllLabel() {
        if !elementIsContainsSubview(element: seeAllLabel) {
            self.contentView.addSubview(seeAllLabel)
            
            NSLayoutConstraint.activate([
                seeAllLabel.bottomAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.bottomAnchor
                ),
                seeAllLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor
                ),
                seeAllLabel.trailingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.trailingAnchor
                )
            ])
        }
    }
    
    private func setupShadowView() {
        if !elementIsContainsSubview(element: shadowView) {
            self.contentView.addSubview(shadowView)
            self.contentView.sendSubviewToBack(shadowView)
            
            shadowView.backgroundColor = .white
            
            NSLayoutConstraint.activate([
                shadowView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
                shadowView.heightAnchor.constraint(
                    equalTo: self.contentView.heightAnchor,
                    multiplier: 1/4
                ),
                shadowView.widthAnchor.constraint(
                    equalTo: self.contentView.widthAnchor,
                    multiplier: 0.8/1
                ),
                shadowView.bottomAnchor.constraint(
                    equalTo: self.contentView.bottomAnchor
                )
            ])
        }
    }
    
    private func elementIsContainsSubview(element: UIView?) -> Bool {
        guard let element = element else { return false }
        return contentView.subviews.contains(element)
    }
}
