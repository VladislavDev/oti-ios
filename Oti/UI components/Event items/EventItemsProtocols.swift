//
//  EventItemsProtocols.swift
//  Oti
//
//  Created by Vlad on 9/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol EventItemProtocol: UICollectionViewCell {
    var titleLabel: UILabel { get set }
    var bgColorFrom: UIColor? { get set }
    var shadowColor: UIColor? { get set }
    var shadowView: UIView { get set }
    var seeAllLabel: UILabel { get set }
    var dateLabel: UILabel { get set }
    var imgName: String { get set }
    func setup()
}
