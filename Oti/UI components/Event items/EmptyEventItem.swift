//
//  EmptyEventItem.swift
//  Oti
//
//  Created by Vlad on 9/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class EmptyEventItem: UICollectionViewCell {
    
    // MARK: - Basic settings
    private var titleLabel: UILabel = {
        let t = UILabel()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.textColor = Colors.black
        t.font = .systemFont(ofSize: FontSizes.preBody, weight: .regular)
        t.numberOfLines = 2
        
        t.text = NSLocalizedString(
            "В этом месяце нет событий",
            comment: "В этом месяце нет событий"
        )
        
        return t
    }()
    
    var addEventBtn = RectBtnWithShadow(
        btnTitle: NSLocalizedString(
            "Добавить",
            comment: "Добавить"
        )
    )
    
    // MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        setupSelfView()
        setupTitleLabel()
        setupAddEventBtn()
    }
    
    private func setupSelfView() {
        self.contentView.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: 16,
            leading: 24,
            bottom: 13,
            trailing: 24
        )
        
        self.contentView.layer.cornerRadius = 20
        self.contentView.backgroundColor = .white
        Effects.addSmallShadow(layer: self.contentView.layer)
    }
    
    private func setupTitleLabel() {
        if !elementIsContainsSubview(element: titleLabel) {
            self.contentView.addSubview(titleLabel)
            
            NSLayoutConstraint.activate([
                titleLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 160),
                titleLabel.centerYAnchor.constraint(
                    equalTo: self.contentView.centerYAnchor
                ),
                titleLabel.leadingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.leadingAnchor
                )
            ])
        }
    }
    
    private func setupAddEventBtn() {
        if !elementIsContainsSubview(element: addEventBtn) {            
            self.contentView.addSubview(addEventBtn)
            
            NSLayoutConstraint.activate([
                addEventBtn.centerYAnchor.constraint(
                    equalTo: self.contentView.centerYAnchor
                ),
                addEventBtn.trailingAnchor.constraint(
                    equalTo: self.contentView.layoutMarginsGuide.trailingAnchor
                ),
            ])
        }
    }
    
    private func elementIsContainsSubview(element: UIView?) -> Bool {
        guard let element = element else { return false }
        return contentView.subviews.contains(element)
    }
}
