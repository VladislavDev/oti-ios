//
//  RoundBtn.swift
//  Oti
//
//  Created by Vlad on 8/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class RoundBtn: UIView {
    enum RoundBtnType {
        case requestFromPair, addPair, createEvent, info
    }
    
    // MARK: - Basic settings
    private var type: RoundBtnType?
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    
    private var iconView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFit
        
        return i
    }()
    
    private var notifyView: UIView = {
        let size: CGFloat = 12.89
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = Colors.green
        v.layer.cornerRadius = size / 2
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: size),
            v.widthAnchor.constraint(equalTo: v.heightAnchor)
        ])
        
        return v
    }()
    
    init(type: RoundBtnType, funcWhenPressed: funcWhenPressedTypeAlias = nil) {
        super.init(frame: CGRect())
        
        self.type = type
        self.funcWhenPressed = funcWhenPressed
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        iconSelection()
        setupIconView()
    }
    
    private func setupSelfView() {
        let size: CGFloat = 55
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = size / 2
        self.backgroundColor = .white
        Effects.addSmallShadow(layer: self.layer)
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: size),
            self.widthAnchor.constraint(equalTo: self.heightAnchor)
        ])
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        self.addGestureRecognizer(tapGesture)
    }
    
    // Setting an icon depending on the self.type
    private func iconSelection() {
        func setupNotifyView() {
            self.addSubview(notifyView)
            
            NSLayoutConstraint.activate([
                notifyView.topAnchor.constraint(equalTo: self.topAnchor, constant: -3),
                notifyView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -6)
            ])
        }
        
        func setIconViewSizeAndIconName(icon: UIImageView, size: CGFloat, name: String) {
            iconView.image = UIImage(named: name)
            
            NSLayoutConstraint.activate([
                icon.heightAnchor.constraint(equalToConstant: size),
                icon.widthAnchor.constraint(equalTo: icon.heightAnchor)
            ])
        }
        
        switch type {
        case .requestFromPair:
            setIconViewSizeAndIconName(icon: iconView, size: 24, name: "pair")
            setupNotifyView()
            
        case .addPair:
            setIconViewSizeAndIconName(icon: iconView, size: 39, name: "addPair")
            
        case .createEvent:
            setIconViewSizeAndIconName(icon: iconView, size: 20, name: "createEvent")
            
        case .info:
            setIconViewSizeAndIconName(icon: iconView, size: 23, name: "alarm")
            
        case .none: break
        }
    }
    
    private func setupIconView() {
        self.addSubview(iconView)
        
        NSLayoutConstraint.activate([
            iconView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            iconView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])
    }
    
    // MARK: - Actions
    @objc private func tap() {
        Animation.animate(layer: self.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}
