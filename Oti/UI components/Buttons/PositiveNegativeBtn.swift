//
//  PositiveNegativeBtn.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class PositiveNegativeBtn: UIButton {
    
    // MARK: - Basic settings
    private var btnTitle = "No title"
    private var positive = true
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    
    init(btnTitle: String, positive: Bool, funcWhenPressed: funcWhenPressedTypeAlias = nil) {
        super.init(frame: CGRect())
        
        self.btnTitle = btnTitle
        self.positive = positive
        self.funcWhenPressed = funcWhenPressed
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setTitle(btnTitle, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: FontSizes.preBody, weight: .semibold)
        
        if positive {
            self.setTitleColor(Colors.blue, for: .normal)
        } else {
            self.setTitleColor(Colors.red, for: .normal)
        }
        
        self.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    // MARK: - Actions
    @objc private func pressed() {
        Animation.animate(layer: self.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}
