//
//  LinkedBtn.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class UnderlineBtn: UIButton {
    
    // MARK: - Basic settings
    private var btnTitle = "No title"
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    
    init(btnTitle: String, funcWhenPressed: funcWhenPressedTypeAlias = nil) {
        super.init(frame: CGRect())
        
        self.btnTitle = btnTitle
        self.funcWhenPressed = funcWhenPressed
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setTitle(btnTitle, for: .normal)
        self.setTitleColor(Colors.blue, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: FontSizes.footnote, weight: .semibold)
        TextAttributes.underline(btn: self, text: btnTitle)
        
        self.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    // MARK: - Actions
    @objc private func pressed() {
        Animation.animate(layer: self.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}
