//
//  RectBtnWithShadow.swift
//  Oti
//
//  Created by Vlad on 9/11/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class RectBtnWithShadow: UIView {
    var button: RectBtn!
    private var shadowView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        
        Effects.addColorizeShadow(layer: v.layer, shadowColor: Colors.forBlueShadow)
        
        return v
    }()
    
    init(frame: CGRect = .zero, btnTitle: String, funcWhenPressed: funcWhenPressedTypeAlias = nil) {
        super.init(frame: frame)
        
        let btn = RectBtn(frame: frame, btnTitle: btnTitle, funcWhenPressed: funcWhenPressed)
        
        self.button = btn
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        setupBtn()
        setupShadowView()
    }
    
    private func setupBtn() {
        self.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: self.topAnchor),
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    private func setupShadowView() {
        self.addSubview(shadowView)
        self.sendSubviewToBack(shadowView)
        
        NSLayoutConstraint.activate([
            shadowView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            shadowView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -2),
            shadowView.heightAnchor.constraint(equalToConstant: 23),
            shadowView.widthAnchor.constraint(
                equalTo: self.widthAnchor,
                multiplier: 3/4
            )
        ])
    }
}
