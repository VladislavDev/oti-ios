//
//  AccountSettingsSectionBtn.swift
//  Oti
//
//  Created by Vlad on 8/16/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class AccountSettingsSectionBtn: UIView {
    
    // MARK: - Basic settings
    private var textColor: UIColor!
    private var btnTitle = "No title"
    private var image: UIImage?
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    private var iconView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFit
        i.image = UIImage(named: "pair")
        
        NSLayoutConstraint.activate([
            i.heightAnchor.constraint(equalToConstant: 30),
            i.widthAnchor.constraint(equalTo: i.heightAnchor),
        ])
        
        return i
    }()
    
    private var titleLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .regular)
        l.textAlignment = .center
        l.numberOfLines = 0
        
        return l
    }()
    
    init(
        btnTitle: String,
        textColor: UIColor = Colors.black,
        image: UIImage?,
        funcWhenPressed: funcWhenPressedTypeAlias = nil
    ) {
        super.init(frame: CGRect())
        
        self.btnTitle = btnTitle.uppercased()
        self.textColor = textColor
        
        if let image = image { self.image = image }
        self.funcWhenPressed = funcWhenPressed
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupWrapperView()
        setupIconView()
        setupTitleLabel()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.backgroundColor = .white
        self.layer.cornerRadius = 14
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalTo: self.widthAnchor),
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(pressed))
        self.addGestureRecognizer(tapGesture)
    }
    
    private func setupWrapperView() {
        self.addSubview(wrapperView)
        
        wrapperView.backgroundColor = .orange
        
        NSLayoutConstraint.activate([
            wrapperView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            wrapperView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])
    }
    
    private func setupIconView() {
        if let image = image {
            self.iconView.image = image
        }
        
        wrapperView.addSubview(iconView)
        
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: wrapperView.topAnchor),
            iconView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])
    }
    
    private func setupTitleLabel() {
        titleLabel.text = btnTitle
        titleLabel.textColor = textColor
        
        wrapperView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 8),
            titleLabel.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 55),
        ])
    }
    
    // MARK: - Actions
    @objc private func pressed() {
        Animation.animate(layer: self.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}
