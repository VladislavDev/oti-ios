//
//  RectBtn.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// MARK: - Basic settings
class RectBtn: UIButton {
    var btnTitle = ""
    var funcWhenPressed: funcWhenPressedTypeAlias = nil
    private var activityIndicator: UIActivityIndicatorView!
    
    init(frame: CGRect = .zero, btnTitle: String, funcWhenPressed: funcWhenPressedTypeAlias = nil) {
        super.init(frame: frame)
        
        self.btnTitle = btnTitle
        self.funcWhenPressed = funcWhenPressed
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel?.font = .systemFont(ofSize: FontSizes.preBody, weight: .heavy)
        self.contentEdgeInsets = UIEdgeInsets(
            top: 12.5,
            left: 28,
            bottom: 12.5,
            right: 28
        )
        self.layer.cornerRadius = 20
        self.setTitle(btnTitle, for: .normal)
        
        // Needs for set concrete CGRect size
        if self.frame == .zero {
            self.sizeToFit()
        }
        
        Effects.addGradient(view: self, colorFrom: Colors.blueFrom, colorTo: Colors.blueTo)
        
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: self.bounds.width)
        ])
        
        self.addTarget(self, action: #selector(pressed), for: .touchUpInside)
    }
    
    @objc private func pressed() {
        Animation.animate(layer: self.layer) { [unowned self] in
            guard let fn = self.funcWhenPressed else { return }
            fn()
        }
    }
}

// MARK: - ActivityIndicator settings
extension RectBtn {
    func showLoading() {
        if activityIndicator == nil {
            activityIndicator = createActivityIndicator()
        }
        
        self.setTitle("", for: .normal)
        self.isEnabled = false
        showSpinning()
    }
    
    func hideLoading() {
        if activityIndicator == nil {
            activityIndicator = createActivityIndicator()
        }
        
        self.setTitle(btnTitle, for: .normal)
        self.isEnabled = true
        activityIndicator.stopAnimating()
    }
    
    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let ai = UIActivityIndicatorView()
        
        ai.hidesWhenStopped = true
        ai.color = .white
        
        return ai
    }
    
    private func centerActivityIndicatorInButton() {
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])
    }
}
