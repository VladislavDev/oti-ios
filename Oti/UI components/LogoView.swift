//
//  LogoView.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class LogoView: UIView {
    private var logoView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        
        i.image = UIImage(named: "logo")
        i.contentMode = .scaleAspectFit
        
        return i
    }()
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupLogoView()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 106),
            self.widthAnchor.constraint(equalTo: self.heightAnchor)
        ])
    }
    
    private func setupLogoView() {
        self.addSubview(logoView)
        
        NSLayoutConstraint.activate([
            logoView.topAnchor.constraint(equalTo: self.topAnchor),
            logoView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            logoView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            logoView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
}
