//
//  CustomTabBar.swift
//  Oti
//
//  Created by Vlad on 8/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
        rootView.popToRootViewController(animated: false)
    }
    
    private func setup() {
        setupCustomTabBar()
        setupSelfController()
    }
    
    private func setupSelfController() {
        // Clear background
        UITabBar.appearance().backgroundImage = UIImage()
        
        // Delete border
        tabBar.clipsToBounds = true
        
        // Set color icon into selected tabBar
        UITabBar.appearance().tintColor = Colors.black

        // Set default tab bar on top of all layers
        self.view.bringSubviewToFront(tabBar)
    }
    
    private func setupCustomTabBar() {
        let customTabBar = CustomTabBar()
        
        view.addSubview(customTabBar)
        
        NSLayoutConstraint.activate([
            // HACK for height tabBar on various devices
            customTabBar.topAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                constant: -50
            ),
            customTabBar.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            customTabBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            customTabBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
    }
}
