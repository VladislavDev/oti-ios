//
//  CustomTabBarUtils.swift
//  Oti
//
//  Created by Vlad on 8/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CustomTabBarUtils {
    static func createTabBarItem(imageNamed: String, tag: Int) -> UITabBarItem {
        var imagesResult: UIImage!
        
        if let img = UIImage(named: imageNamed) {
            imagesResult = img
        } else {
            imagesResult = UIImage(systemName: "doc")
        }
        
        let tabBarItem = UITabBarItem(
            title: nil,
            image: imagesResult,
            tag: tag
        )
        
        tabBarItem.imageInsets = UIEdgeInsets(
            top: 4,
            left: 0,
            bottom: -4,
            right: 0
        )
        
        return tabBarItem
    }
}
