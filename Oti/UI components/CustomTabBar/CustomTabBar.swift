//
//  CustomTabBar.swift
//  Oti
//
//  Created by Vlad on 8/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CustomTabBar: UIView {
    private var cornerRadius: CGFloat = 20
    
    private var containerView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.backgroundColor = .white
        v.layer.masksToBounds = true
        
        return v
    }()
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupContainerView()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.layer.cornerRadius = cornerRadius
        Effects.addMenuShadow(layer: self.layer)
    }
    
    private func setupContainerView() {
        containerView.layer.cornerRadius = cornerRadius
        // Corner radius only top left and top right sides
        containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: self.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
}
