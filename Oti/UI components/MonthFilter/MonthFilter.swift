//
//  CalendarFilter.swift
//  Oti
//
//  Created by Vlad on 8/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class MonthFilter: UIView {
    
    // MARK: - Base settings
    var isLoadedData = false
    
    private var yearLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = .systemFont(ofSize: FontSizes.footnote, weight: .semibold)
        l.textAlignment = .center
        
        return l
    }()
    
    private var monthLabel: UILabel = {
        let l = UILabel()
        
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.font = .systemFont(ofSize: FontSizes.subhead, weight: .bold)
        l.textAlignment = .center
        
        return l
    }()
    
    private var prevMonthBtn: UIButton = {
        let b = UIButton(type: .custom)
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "prev"), for: .normal)
        b.imageView?.contentMode = .scaleAspectFit
        b.contentVerticalAlignment = .center
        b.contentHorizontalAlignment = .center
        
        NSLayoutConstraint.activate([
            b.heightAnchor.constraint(equalToConstant: 50),
            b.widthAnchor.constraint(equalTo: b.heightAnchor)
        ])
        
        b.addTarget(self, action: #selector(actionPrev), for: .touchUpInside)
        
        return b
    }()
    
    private var nextMonthBtn: UIButton = {
        let b = UIButton(type: .custom)
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setImage(UIImage(named: "next"), for: .normal)
        b.imageView?.contentMode = .scaleAspectFit
        b.contentVerticalAlignment = .center
        b.contentHorizontalAlignment = .center
        
        NSLayoutConstraint.activate([
            b.heightAnchor.constraint(equalToConstant: 50),
            b.widthAnchor.constraint(equalTo: b.heightAnchor)
        ])
        
        b.addTarget(self, action: #selector(actionNext), for: .touchUpInside)
        
        return b
    }()
    
    private var resetBtn: UIButton = {
        let b = UIButton(type: .custom)
        
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle(
            NSLocalizedString("сбросить", comment: "Сбросить"),
            for: .normal
        )
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = .systemFont(ofSize: FontSizes.preBody, weight: .bold)
        b.isHidden = true
        
        NSLayoutConstraint.activate([
            b.heightAnchor.constraint(equalToConstant: 40),
            b.widthAnchor.constraint(equalToConstant: 80),
        ])
        
        b.addTarget(self, action: #selector(actionReset), for: .touchUpInside)
        
        return b
    }()
    
    init() {
        super.init(frame: CGRect())
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupContent()
    }
    
    private func setupSelfView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 70),
            self.widthAnchor.constraint(greaterThanOrEqualToConstant: 150)
        ])
    }
    
    private func setupContent() {
        self.addSubview(yearLabel)
        self.addSubview(monthLabel)
        self.addSubview(resetBtn)
        self.addSubview(prevMonthBtn)
        self.addSubview(nextMonthBtn)
        
        yearLabel.text = WorkWithDate.getFormettedDate(format: .year)
        monthLabel.text = WorkWithDate.getFormettedDate(format: .monthName).uppercased()
        
        NSLayoutConstraint.activate([
            yearLabel.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: 2
            ),
            yearLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            
            monthLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            monthLabel.topAnchor.constraint(
                equalTo: yearLabel.bottomAnchor,
                constant: -2
            ),
            
            resetBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            resetBtn.bottomAnchor.constraint(
                equalTo: self.bottomAnchor
            ),
            
            prevMonthBtn.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: 3
            ),
            prevMonthBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            
            nextMonthBtn.topAnchor.constraint(
                equalTo: self.topAnchor,
                constant: 3
            ),
            nextMonthBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    func getData() -> [String: String] {
        return ["year": yearLabel.text!, "month": monthLabel.text!]
    }
    
    // MARK: - Actions
    @objc private func actionNext() {
        nextMonthBtn.debounce(siblings: [prevMonthBtn])
        Animation.animate(layer: nextMonthBtn.layer)
        Animation.moveLabel(
            layer: monthLabel.layer,
            direction: .next
        ) { [unowned self] in
            if self.isLoadedData {
                MonthFilterLogic.updateFilter(
                    monthInFilter: self.monthLabel.text!,
                    yearInFilter: self.yearLabel.text!,
                    monthLabel: self.monthLabel,
                    yearLabel: self.yearLabel,
                    resetBtn: self.resetBtn,
                    direction: .next
                )
            }
        }
    }
    
    @objc private func actionPrev() {
        prevMonthBtn.debounce(siblings: [nextMonthBtn])
        Animation.animate(layer: prevMonthBtn.layer)
        Animation.moveLabel(
            layer: monthLabel.layer,
            direction: .prev
        ) { [unowned self] in
            if self.isLoadedData {
                MonthFilterLogic.updateFilter(
                    monthInFilter: self.monthLabel.text!,
                    yearInFilter: self.yearLabel.text!,
                    monthLabel: self.monthLabel,
                    yearLabel: self.yearLabel,
                    resetBtn: self.resetBtn,
                    direction: .prev
                )
            }
        }
    }
    
    @objc private func actionReset() {
        Animation.animate(layer: resetBtn.layer)
        
        MonthFilterLogic.resetFilter(yearLabel: yearLabel, monthLabel: monthLabel)
        
        resetBtn.isHidden = true
    }
}
