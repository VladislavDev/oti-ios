//
//  MonthFilterLogic.swift
//  Oti
//
//  Created by Vlad on 8/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class MonthFilterLogic {
    enum DirectionFilter {
        case prev
        case next
    }
    
    // MARK: - Basic settings
    static let yearsRange = WorkWithDate.yearsRange
    static let monthsList = WorkWithDate.monthsList
    
    static let curYear = WorkWithDate.getFormettedDate(format: .year)
    static let curMonth = WorkWithDate.getFormettedDate(format: .monthName).uppercased()

    // Logic of getting the next and previous month
    static func getMonthOnDirection(
        monthIdx: Int?,
        direction: DirectionFilter
    ) -> String {
        guard let currentMonthIdx = monthIdx else {
            return monthsList.first!
        }
        
        var month = ""
        
        switch direction {
        case .next:
            // If the next index is less than the December index, then return this index.
            // If the next index more then the December index, then return the January index.
            let newMonthIdx = currentMonthIdx + 1
            let monthsListCtn = monthsList.count - 1
            
            if newMonthIdx >= 0 && newMonthIdx <= monthsListCtn {
                month = monthsList[newMonthIdx]
            } else {
                month = monthsList.first!
            }
        
        case .prev:
            // If the index of the previous month >= 0, then return this index.
            // If the previous month index is -1 then return the December index.
            let newMonthIdx = currentMonthIdx - 1
            let monthsListCtn = monthsList.count - 1
            
            if newMonthIdx >= 0 && newMonthIdx < monthsListCtn {
                month = monthsList[newMonthIdx]
            } else {
                month = monthsList.last!
            }
        }
        
        return month.uppercased()
    }
    
    // MARK: - Year settings
    // Just get increment or decrement current year
    static func getNewYear(
        year: String,
        direction: DirectionFilter
    ) -> String {
        guard let currentYearNum = Int(year) else { return year }
        
        return direction == .next ?
            String(currentYearNum + 1) :
            String(currentYearNum - 1)
    }
    
    // Return the next or previous year if the
    // current month is the first or last,
    // in between, return the current
    static func getYearOnDirection(
        year: String,
        month: String,
        direction: DirectionFilter
    ) -> String {
        let conditionForNextYear =
            direction == .next && month == monthsList.last
        let conditionForPrevYear =
            direction == .prev && month == monthsList.first
        
        if conditionForNextYear || conditionForPrevYear {
            return getNewYear(year: year, direction: direction)
        }
        
        return year
    }
    
    // Stop updating the filter if the current year
    // is outside the specified range of years
    static func stopUpdateFilter(
        year: String,
        direction: DirectionFilter
    ) -> Bool {
        guard
            let yearNum = Int(getNewYear(year: year, direction: direction))
        else { return false }
        
        return yearsRange.contains(yearNum)
    }
    
    // MARK: - Reset filter
    // Set the label values ​​to the values ​​of the current year and month
    static func resetFilter(yearLabel: UILabel, monthLabel: UILabel) {
        yearLabel.text = curYear
        monthLabel.text = curMonth
        
        monthFilterRequest(year: curYear, month: curMonth)
    }
    
    // MARK: - Update filter
    static func updateFilter(
        monthInFilter: String,
        yearInFilter: String,
        monthLabel: UILabel,
        yearLabel: UILabel,
        resetBtn: UIButton,
        direction: DirectionFilter
    ) {
        guard stopUpdateFilter(
            year: yearInFilter,
            direction: direction
        ) else { return }
        
        // Update month label
        let monthInFilter = monthInFilter.capitalized
        let idx = WorkWithDate.getMonthIdx(month: monthInFilter)
        let month = getMonthOnDirection(monthIdx: idx, direction: direction)
        
        monthLabel.text = month
        
        // Update year label
        let year = getYearOnDirection(
            year: yearInFilter,
            month: monthInFilter,
            direction: direction
        )
        
        yearLabel.text = year
        
        // Hide reset button
        if year == curYear && month == curMonth {
            resetBtn.isHidden = true
        } else {
            resetBtn.isHidden = false
        }
        
        // Get events lists
        monthFilterRequest(year: year, month: month)
    }
    
    static func monthFilterRequest(year: String, month: String) {
        guard let monthCnt = WorkWithDate.getMonthIdx(month: month) else { return }
        
        // + 1 because getCurrentMonthIdx method counts from zero
        let monthIdxSrt = String(monthCnt + 1)
        
        // Dispatch month to store
        store.dispatch(
            SetNewValueIntoMonthFilterAction(
                year: year,
                monthIdx: monthIdxSrt,
                month: month
            )
        )
        
        // Requests for get new events lists
        store.dispatch(getEventsListRequestAction(year: year, month: monthIdxSrt))
    }
}
