//
//  SceneSettings.swift
//  Oti
//
//  Created by Vlad on 8/17/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class SceneSettings {
    static func setupWindow(window: UIWindow, scene: UIScene) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window.windowScene = windowScene
        window.backgroundColor = .white
        
        window.rootViewController = ModuleBuilder.checkTokenAndRedirect()
//        window.rootViewController = TestVC()
        
        window.makeKeyAndVisible()
    }
    
    static func setupSnackbar(window: UIWindow) {
        let snackbar = Snackbar.shared
        window.addSubview(snackbar)
        
        NSLayoutConstraint.activate([
            snackbar.topAnchor.constraint(equalTo: window.safeAreaLayoutGuide.topAnchor, constant: -90),
            snackbar.leadingAnchor.constraint(equalTo: window.leadingAnchor),
            snackbar.trailingAnchor.constraint(equalTo: window.trailingAnchor),
            snackbar.centerXAnchor.constraint(equalTo: window.centerXAnchor),
        ])
    }
}

extension UIApplication {
    public static func setRootView(_ viewController: UIViewController) {
        let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        
        sceneDelegate?.window?.rootViewController = viewController
    }
}
