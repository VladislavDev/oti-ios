//
//  WorkWithLang.swift
//  Oti
//
//  Created by Vlad on 8/16/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class WorkWithLang {
    private init() {}
    
    static func getCurrentLocale() -> Locale {
        // Get current locale id
        let identifier = Locale.current.identifier
        
        return Locale(identifier: identifier)
    }
    
    static func getDateFormatter() -> DateFormatter {
        // Create DateFormatter
        let formatter = DateFormatter()
        
        // Setup locale into formatter
        formatter.locale = getCurrentLocale()
        
        return formatter
    }
    
    static func getLocalizedMonthsList() -> [String] {
        // Get DateFormatter
        let formatter = getDateFormatter()
        formatter.dateFormat = GetDateElementType.monthName.rawValue
        
        var res: [String] = []
        
        // Capitalized each month string.
        // "standaloneMonthSymbols" because for "monthSymbols" doesnt dork "LLLL"
        if let monthSymbols = formatter.standaloneMonthSymbols {
            for month in monthSymbols {
                res.append(month.capitalized)
            }
        }
        
        return res
    }
}
