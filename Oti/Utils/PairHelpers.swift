//
//  PairHelpers.swift
//  Oti
//
//  Created by Vlad on 9/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

final class PairHelpers {
    static func getPairEmail(resolveReq: PSGroupsDatas) -> String? {
        guard let selfEmail = TokenService.shared.getAuthUserData().email else { return nil }

        if resolveReq.sender_email == selfEmail {
            return resolveReq.recipient_email
        } else {
            return resolveReq.sender_email
        }
    }
    
    static func getPairName(email: String) -> String? {
        guard let a = email.firstIndex(of: "@") else { return nil }
        
        return String(email[email.startIndex..<a])
    }
}
