//
//  TypeAliases.swift
//  Oti
//
//  Created by Vlad on 8/8/20.
//  Copyright © 2020 Vlad. All rights reserved.
//
import Foundation

typealias funcWhenPressedTypeAlias = (() -> ())?
typealias cbDataWithErrorTypeAliace = (Data?, Bool) -> ()
