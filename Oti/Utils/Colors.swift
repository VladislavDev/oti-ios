//
//  Colors.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

let const: CGFloat = 255
let alpha: CGFloat = 1.0

struct Colors {
    // MARK: - Colors
    static let blue = UIColor(red: 0.313, green: 0.719, blue: 0.95, alpha: alpha)
    static let red = UIColor(red: 0.938, green: 0.312, blue: 0.432, alpha: alpha)
    static let orange = UIColor(red: 1, green: 0.69, blue: 0.161, alpha: alpha)
    static let green = UIColor(red: 0.298, green: 0.851, blue: 0.392, alpha: alpha)
    static let black = UIColor(red: 0.222, green: 0.272, blue: 0.3, alpha: alpha)
    static let grayHard = UIColor(red: 0.569, green: 0.569, blue: 0.584, alpha: alpha)
    static let grayMid = UIColor(red: 0.773, green: 0.773, blue: 0.78, alpha: alpha)
    
    // MARK: - Colors for gradients
    static let blueFrom = UIColor(red: 0.31, green: 0.82, blue: 0.95, alpha: alpha)
    static let blueTo = blue
    
    static let redFrom = red
    static let redTo = UIColor(red: 0.93, green: 0.28, blue: 0.4, alpha: alpha)
    
    static let orangeFrom = orange
    static let orangeTo = UIColor(red: 0.99, green: 0.68, blue: 0.16, alpha: alpha)
    
    // MARK: - Colors for shadows
    static let forBlueShadow = UIColor(red: 69/const, green: 166/const, blue: 221/const, alpha: alpha)
    static let forRedShadow = UIColor(red: 239/const, green: 80/const, blue: 110/const, alpha: alpha)
    static let forOrangeShadow = UIColor(red: 249/const, green: 169/const, blue: 32/const, alpha: alpha)
}
