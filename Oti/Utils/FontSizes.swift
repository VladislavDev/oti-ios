//
//  Sizes.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

struct FontSizes {
    static let large: CGFloat = 33
    static let title: CGFloat = 29
    static let subhead: CGFloat = 25
    static let body: CGFloat = 17
    static let preBody: CGFloat = 15
    static let footnote: CGFloat = 13
}
