//
//  Effects.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

struct Effects {
    // MARK: - Basic shadows
    static func addBigShadow(layer: CALayer, color: CGColor = UIColor.black.cgColor) {
        layer.shadowColor = color
        layer.shadowOffset = .init(width: 0, height: 8)
        layer.shadowRadius = 24
        layer.shadowOpacity = 0.15
    }
    
    static func addMidShadow(layer: CALayer, color: CGColor = UIColor.black.cgColor) {
        layer.shadowColor = color
        layer.shadowOffset = .init(width: 0, height: 4)
        layer.shadowRadius = 9
        layer.shadowOpacity = 0.13
    }
    
    static func addSmallShadow(layer: CALayer) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .init(width: 0, height: 8)
        layer.shadowRadius = 24
        layer.shadowOpacity = 0.07
    }
    
    static func addMenuShadow(layer: CALayer) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .init(width: 10, height: 0)
        layer.shadowRadius = 20
        layer.shadowOpacity = 0.10
    }
    
    // MARK: - Colors shadows
    static func addColorizeShadow(layer: CALayer, shadowColor: UIColor) {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = .init(width: 0, height: 4)
        layer.shadowRadius = 7
        layer.shadowOpacity = 0.35
    }
    
    // MARK: - Blure
    static func addBlure(cgRect: CGRect) -> UIVisualEffectView {
        let effect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurView = UIVisualEffectView(effect: effect)

        blurView.frame = cgRect
    
        return blurView
    }
    
    // MARK: - Gradient
    static func addGradient(
        view: UIView,
        colorFrom: UIColor,
        colorTo: UIColor,
        cornderRadius: CGFloat = 20
    ) {
        let gradient = CAGradientLayer()

        gradient.frame = view.bounds
        gradient.colors = [colorFrom.cgColor, colorTo.cgColor]
        gradient.cornerRadius = cornderRadius

        view.layer.insertSublayer(gradient, at: 0)
    }
}
