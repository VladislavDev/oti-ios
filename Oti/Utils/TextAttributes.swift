//
//  TextAttributes.swift
//  Oti
//
//  Created by Vlad on 8/8/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

struct TextAttributes {
    static func changeTabBarTitleColor(tabBar: UINavigationBar, color: UIColor) {
        tabBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: color
        ]
    }
    
    static func changeSubstringColor(
        text: String,
        elem: UILabel,
        ranges: [(location: Int, length: Int)],
        color: UIColor = Colors.green
    ) {
        let attributedString = NSMutableAttributedString(string: text)
        
        for range in ranges {
            attributedString.addAttribute(
                NSAttributedString.Key.foregroundColor,
                value: color,
                range: NSRange(
                    location: range.location,
                    length: range.length
                )
            )
        }

        elem.attributedText = attributedString
    }
    
    static func underline(btn: UIButton, text: String) {
        let attrs: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        let underlineString = NSMutableAttributedString(string: text, attributes: attrs)
        
        btn.setAttributedTitle(underlineString, for: .normal)
        
    }
    
    static func changePlaceholderTextFiledColor(
        textField: UITextField,
        placeholder: String,
        color: UIColor
    ) {
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [NSAttributedString.Key.foregroundColor: color]
        )
    }

    static func limitingDate(dateStr: String?) -> String {
        let placeholder = "No date"
        
        guard let unwrapDateStr = dateStr else { return placeholder }
        
        if let dateStr = WorkWithDate.transformIsoDateStrToFormattedDateStr(dateStr: unwrapDateStr) {
            return dateStr
        }
        
        return placeholder
    }
    
    static func limitingMonth(month: String) -> String {
        let index = month.startIndex..<month.index(month.startIndex, offsetBy: 3)
        
        return String(month[index])
    }
}
