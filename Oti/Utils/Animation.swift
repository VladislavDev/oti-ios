//
//  Animation.swift
//  Oti
//
//  Created by Vlad on 8/8/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class Animation {
    enum AnimationLabelDirection {
        case next
        case prev
    }
    
    @objc static func animate(layer: CALayer, cb: (() -> ())? = nil) {
        CATransaction.begin()
        let scale = CABasicAnimation(keyPath: "transform.scale")
        
        scale.duration = 0.15
        scale.fromValue = 0.95
        scale.toValue = 1
        scale.autoreverses = false
        scale.repeatCount = 1
        
        CATransaction.setCompletionBlock {
            if let cb = cb { cb() }
        }
        
        layer.add(scale, forKey: nil)
        
        CATransaction.commit()
    }
    
    @objc static func showSnackbar(layer: CALayer) {        
        let duration: Double = 0.3
        
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                layer.anchorPoint.y = 0.1
            }
        ) { _ in
            Animation.hideSnackbar(layer: layer, waitWhenHide: 1.5)
        }
    }
    
    static func hideSnackbar(layer: CALayer, waitWhenHide: Double = 0) {
        let to: CGFloat = 2
        let duration: Double = 0.3
        let waitWhenHide = waitWhenHide
        
        UIView.animate(
            withDuration: duration / 2,
            delay: waitWhenHide,
            options: .curveEaseInOut,
            animations: {
                layer.anchorPoint.y = to
            }
        )
    }
    
    static func moveLabel(
        layer: CALayer,
        direction: AnimationLabelDirection,
        completion: (() -> ())? = nil
    ) {
        let initialXPosition = layer.anchorPoint.x
        let params: (to: CGFloat, from: CGFloat) = direction == .next ? (5, -5) : (-5, 5)
        
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [.curveEaseIn],
            animations: { layer.anchorPoint.x = params.to }
        ) { _ in
            layer.anchorPoint.x = params.from
            
            if let fn = completion {
                fn()
            }

            UIView.animate(
                withDuration: 0.1,
                delay: 0,
                options: .curveEaseInOut,
                animations: {
                    layer.anchorPoint.x = initialXPosition
                }
            )
        }
    }
    
    static func showSelectMenu(
        elem: UIView,
        isShowMenu: Bool,
        completion: (() -> ())? = nil
    ) {
        let params: (from: CGFloat, to: CGFloat) = isShowMenu ? (0, 1) : (1, 0)

        elem.alpha = params.from

        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                elem.alpha = params.to
            }
        ) { _ in
            guard let fn = completion else { return }

            fn()
        }
    }
    
    static func rotate(image: UIImageView, isShowMenu: Bool) {
        if isShowMenu {
            UIView.animate(withDuration: 0.2) {
                image.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            }
        } else {
            UIView.animate(withDuration: 0.15) {
                image.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 50))
            }
        }
    }
}
