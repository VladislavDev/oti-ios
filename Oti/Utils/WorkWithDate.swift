//
//  WorkWithDate.swift
//  Oti
//
//  Created by Vlad on 8/16/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

enum GetDateElementType: String {
    case year = "YYYY"
    case monthName = "LLLL"
    case monthCount = "MM"
    case day = "dd"
    case shortDate = "MMM d"
    case full = "EEEE, MMM d, yyyy"
    case fullFromBackend = "yyyy-MM-dd"
    case iso = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case mdy = "MM/dd/yyyy"
    case mdyWithLodash = "YYYY-MM-dd"
}

class WorkWithDate {
    static let yearsRange = 2000...2050
    
    static let monthsList: [String] = {
        return WorkWithLang.getLocalizedMonthsList()
    }()
    
    // We pass the month and get its index in the array with months
    static func getMonthIdx(month: String) -> Int? {
        if let res = monthsList.firstIndex(where: { $0.capitalized == month.capitalized }) {
            return Int(res)
        }
        return nil
    }
    
    /**
    Get the elements of the current date by argument

    - Parameters:
       - format: How format that date
       - date: Date (optional)

    - Returns: String depending on format
    */
    static func getFormettedDate(format: GetDateElementType, date: Date? = nil) -> String {
        let dateFormatter = WorkWithLang.getDateFormatter()
        
        // Override initial dateFormat setting
        // into WorkWithLang.getDateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        // If date param is NOT NILL, return current date
        if let date = date {
            return dateFormatter.string(from: date).capitalized
        }
        
        // If date param is NILL, return current date
        return dateFormatter.string(from: getCurrentDate()).capitalized
    }
    
    /**
    Format date from string

    - Parameters:
       - dateStr: String date
       - format: GetDateElementType (default .iso)

    - Returns: date
    */
    static func transformStrDateToDate(dateStr: String, format: GetDateElementType = .iso) -> Date? {
        let dateFormatter = WorkWithLang.getDateFormatter()
        
        dateFormatter.dateFormat = format.rawValue
        
        return dateFormatter.date(from: dateStr)
    }
    
    /**
    Input iso date string, output short date string

    - Parameters:
       - dateStr: String iso date

    - Returns: Example: "2020-09-04"
    */
    static func transformIsoDateStrToFormattedDateStr(
        format: GetDateElementType = .shortDate,
        dateStr: String
    ) -> String? {
        // Get date from string
        let date = transformStrDateToDate(dateStr: dateStr)
        
        // Transform date to short date string
        let res = getFormettedDate(format: format, date: date)
        
        return res
    }
    
    /**
    Used as initial value month filter state

    - Returns: Example: "Sep"
    */
    static func getCurrentLimitedMonth() -> String {
        let currentDate = getCurrentDate()
        
        // Transform date to short date string
        let currentShortFormattedDate = getFormettedDate(format: .shortDate, date: currentDate)
        
        // Transform iso month to short version
        let dateStrArr = currentShortFormattedDate.split(separator: " ")
        let month = dateStrArr[0]
        
        let shortMonth = TextAttributes.limitingMonth(month: String(month))
        
        return shortMonth
    }
    
    /**
    Get current date

    - Returns: Current date
    */
    static func getCurrentDate() -> Date {
        return Date()
    }
    
    /**
    Get minimum or maximum Date in App
    
    Needed for MonthFilter and CustoDatePicker for limit date range

    - Parameters:
       - minimum: Minimum or maximum year in years range

    - Returns: Date with maximum or munimum year
    */
    static func rangeDate(minimum: Bool) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = GetDateElementType.mdy.rawValue
        
        var res = Date()
        
        if minimum {
            guard let elem = MonthFilterLogic.yearsRange.first else {
                return res
            }
            
            guard
                let date = formatter.date(from: "12/01/\(String(describing: elem))")
            else { return res }
            
            res = date
            
        } else {
            guard let elem = MonthFilterLogic.yearsRange.last else {
                return res
            }
            
            guard
                let date = formatter.date(from: "01/01/\(String(describing: elem))")
            else { return res }
            
            res = date
        }
        
        return res
    }
    
    static func decrementStrDate(selectedDateStr: String, minusFromDateStr: String) -> String {
        guard
            let selectedDate = transformStrDateToDate(
                dateStr: selectedDateStr,
                format: .iso
            )
        else { return "" }
        
        var modifiedDate: Date?

        if minusFromDateStr == NotifyFor.day.rawValue {
            modifiedDate = Calendar.current.date(byAdding: .day, value: -1, to: selectedDate)
        } else if minusFromDateStr == NotifyFor.week.rawValue {
            modifiedDate = Calendar.current.date(byAdding: .day, value: -7, to: selectedDate)
        }
        
        let notifyDate = WorkWithDate.getFormettedDate(
            format: .mdyWithLodash,
            date: modifiedDate
        )
        
        return notifyDate
    }
}
