//
//  AddGiftModal.swift
//  Oti
//
//  Created by Vlad on 9/5/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

final class AddGiftModal: BaseModalController, AddGiftVCProtocol, StoreSubscriber {
    
    // MARK: - Gift type
    var giftType: GiftType = .desired
    
    // MARK: - Presenter
    var presenter: AddGiftPresenterProtocol?
    
    // MARK: - Create UI elements
    var titleField: WrappedTextField = {
        let validation = TextValidations()
        validation.setAlgoritm(alg: StandartTitleRegex())
        
        let t = WrappedTextField(
            placeholder: NSLocalizedString(
                "Заголовок",
                comment: "Заголовок"
            ),
            textContentType: .name,
            keyboardType: .default,
            validationType: validation
        )
        
        return t
    }()
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.giftsListsState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK - ReSwift
    func newState(state: GiftsListsState) {
        if let buttonWithShadow = buttonWithShadow {
            if state.isStartAddingGiftAction {
                buttonWithShadow.button.showLoading()
            } else {
                buttonWithShadow.button.hideLoading()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setup() {
        if giftType == .desired {
            titleLabel.text = NSLocalizedString(
                "Добавить желаемый подарок",
                comment: "Добавить желание"
            )
            
            subtitleLabel.text = NSLocalizedString(
                "Добавьте желаемый подарок, который вы хотите. Он будет отображен вашему партнеру",
                comment: "Добавьте желаемый подарок, который вы хотите. Он будет отображен вашему партнеру"
            )
        } else {
            titleLabel.text = NSLocalizedString(
                "Добавить нежелательный подарок",
                comment: "Добавить нежелательный подарок"
            )
            
            subtitleLabel.text = NSLocalizedString(
                "Добавьте нежелательный подарок. Он будет отображен вашему партнеру",
                comment: "Добавьте нежелательный подарок. Он будет отображен вашему партнеру"
            )
        }
        
        
        buttonWithShadow = RectBtnWithShadow(
            btnTitle: NSLocalizedString(
                "Добавить",
                comment: "Добавить"
            ),
            funcWhenPressed: presenter?.handleAddGift
        )
        
        setupEmailField()
    }
    
    private func setupEmailField() {
        wrapperView.addSubview(titleField)
        
        NSLayoutConstraint.activate([
            titleField.topAnchor.constraint(
                equalTo: subtitleLabel.bottomAnchor,
                constant: 20
            ),
            titleField.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor),
            titleField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            titleField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
        ])
    }
}
