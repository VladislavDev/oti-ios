//
//  AddGiftPresenter.swift
//  Oti
//
//  Created by Vlad on 9/5/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

protocol AddGiftVCProtocol: class, DismissModalProtocol {
    var giftType: GiftType { get set }
    var titleField: WrappedTextField { get set }
}

protocol AddGiftPresenterProtocol {
    init(controller: AddGiftVCProtocol)
    
    func handleAddGift()
}

class AddGiftPresenter: AddGiftPresenterProtocol {
    private unowned var controller: AddGiftVCProtocol
    
    required init(controller: AddGiftVCProtocol) {
        self.controller = controller
    }
    
    func handleAddGift() {
        let titleField = controller.titleField
        let valResTitleField = titleField.validateField()
        
        if valResTitleField {
            store.dispatch(addingGiftRequestAction(
                    giftType: controller.giftType,
                    title: titleField.getText()!
                ) { [unowned self] in
                    
                    store.dispatch(getGiftsListsRequestAction(cb: {
                        self.controller.dismiss(animated: true, completion: nil)
                    }))
                }
            )
        }
    }
}
