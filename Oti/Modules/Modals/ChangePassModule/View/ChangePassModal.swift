//
//  ChangePassModal.swift
//  Oti
//
//  Created by Vlad on 8/29/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

final class ChangePassModal: BaseModalController, StoreSubscriber, ChangePassVCProtocol {
    
    // MARK: - Presenter
    var presenter: ChangePassPresenterProtocol?
    
    // MARK: - Create UI elements
    var currentPassField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Текущий пароль",
            comment: "Текущий пароль"
        ),
        textContentType: .password,
        keyboardType: .default,
        validationType: PasswordValidations(),
        secure: true
    )
    
    var newPassField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Новый пароль",
            comment: "Новый пароль"
        ),
        textContentType: .password,
        keyboardType: .default,
        validationType: PasswordValidations(),
        secure: true
    )
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.authState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK: - ReSwift
    func newState(state: AuthState) {
        if let buttonWithShadow = buttonWithShadow {
            if state.isStartChangePassRequest {
                buttonWithShadow.button.showLoading()
            } else {
                buttonWithShadow.button.hideLoading()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setup() {
        titleLabel.text = NSLocalizedString(
            "Изменить пароль",
            comment: "Изменить пароль"
        )
        subtitleLabel.text = NSLocalizedString(
            "Введите новый пароль. Текущий будет изменен",
            comment: "Введите новый пароль. Текущий будет изменен"
        )
        
        buttonWithShadow = RectBtnWithShadow(
            btnTitle: NSLocalizedString(
                "Изменить",
                comment: "Изменить"
            ),
            funcWhenPressed: presenter?.handleChangePass
        )
        
        setupCurrentPassField()
        setupNewPassField()
    }
    
    private func setupCurrentPassField() {
        wrapperView.addSubview(currentPassField)
        
        NSLayoutConstraint.activate([
            currentPassField.topAnchor.constraint(
                equalTo: subtitleLabel.bottomAnchor,
                constant: 18
            ),
            currentPassField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            currentPassField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
        ])
    }
    
    private func setupNewPassField() {
        wrapperView.addSubview(newPassField)
        
        NSLayoutConstraint.activate([
            newPassField.topAnchor.constraint(
                equalTo: currentPassField.bottomAnchor,
                constant: 18
            ),
            newPassField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            newPassField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
            newPassField.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor)
        ])
    }
}

