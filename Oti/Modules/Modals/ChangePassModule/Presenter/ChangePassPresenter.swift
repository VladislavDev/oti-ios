//
//  ChangePassPresenter.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol ChangePassVCProtocol: class, DismissModalProtocol {
    var currentPassField: WrappedTextField { get set }
    var newPassField: WrappedTextField { get set }
}

protocol ChangePassPresenterProtocol {
    init(controller: ChangePassVCProtocol)
    
    func handleChangePass()
}

class ChangePassPresenter: ChangePassPresenterProtocol {
    private unowned var controller: ChangePassVCProtocol
    
    required init(controller: ChangePassVCProtocol) {
        self.controller = controller
    }
    
    func handleChangePass() {
        let currPassField = controller.currentPassField
        let newPassField = controller.newPassField
        let valResCurrPassField = currPassField.validateField()
        let valResNewPassField = newPassField.validateField()
        
        guard let userId = TokenService.shared.getAuthUserData().userId else { return }
        
        if valResCurrPassField && valResNewPassField {
            store.dispatch(changePassAction(
                userId: userId,
                oldPass: currPassField.getText()!,
                newPass: newPassField.getText()!,
                cb: {
                    self.controller.dismiss(animated: true, completion: nil)
                })
            )
        }
    }
}
