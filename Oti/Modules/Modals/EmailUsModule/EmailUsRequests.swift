//
//  EmailUsRequests.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class EmailUsRequests {
    static func emailUsRequest(
        from: String,
        subject: String,
        text: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        
        // MARK: - Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.emailUs,
            type: .POST,
            body: [
                "from": from,
                "subject": subject,
                "text": text
            ],
            cb: { (_, err) in
                // MARK: - Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: - Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
