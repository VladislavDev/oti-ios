//
//   EmailUsModal.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

final class EmailUsModal: BaseModalController, EmailUsVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: EmailUsPresenter?
    
    // MARK: - Create UI elements
    var subjectField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Тема сообщения",
            comment: "Тема сообщения"
        ),
        validationType: TextValidations()
    )
    
    var messageField: WrappedTextField = {
        var val = TextValidations()
        val.setAlgoritm(alg: StandartTitleRegex())
        
        return WrappedTextField(
            placeholder: NSLocalizedString(
                "Сообщение",
                comment: "Сообщение"
            ),
            validationType: val
        )
    }()
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.authState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK - ReSwift
    func newState(state: AuthState) {
        if let buttonWithShadow = buttonWithShadow {
            if state.isStartEmailUsRequest {
                buttonWithShadow.button.showLoading()
            } else {
                buttonWithShadow.button.hideLoading()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setup() {
        titleLabel.text = NSLocalizedString(
            "Связаться с нами",
            comment: "Связаться с нами"
        )
        subtitleLabel.text = NSLocalizedString(
            "Вы можете обратиться к нам по любому вопросу. Мы ответим вам, как только сможем.",
            comment: "Вы можете обратиться к нам по любому вопросу. Мы ответим вам, как только сможем."
        )
        
        buttonWithShadow = RectBtnWithShadow(
            btnTitle: NSLocalizedString(
                "Отправить",
                comment: "Отправить"
            ),
            funcWhenPressed: presenter?.sendMessage
        )
        
        setupCurrentPassField()
        setupNewPassField()
    }
    
    private func setupCurrentPassField() {
        wrapperView.addSubview(subjectField)
        
        NSLayoutConstraint.activate([
            subjectField.topAnchor.constraint(
                equalTo: subtitleLabel.bottomAnchor,
                constant: 20
            ),
            subjectField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            subjectField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
        ])
    }
    
    private func setupNewPassField() {
        wrapperView.addSubview(messageField)
        
        NSLayoutConstraint.activate([
            messageField.topAnchor.constraint(
                equalTo: subjectField.bottomAnchor,
                constant: 15
            ),
            messageField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            messageField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
            messageField.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor)
        ])
    }
}

