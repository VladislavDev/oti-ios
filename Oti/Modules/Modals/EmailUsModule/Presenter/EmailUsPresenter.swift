//
//  EmailUsPresenter.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol EmailUsVCProtocol: class, DismissModalProtocol {
    var subjectField: WrappedTextField { get set }
    var messageField: WrappedTextField { get set }
}

protocol EmailUsPresenterProtocol {
    init(controller: EmailUsVCProtocol)
    
    func sendMessage()
}

final class EmailUsPresenter: EmailUsPresenterProtocol {
    private unowned var controller: EmailUsVCProtocol
    
    required init(controller: EmailUsVCProtocol) {
        self.controller = controller
    }
    
    func sendMessage() {
        let subjField = controller.subjectField
        let messField = controller.messageField
        let resValSubjField = subjField.validateField()
        let resValMessField = messField.validateField()
        
        if resValSubjField && resValMessField {
            store.dispatch(emailUsReqAction(
                subject: subjField.getText()!,
                text: messField.getText()!,
                cb: {
                    self.controller.dismiss(animated: true, completion: nil)
                })
            )
        }
    }
}
