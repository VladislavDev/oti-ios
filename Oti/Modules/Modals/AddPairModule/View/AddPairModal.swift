//
//  AddPairModal.swift
//  Oti
//
//  Created by Vlad on 9/1/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

final class AddPairModal: BaseModalController, AddPairVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: AddPairPresenterProtocol?
    
    // MARK: - Create UI elements
    var emailField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Емейл партнера",
            comment: "Емейл партнера"
        ),
        textContentType: .emailAddress,
        keyboardType: .emailAddress,
        validationType: EmailValidations()
    )
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.pairState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK - ReSwift
    func newState(state: PairState) {
        if let buttonWithShadow = buttonWithShadow {
            if state.isStartAddingNewPair {
                buttonWithShadow.button.showLoading()
            } else {
                buttonWithShadow.button.hideLoading()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setup() {
        titleLabel.text = NSLocalizedString(
            "Добавить пару",
            comment: "Добавить пару"
        )
        subtitleLabel.text = NSLocalizedString(
            "Введите емейл вашего партнера. Он должен быть зарегистрирован в приложении",
            comment: "Введите емейл вашего партнера. Он должен быть зарегистрирован в приложении"
        )
        
        buttonWithShadow = RectBtnWithShadow(
            btnTitle: NSLocalizedString(
                "Добавить",
                comment: "Добавить"
            ),
            funcWhenPressed: presenter?.handleAddPair
        )
        
        setupEmailField()
    }
    
    private func setupEmailField() {
        wrapperView.addSubview(emailField)
        
        NSLayoutConstraint.activate([
            emailField.topAnchor.constraint(
                equalTo: subtitleLabel.bottomAnchor,
                constant: 20
            ),
            emailField.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor),
            emailField.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor),
            emailField.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor),
        ])
    }
}

