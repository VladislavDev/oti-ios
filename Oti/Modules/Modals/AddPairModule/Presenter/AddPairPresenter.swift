//
//  AddPairPresenter.swift
//  Oti
//
//  Created by Vlad on 9/1/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol AddPairVCProtocol: class, EmailFieldProtocol, DismissModalProtocol {
    
}

protocol AddPairPresenterProtocol {
    init(controller: AddPairVCProtocol)
    
    func handleAddPair()
}

final class AddPairPresenter: AddPairPresenterProtocol {
    private unowned var controller: AddPairVCProtocol
    
    required init(controller: AddPairVCProtocol) {
        self.controller = controller
    }
    
    func handleAddPair() {
        let emailField = controller.emailField
        let resValEmailField = emailField.validateField()
        
        if resValEmailField {
            store.dispatch(
                addNewPairRequestAction(
                    recipientEmail: emailField.getText()!,
                    cb: {
                        store.dispatch(getPairListRequestAction(cb: { _ in
                            self.controller.dismiss(animated: true, completion: nil)
                        }))
                    }
                )
            )
        }
    }
}
