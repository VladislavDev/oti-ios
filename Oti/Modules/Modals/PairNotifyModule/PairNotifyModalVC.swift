//
//  PairNotifyModalVC.swift
//  Oti
//
//  Created by Vlad on 9/27/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class PairNotifyModalVC: UIViewController {
    private var grabberView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.layer.cornerRadius = 2.5
        v.backgroundColor = Colors.grayHard
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 5),
            v.widthAnchor.constraint(equalToConstant: 36)
        ])
        
        return v
    }()
    
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ])
        
        return v
    }()
    
    private let titleLabel = LabelView(
        title: NSLocalizedString(
            "Синхронизация уведомлений в вашей паре",
            comment: "Синхронизация уведомлений в вашей паре"
        ),
        color: Colors.black,
        fontSize: FontSizes.subhead,
        weight: .bold,
        aligment: .left,
        numberOfLines: 0
    )
    
    private let subtitleLabel: LabelView = {
        let s = LabelView(
                title: NSLocalizedString(
                    "Создав общие события, вашему партнеру достаточно зайти в приложение, чтобы начать получать о них уведомления",
                comment: "Синх описание"
            ),
            color: Colors.black,
            fontSize: FontSizes.preBody,
            weight: .regular,
            aligment: .left,
            numberOfLines: 0
        )
        
        return s
    }()
    
    private let firstImg: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFill
        v.image = UIImage(named: "notifyImg")
        
        Effects.addSmallShadow(layer: v.layer)
        
        return v
    }()
    
    private let secondImg: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFill
        v.image = UIImage(named: "notify")
        
        Effects.addSmallShadow(layer: v.layer)
        
        return v
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    private func setup() {
        setupSelfView()
        setupGrabberView()
        setupWrapperView()
        setupTitleLabelView()
        setupSubtitleLabelView()
        setupFirstImg()
        setupSecondImg()
    }
    
    private func setupSelfView() {
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(Effects.addBlure(cgRect: self.view.bounds))
        
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 32,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 32
        )
    }
    
    private func setupGrabberView() {
        self.view.addSubview(grabberView)
        
        NSLayoutConstraint.activate([
            grabberView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            grabberView.topAnchor.constraint(
                equalTo: self.view.topAnchor,
                constant: 5.5
            )
        ])
    }
    
    private func setupWrapperView() {
        self.view.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.centerYAnchor.constraint(
                equalTo: self.view.centerYAnchor
//                constant: -135
            ),
            wrapperView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            wrapperView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupTitleLabelView() {
        wrapperView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.widthAnchor.constraint(
                equalToConstant: 208
            ),
            titleLabel.topAnchor.constraint(
                equalTo: wrapperView.topAnchor
            ),
            titleLabel.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            )
        ])
    }
    
    private func setupSubtitleLabelView() {
        wrapperView.addSubview(subtitleLabel)
        
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: 19
            ),
            subtitleLabel.widthAnchor.constraint(
                equalTo: wrapperView.widthAnchor, multiplier: 2.8/4
            ),
            subtitleLabel.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            ),
        ])
    }
    
    private func setupFirstImg() {
        wrapperView.addSubview(firstImg)
        
        NSLayoutConstraint.activate([
            firstImg.topAnchor.constraint(
                equalTo: titleLabel.topAnchor,
                constant: 10
            ),
            firstImg.trailingAnchor.constraint(
                equalTo: wrapperView.trailingAnchor,
                constant: 16
            )
        ])
    }
    
    private func setupSecondImg() {
        wrapperView.addSubview(secondImg)
        
        NSLayoutConstraint.activate([
            secondImg.topAnchor.constraint(
                equalTo: subtitleLabel.bottomAnchor,
                constant: 22
            ),
            secondImg.bottomAnchor.constraint(
                equalTo: wrapperView.bottomAnchor
            ),
            secondImg.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor,
                constant: -18
            )
        ])
    }
}
