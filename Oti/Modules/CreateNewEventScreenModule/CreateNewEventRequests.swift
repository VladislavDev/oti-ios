//
//  CreateNewEventRequests.swift
//  Oti
//
//  Created by Vlad on 9/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

final class CreateNewEventRequests {
    static func getIconPack(
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (([String]) -> ())?
    ) {
        // MARK: Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.iconsPack,
            type: .GET,
            cb: { (data, err) in
                // MARK: Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Parse and decode data
                guard let data = data else { return }

                let res = try? JSONDecoder().decode(
                    IconPackResponse.self,
                    from: data
                )
                
                guard let resUnwrap = res else { return }
                
                // MARK: Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest(resUnwrap.message) }
                }
            }
        )
    }
    
    static func createNewEventRequest(
        date: String,
        title: String,
        watching: String,
        icon: String,
        reminder: String,
        notify: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // MARK: Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            if let cbWhenErrorRequest = cbWhenErrorRequest {
                DispatchQueue.main.async { cbWhenErrorRequest() }
            }
            return
        }
        
        // MARK: Prepare data for group event
        let resolveRequest = store.state.pairState.pairList.resolveRequest
        var bodyForGroup: [String: Any] = [:]
        let uuid = UUID().uuidString
        
        if let resolveRequest = resolveRequest, resolveRequest.count > 0 {
            bodyForGroup = [
                "date": date,
                "title": title,
                "watching": watching,
                "icon": icon,
                "reminder": reminder,
                "notify": notify,
                "notifyId": uuid,
                "event_user_id": userId,
                "event_group_id": resolveRequest[0].group_id
            ]
        }
        
        // MARK: Prepare data for only user event
        let bodyForUser: [String: Any] = [
            "date": date,
            "title": title,
            "watching": watching,
            "icon": icon,
            "reminder": reminder,
            "notify": notify,
            "notifyId": uuid,
            "event_user_id": userId
        ]
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.addEvent,
            type: .POST,
            body: watching == "Y" && bodyForGroup.count > 0 ? bodyForGroup : bodyForUser,
            cb: { (_, err) in
                
                // MARK: Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
