//
//  CreateNewEventVC.swift
//  Oti
//
//  Created by Vlad on 9/18/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class CreateNewEventVC: UIViewController, CreateNewEventVCProtocol, StoreSubscriber {
    
    // MARK: - State
    var iconsPackNamesList: [String] = []
    
    // MARK: - Presenter
    var presenter: CreateNewEventPresenterProtocol?
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    var collectionView: UICollectionView?
    
    var titleView = WrappedTextField(
        placeholder: NSLocalizedString("Название события", comment: "Название события"),
        textContentType: .name,
        keyboardType: .default,
        validationType: TextValidations()
    )
    
    var datePickerView = CustomDatePicker(
        title: NSLocalizedString("ДАТА", comment: "Дата")
    )
    
     var notifyForView: SelectField = {
        let s = SelectField(
            title: NSLocalizedString("НАПОМНИТЬ ЗА", comment: "НАПОМНИТЬ ЗА"),
            dataList: [
                NSLocalizedString("День", comment: "День"),
                NSLocalizedString("Неделя", comment: "Неделя"),
            ],
            initialValue: nil,
            initialIdx: 1
        )
        
        return s
    }()
    
    var repetitionCycleView: SelectField = {
        let s = SelectField(
            title: NSLocalizedString("ПОВТ. КАЖДЫЙ", comment: "ПОВТ. КАЖДЫЙ"),
            dataList: [
                NSLocalizedString("Год", comment: "Год"),
                NSLocalizedString("Месяц", comment: "Месяц"),
                NSLocalizedString("Выкл", comment: "Выкл"),
            ],
            initialValue: nil,
            initialIdx: 1
        )
        
        return s
    }()
    
    private let switchTextView = LabelView(
        title: NSLocalizedString("ВИДНО ВАМ ОБОИМ", comment: "ВИДНО ВАМ ОБОИМ"),
        color: Colors.black,
        fontSize: FontSizes.footnote,
        weight: .bold,
        aligment: .left,
        numberOfLines: 0
    )
    
    var switchView = CustomSwitch()
    private var button: RectBtnWithShadow?

    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        store.subscribe(self) { subcription in
            subcription.select(StateForOpenedEventVC.init)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSelfView()
        setupAdditionToNavBar()
        setupCollectionView()
        setupTitleView()
        setupDatePickerView()
        setupNotifyForView()
        setupRepetitionCycleView()
        setupSwitchTitle()
        setupSwitchView()
        
        dismissKey()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Unsubscribe
        store.unsubscribe(self)
    }
    
    // ReSwift
    func newState(state: StateForOpenedEventVC) {
        // Set imgs list
        if state.iconsPackNames.isSuccessGettingIconsNames && iconsPackNamesList.count == 0 {
            iconsPackNamesList = state.iconsPackNames.iconsNamesList
        }
        
        // Button loader
        if state.eventsListsState.isStartGettingEventsList
            || state.eventsListsState.isStartAddingEvent {
            button?.button.showLoading()
        } else {
            button?.button.hideLoading()
        }
        
        // Button constraints
        if let resolveReq = state.pairState.pairList.resolveRequest, resolveReq.count > 0 {
            switchTextView.isHidden = false
            switchView.isHidden = false
            
            if button == nil {
                setupButtonIfHaveAPair()
            }
        } else {
            switchTextView.isHidden = true
            switchView.isHidden = true
            
            if button == nil {
                setupButtonIfNoHaveAPair()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        self.view.clipsToBounds = true
        
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 32,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 32
        )
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString("Создание события", comment: "Создание события")
        )
    }
    
    private func setupCollectionView() {
        guard let presenter = presenter else { return }
        
        collectionView = UICollectionView(
            frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 100),
            collectionViewLayout: presenter.createLayout()
        )
        
        guard
            let additionToNavBar = additionToNavBar,
            let collectionView = collectionView
        else { return }
        
        self.view.addSubview(collectionView)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.clipsToBounds = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(ImgItemCell.self, forCellWithReuseIdentifier: presenter.id_0)
        
        NSLayoutConstraint.activate([
            collectionView.heightAnchor.constraint(equalToConstant: 100),
            collectionView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 43
            ),
            collectionView.leadingAnchor.constraint(
                equalTo: self.view.leadingAnchor
            ),
            collectionView.trailingAnchor.constraint(
                equalTo: self.view.trailingAnchor
            )
        ])
    }
    
    private func setupTitleView() {
        guard let collectionView = collectionView else { return }
        
        self.view.addSubview(titleView)
        
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(
                equalTo: collectionView.bottomAnchor,
                constant: 26
            ),
            titleView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            titleView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupDatePickerView() {
        self.view.addSubview(datePickerView)
        
        NSLayoutConstraint.activate([
            datePickerView.topAnchor.constraint(
                equalTo: titleView.bottomAnchor,
                constant: 21
            ),
            datePickerView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            datePickerView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupNotifyForView() {
        self.view.addSubview(notifyForView)
        
        NSLayoutConstraint.activate([
            notifyForView.topAnchor.constraint(
                equalTo: datePickerView.bottomAnchor,
                constant: 19
            ),
            notifyForView.widthAnchor.constraint(
                equalTo: self.view.widthAnchor,
                multiplier: 2/5
            ),
            notifyForView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
        ])
    }
    
    private func setupRepetitionCycleView() {
        self.view.addSubview(repetitionCycleView)
        
        NSLayoutConstraint.activate([
            repetitionCycleView.topAnchor.constraint(
                equalTo: datePickerView.bottomAnchor,
                constant: 19
            ),
            repetitionCycleView.leadingAnchor.constraint(
                equalTo: notifyForView.trailingAnchor,
                constant: 16
            ),
            repetitionCycleView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupSwitchTitle() {
        self.view.addSubview(switchTextView)
        self.view.sendSubviewToBack(switchTextView)
        
        switchTextView.isHidden = true
        
        NSLayoutConstraint.activate([
            switchTextView.topAnchor.constraint(
                equalTo: self.notifyForView.bottomAnchor,
                constant: 38
            ),
            switchTextView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            )
        ])
    }
    
    private func setupSwitchView() {
        self.view.addSubview(switchView)
        self.view.sendSubviewToBack(switchView)
        
        switchView.isHidden = true
        
        NSLayoutConstraint.activate([
            switchView.centerYAnchor.constraint(
                equalTo: switchTextView.centerYAnchor
            ),
            switchView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupButtonIfHaveAPair() {
        button = RectBtnWithShadow(
            frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 64, height: 43),
            btnTitle: NSLocalizedString("Создать", comment: "Создать"),
            funcWhenPressed: presenter?.handleCreateEvent
        )
        
        guard let button = button else { return }
        
        self.view.addSubview(button)
        self.view.sendSubviewToBack(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(
                equalTo: switchView.bottomAnchor,
                constant: 32
            ),
            button.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            button.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupButtonIfNoHaveAPair() {
        button = RectBtnWithShadow(
            frame: CGRect(x: 0, y: 0, width: self.view.bounds.width - 64, height: 43),
            btnTitle: NSLocalizedString("Создать", comment: "Создать"),
            funcWhenPressed: presenter?.handleCreateEvent
        )
        
        guard let button = button else { return }
        
        self.view.addSubview(button)
        self.view.sendSubviewToBack(button)
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(
                equalTo: notifyForView.bottomAnchor,
                constant: 37
            ),
            button.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            button.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
}

extension CreateNewEventVC: UICollectionViewDelegate {
    // Select img
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Apply styles and save selected path img
        presenter?.applyStylesAndSaveSelectedIndexPath(indexPath: indexPath)
        
        // Deselect default established img (first img)
        presenter?.deselectDefaultEstablishedItem()
    }
    
    // Deselect imgs
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        presenter?.deselectItems(indexPath: indexPath)
    }
}

extension CreateNewEventVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconsPackNamesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let presenter = presenter,
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: presenter.id_0,
                for: indexPath
            ) as? ImgItemCell
        else {
            return UICollectionViewCell()
        }
        
        presenter.configureCellItem(indexPath: indexPath, cell: cell)
        
        return cell
    }
}
