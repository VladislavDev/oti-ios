//
//  CreateNewEventPresenter.swift
//  Oti
//
//  Created by Vlad on 9/24/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol CreateNewEventVCProtocol: class, TitleViewProtocol, NotifyForViewProtocol, DatePickerViewProtocol, RepetitionCycleViewProtocol, SwitchViewProtocol, IconsPackNamesListProtocol, NavigationControllerProtocol, CollectionViewProtocol {
    
}

protocol CreateNewEventPresenterProtocol {
    init(controller: CreateNewEventVCProtocol)
    
    var id_0: String { get set }
    
    func createSection() -> NSCollectionLayoutSection
    func createLayout() -> UICollectionViewLayout
    func applyStylesAndSaveSelectedIndexPath(indexPath: IndexPath)
    func deselectDefaultEstablishedItem()
    func deselectItems(indexPath: IndexPath)
    func configureCellItem(indexPath: IndexPath, cell: ImgItemCell)
    func handleCreateEvent()
}

final class CreateNewEventPresenter: CreateNewEventPresenterProtocol {
    var id_0 = "id_0"
    
    private unowned var controller: CreateNewEventVCProtocol
    private var selectedItemIndexPath: IndexPath?
    
    init(controller: CreateNewEventVCProtocol) {
        self.controller = controller
    }
    
    // MARK: - Create layout
    func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { [unowned self] (sectionIdx, _) -> NSCollectionLayoutSection? in
            let section = OpenedEventSectionKind(rawValue: sectionIdx)!
            
            switch section {
            case .imgs: return self.createSection()
            }
        }
        
        return layout
    }
    
    // MARK: - Create sections
    func createSection() -> NSCollectionLayoutSection {
        // Configure item
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        // Configure group
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .estimated(106),
            heightDimension: .estimated(100)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item]
        )
        
        // Configure section
        let section = NSCollectionLayoutSection(group: group)
        
        section.orthogonalScrollingBehavior = .continuous
        section.interGroupSpacing = 16
        
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 0,
            leading: 32,
            bottom: 0,
            trailing: 32
        )
        
        return section
    }
    
    func applyStylesAndSaveSelectedIndexPath(indexPath: IndexPath) {
        if let cell = controller.collectionView?.cellForItem(at: indexPath) as? ImgItemCell {
            cell.setSelected()
            
            selectedItemIndexPath = indexPath
        }
    }
    
    func deselectDefaultEstablishedItem() {
        if let selectedItemIndexPath = selectedItemIndexPath, selectedItemIndexPath.row != 0 {
            if let cell = controller.collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? ImgItemCell {
                cell.setDeselected()
            }
        }
    }
    
    func deselectItems(indexPath: IndexPath) {
        if let cell = controller.collectionView?.cellForItem(at: indexPath) as? ImgItemCell {
            cell.setDeselected()
            
            selectedItemIndexPath = nil
        }
    }
    
    func configureCellItem(indexPath: IndexPath, cell: ImgItemCell) {        
        guard
            let url = URL(string: "\(URLs.api)/static/images_pack2/\(controller.iconsPackNamesList[indexPath.row])")
        else { return }
        
        cell.illustrationView.sd_setImage(with: url, completed: nil)
    
        // If no selected img set selected first img
        if selectedItemIndexPath == nil && indexPath.row == 0 {
            cell.setSelected()
            selectedItemIndexPath = IndexPath(row: 0, section: 0)
        }
        
        // If selected row == dequeue cell row set it selected (it is necessary that during scrolling
        // the selected cell does not lose styles)
        if selectedItemIndexPath != nil && indexPath.row == selectedItemIndexPath?.row {
            cell.setSelected()
        }
    }
    
    func handleCreateEvent() {
        // validate title
        let isValidTitle = controller.titleView.validateField()
        
        guard let selectedItemIndexPath = selectedItemIndexPath else { return }
        
        if isValidTitle {
            let baseDate = controller.datePickerView.getDate()
            
            // MARK: Prepare data for new event
            let newEventImgName = controller.iconsPackNamesList[selectedItemIndexPath.row]
            guard let newEventTitle = controller.titleView.getText() else { return }
            
            let newEventDate = WorkWithDate.getFormettedDate(
                format: .mdyWithLodash,
                date: baseDate
            )
            
            let newEventNotifyForRes = NotifyFor.allCases[controller.notifyForView.getIdxBtn()].rawValue
            let newEventRepitCycleRes = RepetitionCycle.allCases[controller.repetitionCycleView.getIdxBtn()].rawValue
            let newEventIsSeeAll = controller.switchView.isOn()
            
            // MARK: Create new event
            store.dispatch(addEventRequestAction(
                date: newEventDate,
                title: newEventTitle,
                watching: newEventIsSeeAll ? "Y" : "N",
                icon: newEventImgName,
                reminder: newEventRepitCycleRes,
                notify: newEventNotifyForRes,
                cb: {
                    store.dispatch(getEventsListRequestAction(
                        year: store.state.monthFilterState.year,
                        month: store.state.monthFilterState.monthIdx,
                        cb: { [unowned self] in
                            self.controller.navigationController?.popToRootViewController(animated: true)
                        })
                    )
                }
            ))
        }
    }
}
