//
//  CreateAccRequests.swift
//  Oti
//
//  Created by Vlad on 8/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class CreateAccRequests {
    static func createAccRequest(
        email: String,
        pass: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        
        // MARK: - Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.createAcc,
            type: .POST,
            body: [
                "email": email,
                "pass": pass
            ],
            cb: { (_, err) in
                // MARK: - Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: - Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
