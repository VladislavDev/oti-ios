//
//  CreateAccVC.swift
//  Oti
//
//  Created by Vlad on 8/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class CreateAccVC: UIViewController, CreateAccVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: CreateAccPresenterProtocol?
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    private var screenLabelView = LabelView(
        title: NSLocalizedString(
            "Зачем?",
            comment: "Зачем?"
        ),
        color: Colors.black,
        fontSize: FontSizes.large,
        weight: .bold,
        aligment: .center,
        numberOfLines: 0
    )
    
    private var subtitleView = LabelView(
        title: NSLocalizedString(
            "Регистрация нужна чтобы вы могли добавить пару",
            comment: "Регистрация нужна чтобы вы могли добавить пару"
        ),
        color: Colors.grayHard,
        fontSize: FontSizes.preBody,
        weight: .regular,
        aligment: .center,
        numberOfLines: 0
    )
    
    var emailField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Email",
            comment: "Email"
        ),
        textContentType: .username,
        keyboardType: .emailAddress,
        validationType: EmailValidations()
    )
    
    var passField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Пароль",
            comment: "Пароль"
        ),
        textContentType: .newPassword,
        keyboardType: .default,
        validationType: PasswordValidations(),
        secure: true
    )
    
    var repeatPassField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Повторите пароль",
            comment: "Повторите пароль"
        ),
        textContentType: .newPassword,
        keyboardType: .default,
        validationType: PasswordValidations(),
        secure: true
    )
    
    private var createBtn = RectBtnWithShadow(
        btnTitle: NSLocalizedString(
            "Готово",
            comment: "Готово"
        )
    )
    
    private var privacyLabelView = LabelView(
        title: NSLocalizedString(
            "Регистрируясь вы соглашаетесь с",
            comment: "Регистрируясь вы соглашаетесь с"
        ),
        color: Colors.grayHard,
        fontSize: FontSizes.footnote,
        weight: .regular,
        aligment: .center
    )
    
    private var privacyBtn = UnderlineBtn(
        btnTitle: NSLocalizedString(
            "политикой конфиденциальности",
            comment: "политикой конфиденциальности"
        )
    )
    
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.authState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSelfView()
        setupAdditionToNavBar()
        setupFormLabelView()
        setupSubtitleView()
        setupFields()
        setupCreateBtn()
        setupPrivacyViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    // ReSwift
    func newState(state: AuthState) {
        if state.isStartCreateAccRequest {
            createBtn.button.showLoading()
        } else {
            createBtn.button.hideLoading()
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.view.backgroundColor = .white
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 57,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 57
        )
        
        dismissKey()
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString("Создание аккаунта", comment: "Создание аккаунта")
        )
    }
    
    private func setupFormLabelView() {
        screenLabelView.numberOfLines = 0
        
        self.view.addSubview(screenLabelView)
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            screenLabelView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 51
            ),
            screenLabelView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            screenLabelView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupSubtitleView() {
        self.view.addSubview(subtitleView)
        
        NSLayoutConstraint.activate([
            subtitleView.centerXAnchor.constraint(
                equalTo: screenLabelView.centerXAnchor
            ),
            subtitleView.topAnchor.constraint(
                equalTo: screenLabelView.bottomAnchor,
                constant: 18
            ),
            subtitleView.widthAnchor.constraint(
                equalToConstant: 213
            ),
        ])
    }
    
    private func setupFields() {
        self.view.addSubview(emailField)
        self.view.addSubview(passField)
        self.view.addSubview(repeatPassField)
        
        NSLayoutConstraint.activate([
            emailField.topAnchor.constraint(
                equalTo: subtitleView.bottomAnchor,
                constant: 30
            ),
            emailField.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            emailField.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
            
            passField.topAnchor.constraint(
                equalTo: emailField.bottomAnchor,
                constant: 16
            ),
            passField.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            passField.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
            
            repeatPassField.topAnchor.constraint(
                equalTo: passField.bottomAnchor,
                constant: 17
            ),
            repeatPassField.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            repeatPassField.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupCreateBtn() {
        createBtn.button.funcWhenPressed = presenter?.handleCreateAccBtn
        
        self.view.addSubview(createBtn)
        
        NSLayoutConstraint.activate([
            createBtn.topAnchor.constraint(
                equalTo: repeatPassField.bottomAnchor,
                constant: 35
            ),
            createBtn.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            )
        ])
    }
    
    private func setupPrivacyViews() {
        privacyBtn.funcWhenPressed = presenter?.handleShowPrivacyPolicyBtn
        
        self.view.addSubview(privacyLabelView)
        self.view.addSubview(privacyBtn)
        
        NSLayoutConstraint.activate([
            privacyBtn.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            ),
            privacyBtn.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                constant: -30
            ),
            
            privacyLabelView.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            ),
            privacyLabelView.bottomAnchor.constraint(
                equalTo: privacyBtn.topAnchor,
                constant: 6
            )
        ])
    }
}
