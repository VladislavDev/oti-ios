//
//  CreateAccPresenter.swift
//  Oti
//
//  Created by Vlad on 8/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol CreateAccVCProtocol: class, EmailFieldProtocol, PassFieldProtocol {
    var repeatPassField: WrappedTextField { get set }
}

protocol CreateAccPresenterProtocol: class {
    init(controller: CreateAccVCProtocol)
    
    func handleCreateAccBtn()
    func handleShowPrivacyPolicyBtn()
}

class CreateAccPresenter: CreateAccPresenterProtocol {
    private unowned var controller: CreateAccVCProtocol
    
    required init(controller: CreateAccVCProtocol) {
        self.controller = controller
    }
    
    func handleCreateAccBtn() {
        let emailField = controller.emailField
        let passField = controller.passField
        let repeatPassField = controller.repeatPassField
        
        let resValidateEmailField = emailField.validateField()
        let resValidatePassField = passField.validateField()
        let resValidateRepeatPassField = repeatPassField.validateField()
        
        if resValidateEmailField && resValidatePassField && resValidateRepeatPassField {
            let emailText = emailField.getText()!
            let passText = passField.getText()!
            
            if passText == repeatPassField.getText() {
                store.dispatch(createAccRequestAction(email: emailText, pass: passText))
                
            } else {
                Snackbar.shared.showSnackbar(
                    title: "Passwords must match",
                    type: .error
                )
            }
        }
    }
    
    func handleShowPrivacyPolicyBtn() {
        if let url = URL(string: "https://your-dates.ru") {
            UIApplication.shared.open(url)
        }
    }
}
