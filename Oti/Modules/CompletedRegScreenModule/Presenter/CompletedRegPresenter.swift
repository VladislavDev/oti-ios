//
//  CompletedRegPresenter.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol CompletedRegVCProtocol: class, NavigationControllerProtocol {
    
}

protocol CompletedRegPresenterProtocol {
    init(controller: CompletedRegVCProtocol)
    
    func handleGoToLoginBtn()
}

class CompletedRegPresenter: CompletedRegPresenterProtocol {
    private unowned var controller: CompletedRegVCProtocol
    
    required init(controller: CompletedRegVCProtocol) {
        self.controller = controller
    }
    
    func handleGoToLoginBtn() {
        DispatchQueue.main.async { [unowned _self = self] in
            _self.controller.navigationController?.popToRootViewController(animated: true)
        }
    }
}
