//
//  CompletedVC.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CompletedRegVC: UIViewController, CompletedRegVCProtocol {
    
    // MARK: - Presenter
    var presenter: CompletedRegPresenterProtocol?
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 174)
        ])
        
        return v
    }()
    
    private var formTitle = LabelView(
        title: NSLocalizedString(
            "Готово",
            comment: "Готово"
        ),
        color: Colors.black,
        fontSize: FontSizes.large,
        weight: .bold,
        aligment: .center,
        numberOfLines: 0
    )
    
    private var formSubTitle = LabelView(
        title: NSLocalizedString(
            "Проверьте ваш email и перейдите по ссылке из письма",
            comment: "Проверьте ваш email и перейдите по ссылке из письма"
        ),
        color: Colors.grayHard,
        fontSize: FontSizes.preBody,
        weight: .regular,
        aligment: .center,
        numberOfLines: 0
    )
    
    private var goToLoginBtn = RectBtn(
        btnTitle: NSLocalizedString(
            "На главную",
            comment: "На главную"
        )
    )

    // MARK: - VC lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSelfView()
        setupWrapperView()
        setupFormTitle()
        setupFormSubTitle()
        setupGoToLoginBtn()
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        
        setupAdditionToNavBar()
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString("Готово", comment: "Готово")
        )
    }
    
    private func setupWrapperView() {
        self.view.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            wrapperView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            wrapperView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupFormTitle() {
        wrapperView.addSubview(formTitle)
        
        NSLayoutConstraint.activate([
            formTitle.topAnchor.constraint(equalTo: wrapperView.topAnchor),
            formTitle.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            ),
            formTitle.trailingAnchor.constraint(
                equalTo: wrapperView.trailingAnchor
            ),
        ])
    }
    
    private func setupFormSubTitle() {
        wrapperView.addSubview(formSubTitle)
        
        NSLayoutConstraint.activate([
            formSubTitle.centerXAnchor.constraint(
                equalTo: wrapperView.centerXAnchor
            ),
            formSubTitle.topAnchor.constraint(
                equalTo: formTitle.bottomAnchor,
                constant: 10
            ),
            formSubTitle.widthAnchor.constraint(
                equalToConstant: 243
            ),
        ])
    }
    
    private func setupGoToLoginBtn() {
        goToLoginBtn.funcWhenPressed = presenter?.handleGoToLoginBtn
        
        wrapperView.addSubview(goToLoginBtn)
        
        NSLayoutConstraint.activate([
            goToLoginBtn.bottomAnchor.constraint(
                equalTo: wrapperView.bottomAnchor
            ),
            goToLoginBtn.centerXAnchor.constraint(
                equalTo: wrapperView.centerXAnchor
            )
        ])
    }
}
