//
//  CheckTokenVC.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class AccountSettingsVC: UIViewController, AccSettingsVCProtocol {
    
    // MARK: - Presenter
    var presenter: AccSettingsPresenterProtocol?
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    var tableView: UITableView = {
        let t = UITableView()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.backgroundColor = .clear
        
        return t
    }()

    // MARK: - VC lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSelfView()
        setupAdditionToNavBar()
        setupTableView()
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.tabBarItem = CustomTabBarUtils.createTabBarItem(
            imageNamed: "settingsNormal",
            tag: 3
        )
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString(
                "Настройки аккаунта",
                comment: "Настройки аккаунта"
            )
        )
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.clipsToBounds = false
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(
                equalTo: additionToNavBar!.bottomAnchor,
                constant: 42
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    // MARK: - Actions(not ReSwift)
    private func showAlertLogout() {
        let alert = UIAlertController(
            title: NSLocalizedString(
                "Выйти",
                comment: "Выйти"
            ),
            message: NSLocalizedString(
                "Вы уверены что хотите выйти из аккаунта?",
                comment: "Вы уверены что хотите выйти из аккаунта?"
            ),
            preferredStyle: .alert
        )
        
        let actionCancel = UIAlertAction(
            title: NSLocalizedString("Отмена", comment: "Отмена"),
            style: .cancel,
            handler: nil
        )
        
        let actionLogout = UIAlertAction(
            title: NSLocalizedString("Выйти", comment: "Выйти"),
            style: .destructive,
            handler: { _ in
                self.presenter?.handleLogOut()
            }
        )
        
        alert.addAction(actionCancel)
        alert.addAction(actionLogout)
        
        present(alert, animated: true, completion: nil)
    }
    
    private func showAlertDeleteAcc() {
        let alert = UIAlertController(
            title: NSLocalizedString(
                "Удаление аккаунта",
                comment: "Удаление аккаунта"
            ),
            message: NSLocalizedString(
                "Вы уверены что хотите удалить аккаунт? Все данные будут удалены",
                comment: "Вы уверены что хотите удалить аккаунт? Все данные будут удалены"
            ),
            preferredStyle: .alert
        )
        
        let actionCancel = UIAlertAction(
            title: NSLocalizedString("Отмена", comment: "Отмена"),
            style: .cancel,
            handler: nil
        )
        
        let actionDelete = UIAlertAction(
            title: NSLocalizedString("Удалить", comment: "Удалить"),
            style: .destructive,
            handler: { _ in
                self.presenter?.handleDeleteAccount()
            }
        )
        
        alert.addAction(actionCancel)
        alert.addAction(actionDelete)
        
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Table settings
extension AccountSettingsVC: UITableViewDelegate {
    // Height cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}

extension AccountSettingsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BasicCell()
        cell.isShowDisclosure = true
        
        switch indexPath.row {
        case 0:
            cell.titleItem = NSLocalizedString(
                "Настройки пары",
                comment: "Настройки пары"
            )
            return cell
            
        case 1:
            cell.titleItem = NSLocalizedString(
                "Изменение данных",
                comment: "Изменение данных"
            )
            return cell
            
        case 2:
            cell.titleItem = NSLocalizedString(
                "Связаться с нами",
                comment: "Связаться с нами"
            )
            return cell
            
        case 3:
            cell.titleItem = NSLocalizedString(
                "Выйти",
                comment: "Выйти"
            )
            cell.titleLabel.textColor = Colors.red
            return cell
            
        case 4:
            cell.titleItem = NSLocalizedString(
                "Удалить аккаунт",
                comment: "Удалить аккаунт"
            )
            cell.titleLabel.textColor = Colors.red
            return cell
            
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenter = presenter else { return }
        
        switch indexPath.row {
        case 0: presenter.handleGoToPairSettings()
        case 1: presenter.handleGoToDataSettings()
        case 2:
            // This is necessary to avoid delay when the modal
            // window appears from the table.
            // Reason for the delay - cell.selectionStyle
            DispatchQueue.main.async {
                presenter.handleEmailUs()
            }
        case 3:
            // This is necessary to avoid delay when the modal
            // window appears from the table.
            // Reason for the delay - cell.selectionStyle
            DispatchQueue.main.async {
                self.showAlertLogout()
            }
        case 4:
            // This is necessary to avoid delay when the modal
            // window appears from the table.
            // Reason for the delay - cell.selectionStyle
            DispatchQueue.main.async {
                self.showAlertDeleteAcc()
            }
        default: break
        }
    }
}
