//
//  AccountSettingsPresenter.swift
//  Oti
//
//  Created by Vlad on 8/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import BackgroundTasks

protocol AccSettingsVCProtocol: class, NavigationControllerProtocol {
    
}

protocol AccSettingsPresenterProtocol {
    init(controller: AccSettingsVCProtocol)
    
    func handleGoToPairSettings()
    func handleGoToDataSettings()
    func handleLogOut()
    func handleDeleteAccount()
    func handleEmailUs()
}

class AccSettingsPresenter: AccSettingsPresenterProtocol {
    private unowned var controller: AccSettingsVCProtocol
    
    required init(controller: AccSettingsVCProtocol) {
        self.controller = controller
    }
    
    func handleGoToPairSettings() {
        let pairSettingsModule = ModuleBuilder.createPairSettingsModule()
        
        self.controller.navigationController?.pushViewController(
            pairSettingsModule,
            animated: true
        )
    }
    
    func handleGoToDataSettings() {
        let dataSettingsModule = ModuleBuilder.createDataSettingsModule()
        
        self.controller.navigationController?.pushViewController(
            dataSettingsModule,
            animated: true
        )
    }
    
    func handleLogOut() {
        let loginModule = CustomNavController(rootViewController: ModuleBuilder.createLoginModule())
        
        TokenService.shared.removeAuthUserData()
        
        // MARK: - Remove all local notifications
        LocalNotifications.shared.removeAllNotify()
        BGTaskScheduler.shared.cancelAllTaskRequests()
        
        UIApplication.setRootView(loginModule)
    }
    
    func handleDeleteAccount() {
        AccSettingsRequests.deleteAccountRequest(
            cbWhenStartRequest: nil,
            cbWhenErrorRequest: nil,
            cbWhenSuccessRequest: {
                self.handleLogOut()
            }
        )
    }
    
    func handleEmailUs() {
        self.controller.navigationController?.present(
            ModuleBuilder.createEmailUSModule(),
            animated: true,
            completion: nil
        )
    }
}
