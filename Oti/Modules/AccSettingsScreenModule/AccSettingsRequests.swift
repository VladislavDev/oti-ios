//
//  AccSettingsRequests.swift
//  Oti
//
//  Created by Vlad on 8/26/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation
import BackgroundTasks

class AccSettingsRequests {
    static func deleteAccountRequest(
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // MARK: - Start request
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            return
        }
        
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.deleteAcc,
            type: .POST,
            body: [
                "userId": userId
            ],
            cb: { (_, err) in
                // MARK: - Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: - Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
                
                // MARK: - Remove all local notifications
                LocalNotifications.shared.removeAllNotify()
                BGTaskScheduler.shared.cancelAllTaskRequests()
            }
        )
    }
}
