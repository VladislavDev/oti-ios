//
//  ForgotPassRequests.swift
//  Oti
//
//  Created by Vlad on 8/26/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class ForgotPassRequests {
    static func forgotPassRequest(
        email: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        
        // MARK: - Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.restorePass,
            type: .POST,
            body: ["to": email],
            cb: { (_, err) in
                // MARK: - Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: - Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
