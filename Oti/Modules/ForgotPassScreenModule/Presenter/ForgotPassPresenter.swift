//
//  ForgotPassPresenter.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol ForgotPassVCProtocol: class, NavigationControllerProtocol, EmailFieldProtocol {
    
}

protocol ForgotPassPresenterProtocol {
    init(controller: ForgotPassVCProtocol)
    
    func handleRestoreBtn()
}

class ForgotPassPresenter: ForgotPassPresenterProtocol {
    private unowned var controller: ForgotPassVCProtocol
    
    required init(controller: ForgotPassVCProtocol) {
        self.controller = controller
    }
    
    func handleRestoreBtn() {
        let emailField = controller.emailField
        
        if emailField.validateField() {
            store.dispatch(forgotPassRequestAction(
                            email: emailField.getText()!,
                            cb: {
                                self.controller.navigationController?.pushViewController(
                                    ModuleBuilder.createCompletedVCModule(),
                                    animated: true
                                )
                            })
            )
        }
    }
}
