//
//  ForgotPassVC.swift
//  Oti
//
//  Created by Vlad on 8/23/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class ForgotPassVC: UIViewController, ForgotPassVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: ForgotPassPresenterProtocol?
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 276)
        ])
        
        return v
    }()
    
    private var formTitle = LabelView(
        title: NSLocalizedString(
            "Забыли пароль?",
            comment: "Забыли пароль?"
        ),
        color: Colors.black,
        fontSize: FontSizes.large,
        weight: .bold,
        aligment: .center,
        numberOfLines: 0
    )
    
    private var formSubTitle = LabelView(
        title: NSLocalizedString(
            "Введите ваш емейл и вы отправим письмо с ссылкой для восстановления",
            comment: "Введите ваш емейл и вы отправим письмо с ссылкой для восстановления"
        ),
        color: Colors.grayHard,
        fontSize: FontSizes.footnote,
        weight: .regular,
        aligment: .center,
        numberOfLines: 0
    )
    
    var emailField = WrappedTextField(
        placeholder: NSLocalizedString(
            "Email",
            comment: "Email"
        ),
        textContentType: .emailAddress,
        keyboardType: .emailAddress,
        validationType: EmailValidations()
    )
    
    private var restoreBtnWithShadow = RectBtnWithShadow(
        btnTitle: NSLocalizedString(
            "Восстановить",
            comment: "Восстановить"
        )
    )

    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.authState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSelfView()
        setupAdditionToNavBar()
        setupWrapperView()
        setupFormTitle()
        setupFormSubTitle()
        setupEmailField()
        setupRestoreBtn()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    // MARK: - ReSwift
    func newState(state: AuthState) {
        if state.isStartForgotPassRequest {
            restoreBtnWithShadow.button.showLoading()
        } else {
            restoreBtnWithShadow.button.hideLoading()
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 57,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 57
        )
        
        dismissKey()
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString(
                "Восстановить пароль",
                comment: "Восстановить пароль"
            )
        )
    }
    
    private func setupWrapperView() {
        self.view.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.centerYAnchor.constraint(
                equalTo: self.view.centerYAnchor,
                constant: -45
            ),
            wrapperView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            wrapperView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupFormTitle() {
        wrapperView.addSubview(formTitle)
        
        NSLayoutConstraint.activate([
            formTitle.topAnchor.constraint(equalTo: wrapperView.topAnchor),
            formTitle.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            ),
            formTitle.trailingAnchor.constraint(
                equalTo: wrapperView.trailingAnchor
            ),
        ])
    }
    
    private func setupFormSubTitle() {
        wrapperView.addSubview(formSubTitle)
        
        NSLayoutConstraint.activate([
            formSubTitle.centerXAnchor.constraint(
                equalTo: wrapperView.centerXAnchor
            ),
            formSubTitle.topAnchor.constraint(
                equalTo: formTitle.bottomAnchor,
                constant: 17
            ),
            formSubTitle.widthAnchor.constraint(
                equalTo: wrapperView.widthAnchor
            ),
        ])
    }
    
    private func setupEmailField() {
        wrapperView.addSubview(emailField)
        
        NSLayoutConstraint.activate([
            emailField.topAnchor.constraint(
                equalTo: formSubTitle.bottomAnchor,
                constant: 30
            ),
            emailField.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            ),
            emailField.trailingAnchor.constraint(
                equalTo: wrapperView.trailingAnchor
            ),
        ])
    }
    
    private func setupRestoreBtn() {
        restoreBtnWithShadow.button.funcWhenPressed = presenter?.handleRestoreBtn
        
        wrapperView.addSubview(restoreBtnWithShadow)
        
        NSLayoutConstraint.activate([
            restoreBtnWithShadow.bottomAnchor.constraint(
                equalTo: wrapperView.bottomAnchor,
                constant: -5
            ),
            restoreBtnWithShadow.centerXAnchor.constraint(
                equalTo: wrapperView.centerXAnchor
            )
        ])
    }
}
