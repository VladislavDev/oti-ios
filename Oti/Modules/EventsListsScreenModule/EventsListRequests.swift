//
//  EventsListRequests.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class EventsListRequests {
    static func getEventsListsRequest(
        year: String,
        month: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (([Event]) -> ())?
    ) {
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            // Return error
            if let cbWhenErrorRequest = cbWhenErrorRequest {
                DispatchQueue.main.async { cbWhenErrorRequest() }
            }
            return
        }
        
        // -1 just placeholder. MySQL won't find anything by this number.
        // The decision to add -1 was made to simplify the code. Consider it a dirty hack
        var groupId: Int = -1
        let resolveReq = store.state.pairState.pairList.resolveRequest
        let resolveReqCnt = resolveReq?.count
        if let resolveReqCnt = resolveReqCnt, resolveReqCnt > 0 {
            if let id = resolveReq?[0].group_id {
                groupId = id
            }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.getEventsLists,
            type: .POST,
            body: [
                "year": year,
                "month": month,
                "userId": userId,
                "groupId": groupId
            ],
            cb: { (data, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Parse and decode data
                guard let data = data else { return }

                let res = try? JSONDecoder().decode(
                    EventsListsResponse.self,
                    from: data
                )
                
                guard let resUnwrap = res else { return }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest(resUnwrap.message) }
                }
            }
        )
    }

}
