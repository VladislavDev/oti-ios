//
//  EventListModel.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct EventsListsResponse: Decodable {
    let status: Int
    let message: [Event]
}

// MARK: - Message
struct Event: Decodable, Hashable {
    let event_id: Int
    let title, seeEveryoneInThePair, dateEvent, iconPath: String
    let remindEvery: String
    let notifyInA: String?
    let notifyId: String?
    let event_user_id: Int?
    let event_group_id: Int?
}
