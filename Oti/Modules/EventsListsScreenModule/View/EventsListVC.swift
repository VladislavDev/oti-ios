//
//  CheckTokenVC.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

struct StateForEventsListsScreen {
    var pairState: PairState
    var eventsListsState: EventsListsState

    init(state: AppState) {
        self.pairState = state.pairState
        self.eventsListsState = state.eventsListsState
    }
}

class EventsListVC: UIViewController, StoreSubscriber, EventListVCProtocol {
    
    // MARK: - Presenter
    var presenter: EventListPresenterProtocol?
    
    // MARK: - Events lists
    var collectionView: UICollectionView?
    var dataSource: UICollectionViewDiffableDataSource<EventListSectionKind, Event>! = nil
    
    var successGettingEventsList = false
    var todayEvents: [Event] = []
    var nextEvents: [Event] = []
    var prevEvents: [Event] = []
    
    // MARK: - Create UI elements
    var monthFilter = MonthFilter()
    var additionToNavBar: AdditionToNavBar?
    var refreshControl = UIRefreshControl()
    
    private var vStackView: UIStackView = {
        let s = UIStackView()
        
        s.translatesAutoresizingMaskIntoConstraints = false
        
        s.axis = .vertical
        s.spacing = 16
        
        NSLayoutConstraint.activate([
            s.widthAnchor.constraint(equalToConstant: 55)
        ])
        
        return s
    }()
    
    private var infoNotifyBtn = RoundBtn(type: .info)
    private var createNewEventBtn = RoundBtn(type: .createEvent)
    private var addPairBtn = RoundBtn(type: .addPair)
    private var existNewUserRequestBtn = RoundBtn(type: .requestFromPair)
    
    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        store.subscribe(self) { subcription in
            subcription.select(StateForEventsListsScreen.init)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBaseSettings()
        setupMonthFilter()
        setupVStackView()
        
        guard let presenter = presenter else { return }
        
        presenter.setupCollectionView()
        presenter.setupDataSource()
        presenter.setupRefreshControl()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    // MARK: - ReSwift
    func newState(state: StateForEventsListsScreen) {
        let eventsListsIsLoaded = state.eventsListsState.isSuccessGettingEventsList
        let eventListsIsErrorLoaded = state.eventsListsState.isErrorGettingEventsList
        
        let newTodayEvents = state.eventsListsState.todayEventsList
        let newNextEvents = state.eventsListsState.nextEventsList
        let newPrevEevents = state.eventsListsState.prevEventsList
        
        if eventListsIsErrorLoaded {
            collectionView?.refreshControl?.endRefreshing()
        }
        
        // MARK: Send into MonthFilter is loaded event lists data.
        // It is need for waiting when events lists data will be loaded, and then
        // may tap on < and > btns. To avoid chash app.
        monthFilter.isLoadedData = eventsListsIsLoaded
        successGettingEventsList = eventsListsIsLoaded
        
        // MARK: Reload settings
        guard let presenter = presenter else { return }
        
        todayEvents = state.eventsListsState.todayEventsList
        nextEvents = state.eventsListsState.nextEventsList
        prevEvents = state.eventsListsState.prevEventsList
        
        presenter.reloadData()
        
        // MARK: If all new events counts equal 0 show "add new event card"
        if newTodayEvents.count == 0
            && newNextEvents.count == 0
            && newPrevEevents.count == 0
            && eventsListsIsLoaded
            && !eventListsIsErrorLoaded {
            presenter.showEmptyCell()
        }
        
        // MARK: Update sections labels
        presenter.updateAllSectionsSubtitles()
        
        // MARK: Toggle fast btns
        let pairList = state.pairState.pairList
        
        if let recipReqs = pairList.recipientsRequests, recipReqs.count > 0 {
            existNewUserRequestBtn.isHidden = false
        } else {
            existNewUserRequestBtn.isHidden = true
        }
        
        if let resolveReqs = pairList.resolveRequest, resolveReqs.count == 0 {
            addPairBtn.isHidden = false
            infoNotifyBtn.isHidden = true
        } else {
            addPairBtn.isHidden = true
            infoNotifyBtn.isHidden = false
        }
    }
    
    // MARK: - UI elements ancors
    private func setupBaseSettings() {
        // Setup title
        self.title = NSLocalizedString("События", comment: "События")
        
        // Setup tab bar item
        self.tabBarItem = CustomTabBarUtils.createTabBarItem(
            imageNamed: "eventsNormal",
            tag: 1
        )
        
        // Setup AdditionToNavBar
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: nil
        )
    }
    
    private func setupMonthFilter() {
        view.addSubview(monthFilter)
        
        let marginsGuide = view.layoutMarginsGuide
        let paddingXAxis: CGFloat = 5
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            monthFilter.bottomAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor
            ),
            
            monthFilter.leadingAnchor.constraint(
                equalTo: marginsGuide.leadingAnchor,
                constant: paddingXAxis
            ),
            
            monthFilter.trailingAnchor.constraint(
                equalTo: marginsGuide.trailingAnchor,
                constant: -paddingXAxis
            ),
        ])
    }
    
    private func setupVStackView() {
        self.view.addSubview(vStackView)
        
        // infoNotifyBtn setings
        vStackView.addArrangedSubview(infoNotifyBtn)
        
        infoNotifyBtn.funcWhenPressed = { [unowned self] in
            self.present(
                ModuleBuilder.createPairNotifyModule(),
                animated: true,
                completion: nil
            )
        }
        
        
        // existNewUserRequestBtn setings
        vStackView.addArrangedSubview(existNewUserRequestBtn)
        
        existNewUserRequestBtn.funcWhenPressed = { [unowned self] in
            self.navigationController?.pushViewController(
                ModuleBuilder.createPairSettingsModule(),
                animated: true
            )
        }
        
        // addPairBtn setings
        vStackView.addArrangedSubview(addPairBtn)
        
        addPairBtn.funcWhenPressed = { [unowned self] in
            self.present(
                ModuleBuilder.createAddPairModule(),
                animated: true,
                completion: nil
            )
        }
        
        // createNewEventBtn setings
        vStackView.addArrangedSubview(createNewEventBtn)
        
        createNewEventBtn.funcWhenPressed = { [unowned self] in
            self.navigationController?.pushViewController(
                ModuleBuilder.createCreateNewEventModule(),
                animated: true
            )
        }
        
        // vStackView constraints
        NSLayoutConstraint.activate([
            vStackView.trailingAnchor.constraint(
                equalTo: self.view.trailingAnchor,
                constant: -24
            ),
            vStackView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                constant: -24
            )
        ])
    }
}

extension EventsListVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section != 3 {
            presenter?.pushEventScreen(indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        
        if let sectionHeader = view as? CollectionSectionHeader {
            switch indexPath.section {
            case 0:
                sectionHeader.labelView.text = NSLocalizedString("СЕГОДНЯ", comment: "СЕГОДНЯ")

                presenter?.updateSectionSubTitle(
                    sectionHeader: sectionHeader,
                    sectionIdx: 0,
                    dataCnt: todayEvents.count,
                    isLoadedData: successGettingEventsList
                )

            case 1:
                sectionHeader.labelView.text = NSLocalizedString("СЛЕДУЮЩИЕ", comment: "СЛЕДУЮЩИЕ")

                presenter?.updateSectionSubTitle(
                    sectionHeader: sectionHeader,
                    sectionIdx: 1,
                    dataCnt: nextEvents.count,
                    isLoadedData: successGettingEventsList
                )

            case 2:
                sectionHeader.labelView.text = NSLocalizedString("ПРОШЕДШИЕ", comment: "ПРОШЕДШИЕ")

                presenter?.updateSectionSubTitle(
                    sectionHeader: sectionHeader,
                    sectionIdx: 2,
                    dataCnt: prevEvents.count,
                    isLoadedData: successGettingEventsList
                )
                
            case 3:
                sectionHeader.labelView.text = ""
                sectionHeader.noDataLabelView.text = ""
            default: break
            }
        }
    }
}
