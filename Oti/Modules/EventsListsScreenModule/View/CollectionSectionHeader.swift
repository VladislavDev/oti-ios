//
//  CollectionSectionHeader.swift
//  Oti
//
//  Created by Vlad on 9/16/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class CollectionSectionHeader: UICollectionReusableView {
    var labelView = LabelView(
        title: "No title",
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .regular,
        aligment: .left,
        numberOfLines: 0
    )
    
    var noDataLabelView = LabelView(
        title: "",
        color: Colors.grayHard,
        fontSize: FontSizes.preBody,
        weight: .regular,
        aligment: .left,
        numberOfLines: 0
    )
    
    private var wrapperView: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        setupWrapper()
        setupLabelView()
        setupNoDataLabelView()
    }
    
    private func setupWrapper() {
        self.addSubview(wrapperView)
        
        NSLayoutConstraint.activate([
            wrapperView.heightAnchor.constraint(equalToConstant: 18),
            wrapperView.widthAnchor.constraint(equalTo: self.widthAnchor),
            wrapperView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            wrapperView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            wrapperView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    private func setupLabelView() {
        wrapperView.addSubview(labelView)
        
        NSLayoutConstraint.activate([
            labelView.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor),
            labelView.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor)
        ])
    }
    
    private func setupNoDataLabelView() {
        wrapperView.addSubview(noDataLabelView)
        
        NSLayoutConstraint.activate([
            noDataLabelView.topAnchor.constraint(
                equalTo: wrapperView.bottomAnchor,
                constant: 2
            ),
            noDataLabelView.leadingAnchor.constraint(
                equalTo: wrapperView.leadingAnchor
            ),
        ])
    }
}
