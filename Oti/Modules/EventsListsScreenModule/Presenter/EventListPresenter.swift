//
//  EventListPresenter.swift
//  Oti
//
//  Created by Vlad on 9/12/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// How many items in one row
enum EventListSectionKind: Int, CaseIterable {
    case today, next, prev, no
    var columnCount: Int {
        switch self {
        case .today: return 1
        case .next: return 2
        case .prev: return 2
        case .no: return 1
        }
    }
}

protocol EventListVCProtocol: class, UICollectionViewDelegate, NavigationControllerProtocol, CollectionViewProtocol, RefreshControlProtocol, AdditionToNavBarProtocol {
    var view: UIView! { get set }
    
    var monthFilter: MonthFilter { get set }
    var dataSource: UICollectionViewDiffableDataSource<EventListSectionKind, Event>! { get set }
    
    var successGettingEventsList: Bool { get set }
    var todayEvents: [Event] { get set }
    var nextEvents: [Event] { get set }
    var prevEvents: [Event] { get set }
}

protocol EventListPresenterProtocol {
    init(controller: EventListVCProtocol)
    
    func setupCollectionView()
    func setupDataSource()
    func reloadData()
    func setupRefreshControl()
    func pushEventScreen(indexPath: IndexPath)
    func updateSectionSubTitle(sectionHeader: CollectionSectionHeader, sectionIdx: Int, dataCnt: Int, isLoadedData: Bool)
    func updateAllSectionsSubtitles()
    func showEmptyCell()
}

final class EventListPresenter: EventListPresenterProtocol {
    // MARK: - Cell ids
    private let id_section = "section_id_0"
    private let id_0 = "id_0"
    private let id_1 = "id_1"
    private let id_2 = "id_2"
    private let id_3 = "id_3"
    
    // MARK: - Controller
    private unowned var controller: EventListVCProtocol
    
    // MARK: - Padding sections
    private let paddingLeadingAndTraling: CGFloat = 16
    
    // MARK: - Init
    init(controller: EventListVCProtocol) {
        self.controller = controller
    }
    
    // MARK: - Setup collection view
    func setupCollectionView() {
        // Create collection view
        controller.collectionView = UICollectionView(
            frame: controller.view.bounds,
            collectionViewLayout: createLayout()
        )
        
        guard let collectionView = controller.collectionView else { return }
        
        // Setup delegate(for taps)
        collectionView.delegate = controller
        
        // Setup styles
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        // Register cells
        collectionView.register(
            CollectionSectionHeader.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: id_section
        )
        collectionView.register(
            EventItemBig.self,
            forCellWithReuseIdentifier: id_0
        )
        collectionView.register(
            EventItem.self,
            forCellWithReuseIdentifier: id_1
        )
        collectionView.register(
            EventItem.self,
            forCellWithReuseIdentifier: id_2
        )
        collectionView.register(
            EmptyEventItem.self,
            forCellWithReuseIdentifier: id_3
        )
        
        // Add how subview
        controller.view.addSubview(collectionView)
        controller.view.sendSubviewToBack(collectionView)
        
        // Setup constraints
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        let safeArea = controller.view.safeAreaLayoutGuide
        
        guard let additionToNavBar = controller.additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: -30
            ),
            collectionView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
        ])
    }
    
    // MARK: - Refresh controll setting
    func setupRefreshControl() {
        guard let collectionView = controller.collectionView else { return }
        
        collectionView.refreshControl = controller.refreshControl
        controller.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        guard
            let collectionRefreshControl = collectionView.refreshControl,
            let additionToNavBar = controller.additionToNavBar
        else { return }
        
        collectionRefreshControl.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionRefreshControl.centerXAnchor.constraint(
                equalTo: controller.view.centerXAnchor
            ),
            collectionRefreshControl.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 30
            )
        ])
    }
    
    @objc private func refresh() {
        let date = controller.monthFilter.getData()
        
        let year = date["year"]
        let month = date["month"]
        
        if
            let year = year,
            let monthStr = month,
            let monthCnt = WorkWithDate.getMonthIdx(month: monthStr) {
                // Get events lists
                store.dispatch(getEventsListRequestAction(
                    year: year,
                    month: "\(monthCnt + 1)",
                    cb: { [unowned self] in
                        
                        // Get pair list
                        store.dispatch(getPairListRequestAction(cb: { resolveRequests in
                            
                            // Get pair gifts
                            guard
                                let resolveRequests = resolveRequests,
                                resolveRequests.count > 0
                            else {
                                self.controller.refreshControl.endRefreshing()
                                
                                return
                            }
                            
                            store.dispatch(getPairGiftsListsRequestAction(cb: {
                                self.controller.refreshControl.endRefreshing()
                            }))
                        }))
                    })
                )
        }
    }
    
    // MARK: - Create layout
    private func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { [unowned self] (sectionIdx, _) -> NSCollectionLayoutSection? in
            let section = EventListSectionKind(rawValue: sectionIdx)!
            
            switch section {
            case .today: return self.createSectionWithOneItemGroup()
            case .next: return self.createSectionWithTwoItemsGroup()
            case .prev: return self.createSectionWithTwoItemsGroup()
            case .no: return self.createSectionUserNotHaveEvents()
            }
        }
        
        return layout
    }
    
    // MARK: - Create sections
    private func createSectionWithOneItemGroup() -> NSCollectionLayoutSection {
        // Configure item
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        // Configure group
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(0.79),
            heightDimension: .estimated(141)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item]
        )
        
        // Configure section
        let section = NSCollectionLayoutSection(group: group)
        
        if controller.todayEvents.count == 0 {
            section.orthogonalScrollingBehavior = .none
        } else {
            section.orthogonalScrollingBehavior = .groupPaging
        }
        
        section.interGroupSpacing = 16
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 8,
            leading: paddingLeadingAndTraling,
            bottom: 22,
            trailing: paddingLeadingAndTraling
        )
        
        // Setup section title
        let header = createSectionHeader(heightDimension: 69)
        section.boundarySupplementaryItems = [header]
        
        return section
    }
    
    private func createSectionWithTwoItemsGroup() -> NSCollectionLayoutSection {
        // Configure item
        let size = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        )
        let item = NSCollectionLayoutItem(layoutSize: size)
        
        // Configure group
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .estimated(142)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitem: item,
            count: 2
        )
        
        group.interItemSpacing = .fixed(13)
        
        // Configure section
        let section = NSCollectionLayoutSection(group: group)
        
        section.interGroupSpacing = 13
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 8,
            leading: paddingLeadingAndTraling,
            bottom: 22,
            trailing: paddingLeadingAndTraling
        )
        
        // Setup section title
        let header = createSectionHeader(heightDimension: 18)
        section.boundarySupplementaryItems = [header]
        
        return section
    }
    
    private func createSectionUserNotHaveEvents() -> NSCollectionLayoutSection {
        // Configure item
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        // Configure group
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .estimated(91)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item]
        )
        
        // Configure section
        let section = NSCollectionLayoutSection(group: group)
        
        section.interGroupSpacing = 16
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 8,
            leading: paddingLeadingAndTraling,
            bottom: 22,
            trailing: paddingLeadingAndTraling
        )
        
        // Setup empty section title
        let header = createSectionHeader(heightDimension: 1)
        section.boundarySupplementaryItems = [header]
        
        return section
    }
    
    private func createSectionHeader(heightDimension: CGFloat) -> NSCollectionLayoutBoundarySupplementaryItem {
        let layoutSectionHeaderSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .absolute(heightDimension)
        )
        let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: layoutSectionHeaderSize,
            elementKind: UICollectionView.elementKindSectionHeader,
            alignment: .top
        )
       
        return layoutSectionHeader
   }
    
    // MARK: - Congigure cells
    private func configure<T: EventItemProtocol>(
        cellType: T.Type,
        eventData: Event,
        bgFrom: UIColor,
        bgTo: UIColor,
        shadowColor: UIColor,
        indexPath: IndexPath,
        reuseId: String
    ) -> T {
        // Cast cell
        guard
            let cell = controller.collectionView?.dequeueReusableCell(
                withReuseIdentifier: reuseId,
                for: indexPath
            ) as? T
        else { fatalError("Fatar error cast cell") }
        
        // Setup gradient background
        if cell.bgColorFrom != bgFrom {
            // I did not transfer the code for adding a gradient to
            // a separate method, because it creates memory leak
            cell.bgColorFrom = bgFrom

            let gradient = CAGradientLayer()

            gradient.frame = cell.contentView.frame
            gradient.colors = [bgFrom.cgColor, bgTo.cgColor]
            gradient.cornerRadius = 20

            cell.contentView.layer.insertSublayer(gradient, at: 0)
        }
        
        // Setup shadow
        if cell.shadowColor != shadowColor {
            cell.shadowColor = shadowColor
            Effects.addColorizeShadow(layer: cell.shadowView.layer, shadowColor: shadowColor)
        }
        
        // Setup dateLabel
        let cellDateLabel = cell.dateLabel
        let eventDataDateEvent = setupDateStr(dateStr: eventData.dateEvent)
        
        if cellDateLabel.text != eventDataDateEvent {
            cellDateLabel.text = eventDataDateEvent
        }
        
        // Setup title label
        let cellTitleLabel = cell.titleLabel
        let eventDataTitle = eventData.title
        
        if cellTitleLabel.text != eventDataTitle {
            switch indexPath.section {
            case 0: cellTitleLabel.text = eventDataTitle.capitalizingFirstLetter()
            default: cellTitleLabel.text = eventDataTitle.uppercased()
            }
        }
        
        // Setup seeAllLabel
        let cellSeeAllLabel = cell.seeAllLabel
        let eventDataIsSeeEveryone = eventData.seeEveryoneInThePair
        
        if cellSeeAllLabel.text != eventDataIsSeeEveryone {
            var seeAllText = ""
            
            if eventDataIsSeeEveryone == "Y" {
                seeAllText = NSLocalizedString("Видят: вы, партнер", comment: "Видят: вы, партнер")
            } else {
                seeAllText = NSLocalizedString("Видят: вы", comment: "Видят: вы")
            }
            
            cellSeeAllLabel.text = seeAllText
        }
        
        // Setup img
        let eventDataIconName = eventData.iconPath
        
        if cell.imgName != eventDataIconName {
            cell.imgName = eventDataIconName
        }
        
        // Init cell
        cell.setup()
        
        return cell
    }
    
    private func setupDateStr(dateStr: String) -> String {
        // Prepare date
        let shortDate = TextAttributes.limitingDate(dateStr: dateStr)
        
        var shortDateArr = shortDate.split(separator: " ")
        shortDateArr[0] = String.SubSequence(store.state.monthFilterState.month)
        
        let shortDateWithReplacedMonth = shortDateArr.joined(separator: " ")
        
        return shortDateWithReplacedMonth
    }
    
    // MARK: - Setup data
    func setupDataSource() {
        guard let collectionView = controller.collectionView else { return }
        
        controller.dataSource = UICollectionViewDiffableDataSource<EventListSectionKind, Event>(
            collectionView: collectionView,
            cellProvider: { [unowned self] (collectionView, indexPath, eventData) -> UICollectionViewCell? in
                
                let section = EventListSectionKind(rawValue: indexPath.section)!
                
                switch section {
                case .today:
                    return self.configure(
                        cellType: EventItemBig.self,
                        eventData: eventData,
                        bgFrom: Colors.blueFrom,
                        bgTo: Colors.blueTo,
                        shadowColor: Colors.forBlueShadow,
                        indexPath: indexPath,
                        reuseId: self.id_0
                    )
                    
                case .next:
                    return self.configure(
                        cellType: EventItem.self,
                        eventData: eventData,
                        bgFrom: Colors.orangeFrom,
                        bgTo: Colors.orangeTo,
                        shadowColor: Colors.forOrangeShadow,
                        indexPath: indexPath,
                        reuseId: self.id_1
                    )
                    
                case .prev:
                    return self.configure(
                        cellType: EventItem.self,
                        eventData: eventData,
                        bgFrom: Colors.redFrom,
                        bgTo: Colors.redTo,
                        shadowColor: Colors.forRedShadow,
                        indexPath: indexPath,
                        reuseId: self.id_2
                    )
                case .no:
                    if let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: self.id_3,
                        for: indexPath
                    ) as? EmptyEventItem {
                        cell.addEventBtn.button.funcWhenPressed = { [unowned self] in
                            self.controller.navigationController?.pushViewController(
                                ModuleBuilder.createCreateNewEventModule(),
                                animated: true
                            )
                        }
                        
                        return cell
                    }
                    
                    return UICollectionViewCell()
                }
            }
        )
        
        // Setup sections headers
        controller.dataSource.supplementaryViewProvider = { [unowned self] (collectionView, kind, idxPath) in
            let supplementaryView = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: self.id_section,
                for: idxPath
            )

            guard let sectionHeader = supplementaryView as? CollectionSectionHeader else { return nil }

            return sectionHeader
        }
    }
    
    // MARK: - Setup snapshot
    func reloadData() {
        var snapshot = NSDiffableDataSourceSnapshot<EventListSectionKind, Event>()
        
        let todayEvents = controller.todayEvents
        let nextEvents = controller.nextEvents
        let prevEvents = controller.prevEvents
        
        // Append sections
        snapshot.appendSections(EventListSectionKind.allCases)
        
        // Append items ids into sections
        snapshot.appendItems(todayEvents, toSection: EventListSectionKind.today)
        snapshot.appendItems(nextEvents, toSection: EventListSectionKind.next)
        snapshot.appendItems(prevEvents, toSection: EventListSectionKind.prev)
        
        // Apply new data
        controller.dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    func showEmptyCell() {
        var snapshot = NSDiffableDataSourceSnapshot<EventListSectionKind, Event>()
        
        // Append sections
        snapshot.appendSections(EventListSectionKind.allCases)
        
        // Show(append) that item if no data in current month
        // Just mock event(not will be use)
        let event = Event(
            event_id: 0,
            title: "",
            seeEveryoneInThePair: "",
            dateEvent: "",
            iconPath: "",
            remindEvery: "",
            notifyInA: "",
            notifyId: "",
            event_user_id: 0,
            event_group_id: 0
        )

        snapshot.appendItems([event], toSection: EventListSectionKind.no)
        
        // Apply new data
        controller.dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    func updateAllSectionsSubtitles() {
        let todayEvents = controller.todayEvents
        let nextEvents = controller.nextEvents
        let prevEvents = controller.prevEvents
        
        // MARK: Set sections subtitles titles when get new events lists
        // Create data arr. [] - it is ".no" section. It is showing empty card.
        let eventsListsData = [todayEvents, nextEvents, prevEvents, []]
        
        // Get each SectionKind case
        for (idx, sectionKind) in EventListSectionKind.allCases.enumerated() {
            
            // Get section header by IndexPath
            if let sectionHeader = controller.collectionView?.supplementaryView(
                forElementKind: UICollectionView.elementKindSectionHeader,
                at: IndexPath(row: 0, section: sectionKind.rawValue)
            ) as? CollectionSectionHeader {
                
                // Update section header subtitle
                updateSectionSubTitle(
                    sectionHeader: sectionHeader,
                    sectionIdx: idx,
                    dataCnt: eventsListsData[idx].count,
                    isLoadedData: controller.successGettingEventsList
                )
            }
        }
    }
    
    // Update sections subtitles
    func updateSectionSubTitle(sectionHeader: CollectionSectionHeader, sectionIdx: Int, dataCnt: Int, isLoadedData: Bool) {
        let todaySectionSubTitleMess = NSLocalizedString(
            "Сегодня нет событий",
            comment: "Сегодня нет событий"
        )
        
        let nextSectionSubTitleMess = NSLocalizedString(
            "В этом месяце нет событий",
            comment: "В этом месяце нет событий"
        )
        
        let prevSectionSubTitleMess = NSLocalizedString(
            "Нет прошедших событий в этом месяце",
            comment: "Нет прошедших событий в этом месяце"
        )
        
        // All localized messages arr
        let messages = [todaySectionSubTitleMess, nextSectionSubTitleMess, prevSectionSubTitleMess, ""]
        
        
        if isLoadedData {
            if dataCnt == 0 {
                sectionHeader.noDataLabelView.text = messages[sectionIdx]
            } else {
                sectionHeader.noDataLabelView.text = ""
            }
        }
    }
    
    // MARK: - Push event screen
    func pushEventScreen(indexPath: IndexPath) {
        if let cell = controller.collectionView?.cellForItem(at: indexPath) as? EventItemProtocol {
            switch indexPath.section {
            case 0: self.prepareDataAndPushEventScreen(eventData: self.controller.todayEvents[indexPath.row], cell: cell)
            case 1: self.prepareDataAndPushEventScreen(eventData: self.controller.nextEvents[indexPath.row], cell: cell)
            case 2: self.prepareDataAndPushEventScreen(eventData: self.controller.prevEvents[indexPath.row], cell: cell)
            default: break
            }
        }
    }
    
    private func prepareDataAndPushEventScreen(eventData: Event, cell: EventItemProtocol) {
        
        // Full month from filter
        guard let month = controller.monthFilter.getData()["month"]?.lowercased().capitalizingFirstLetter() else { return }
        
        // Day from cell
        guard let cellDate = cell.dateLabel.text else { return }
        let day = String(cellDate.split(separator: " ")[1])
        
        // Create opened event data for store
        let createOpenedEventData = OpenedEvent(
            month: month,
            day: day,
            rawEventValues: eventData
        )
        
        // Dispatch to stored opened event data
        store.dispatch(SetDataOpenedEventAction(payload: createOpenedEventData))
        
        // Push event screen module and animate tap on event item
        guard let eventScreen = ModuleBuilder.createEventScreenModule() as? EventScreenVC else { return }
        
        Animation.animate(layer: cell.layer) { [unowned self] in
            self.controller.navigationController?.pushViewController(
                eventScreen,
                animated: true
            )
        }
    }
}

