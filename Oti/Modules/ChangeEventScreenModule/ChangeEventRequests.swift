//
//  ChangeEventRequests.swift
//  Oti
//
//  Created by Vlad on 9/13/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

final class ChangeEventRequests {
    static func changeEventRequest(
        eventId: Int,
        date: String,
        title: String,
        watching: String,
        icon: String,
        reminder: String,
        notify: String,
        notifyId: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // MARK: Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        // MARK: Prepare data for group event
        let resolveRequest = store.state.pairState.pairList.resolveRequest
        var bodyForGroup: [String: Any] = [:]
        let uuid = UUID().uuidString
        
        if let resolveRequest = resolveRequest, resolveRequest.count > 0 {
            bodyForGroup = [
                "id": eventId,
                "date": date,
                "title": title,
                "watching": watching,
                "icon": icon,
                "reminder": reminder,
                "notify": notify,
                "notifyId": uuid,
                "event_group_id": resolveRequest[0].group_id
            ]
        }
        
        // Prepare data for only user event
        let bodyForUser: [String: Any] = [
            "id": eventId,
            "date": date,
            "title": title,
            "watching": watching,
            "icon": icon,
            "reminder": reminder,
            "notify": notify,
            "notifyId": uuid
        ]
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.changeEvent,
            type: .POST,
            body: watching == "Y" && bodyForGroup.count > 0 ? bodyForGroup : bodyForUser,
            cb: { (_, err) in
                // MARK: Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
    
    static func deleteEventRequest(
        eventId: Int,
        notifyId: String?,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {        
        // MARK: Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.deleteEvent,
            type: .POST,
            body: ["id": eventId],
            cb: { (_, err) in
                // MARK: Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // MARK: Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
                
                // MARK: Remove event notify
                if let notifyId = notifyId {
                    LocalNotifications.shared.removeNotify(notifyId: notifyId)
                }
            }
        )
    }
}
