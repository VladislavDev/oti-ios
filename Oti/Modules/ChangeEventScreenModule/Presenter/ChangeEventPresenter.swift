//
//  ChangeEventPresenter.swift
//  Oti
//
//  Created by Vlad on 9/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol ChangeEventVCProtocol: class, TitleViewProtocol, NotifyForViewProtocol, DatePickerViewProtocol, RepetitionCycleViewProtocol, SwitchViewProtocol, IconsPackNamesListProtocol, NavigationControllerProtocol, CollectionViewProtocol {
    var openedEventState: OpenedEvent? { get }
}

protocol ChangeEventPresenterProtocol {
    init(controller: ChangeEventVCProtocol)
    
    var id_0: String { get set }
    
    func createSection() -> NSCollectionLayoutSection
    func createLayout() -> UICollectionViewLayout
    func applyStylesAndSaveSelectedIndexPath(indexPath: IndexPath)
    func deselectDefaultEstablishedItem()
    func deselectItems(indexPath: IndexPath)
    func configureCellItem(indexPath: IndexPath, cell: ImgItemCell)
    func handleChangeEvent()
    
    func prepareCollectionView()
    func prepareTitleView()
    func prepareDateField()
    func prepareNotifyForView()
    func prepateRepetitionCycleView()
    func prepareSwitchView()
    func handleDeleteEvent()
}

final class ChangeEventPresenter: ChangeEventPresenterProtocol {
    var id_0 = "id_0"
    
    private unowned var controller: ChangeEventVCProtocol
    private var selectedItemIndexPath: IndexPath?
    
    init(controller: ChangeEventVCProtocol) {
        self.controller = controller
    }
    
    // MARK: - Create layout
    func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { [unowned self] (sectionIdx, _) -> NSCollectionLayoutSection? in
            let section = OpenedEventSectionKind(rawValue: sectionIdx)!
            
            switch section {
            case .imgs: return self.createSection()
            }
        }
        
        return layout
    }
    
    // MARK: - Create sections
    func createSection() -> NSCollectionLayoutSection {
        // Configure item
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(1)
        )
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        // Configure group
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .estimated(106),
            heightDimension: .estimated(100)
        )
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: [item]
        )
        
        // Configure section
        let section = NSCollectionLayoutSection(group: group)
        
        section.orthogonalScrollingBehavior = .continuous
        section.interGroupSpacing = 16
        
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 0,
            leading: 32,
            bottom: 0,
            trailing: 32
        )
        
        return section
    }
    
    func applyStylesAndSaveSelectedIndexPath(indexPath: IndexPath) {
        if let cell = controller.collectionView?.cellForItem(at: indexPath) as? ImgItemCell {
            cell.setSelected()
            
            selectedItemIndexPath = indexPath
        }
    }
    
    func deselectDefaultEstablishedItem() {
        if let selectedItemIndexPath = selectedItemIndexPath, selectedItemIndexPath.row != 0 {
            if let cell = controller.collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? ImgItemCell {
                cell.setDeselected()
            }
        }
    }
    
    func deselectItems(indexPath: IndexPath) {
        if let cell = controller.collectionView?.cellForItem(at: indexPath) as? ImgItemCell {
            cell.setDeselected()
            
            selectedItemIndexPath = nil
        }
    }
    
    func configureCellItem(indexPath: IndexPath, cell: ImgItemCell) {
        guard
            let url = URL(string: "\(URLs.api)/static/images_pack2/\(controller.iconsPackNamesList[indexPath.row])")
        else { return }
        
        cell.illustrationView.sd_setImage(with: url, completed: nil)
    
        // If no selected img set selected first img
        if selectedItemIndexPath == nil && indexPath.row == 0 {
            cell.setSelected()
            selectedItemIndexPath = IndexPath(row: 0, section: 0)
        }
        
        // If selected row == dequeue cell row set it selected (it is necessary that during scrolling
        // the selected cell does not lose styles)
        if selectedItemIndexPath != nil && indexPath.row == selectedItemIndexPath?.row {
            cell.setSelected()
        }
    }
    
    func prepareCollectionView() {
        guard
            let openedEventState = controller.openedEventState,
            let imgName = openedEventState.rawEventValues?.iconPath
        else { return }
        
        let imgIdx = controller.iconsPackNamesList.firstIndex { $0 == imgName }
        
        if let imgIdxUnwrap = imgIdx {
            let indexPath = IndexPath(row: Int(imgIdxUnwrap), section: 0)
            
            selectedItemIndexPath = indexPath
            
            controller.collectionView?.scrollToItem(
                at: indexPath,
                at: .centeredHorizontally,
                animated: false
            )
            
            controller.collectionView?.selectItem(
                at: indexPath,
                animated: false,
                scrollPosition: .centeredHorizontally
            )
        }
    }
    
    func prepareTitleView() {
        guard let openedEventState = controller.openedEventState else { return }
        
        if let textFieldValue = controller.titleView.textField.text, textFieldValue == "" {
            controller.titleView.textField.text = openedEventState.rawEventValues?.title
        }
    }
    
    func prepareDateField() {
        guard let openedEventState = controller.openedEventState else { return }
        
        let dateField = controller.datePickerView.dateField
        let datePicker = controller.datePickerView.datePicker
        
        if let openedDateEvent = openedEventState.rawEventValues?.dateEvent {
            // Setup dateField text placeholder
            let preparedDate =
                WorkWithDate.transformIsoDateStrToFormattedDateStr(
                    format: .full,
                    dateStr: openedDateEvent
                )
            
            dateField.text = preparedDate
            
            // Setup date picker date
            guard let openedEventISODate =
                WorkWithDate.transformStrDateToDate(dateStr: openedDateEvent)
            else { return }
            
            datePicker.date = openedEventISODate
        }
    }
    
    func prepareNotifyForView() {
        guard let openedEventState = controller.openedEventState else { return }
        
        if let notifyInA = openedEventState.rawEventValues?.notifyInA {
            controller.notifyForView.initialIdx = NotifyFor.getIdx(str: notifyInA)
        }
    }

    func prepateRepetitionCycleView() {
        guard let openedEventState = controller.openedEventState else { return }
        
        if let remindEvery = openedEventState.rawEventValues?.remindEvery {
            controller.repetitionCycleView.initialIdx = RepetitionCycle.getIdx(str: remindEvery)
        }
    }
    
    func prepareSwitchView() {
        guard let openedEventState = controller.openedEventState else { return }
        
        controller.switchView.isOn = openedEventState
            .rawEventValues?
            .seeEveryoneInThePair == "Y" ? true : false
    }
    
    func handleChangeEvent() {
        guard
            let openedEventState = controller.openedEventState,
            let eventId = openedEventState.rawEventValues?.event_id
        else { return }
        
        let notifyId = openedEventState.rawEventValues?.notifyId ?? ""
        
        // validate title
        let isValidTitle = controller.titleView.validateField()
        
        guard let selectedItemIndexPath = selectedItemIndexPath else { return }
        
        if isValidTitle {
            let newEventImgName = controller.iconsPackNamesList[selectedItemIndexPath.row]

            guard let newEventTitle = controller.titleView.getText() else { return }

            let newEventDate = WorkWithDate.getFormettedDate(
                format: .mdyWithLodash,
                date: controller.datePickerView.getDate()
            )

            let newEventNotifyForRes = NotifyFor.allCases[controller.notifyForView.getIdxBtn()].rawValue
            let newEventRepitCycleRes = RepetitionCycle.allCases[controller.repetitionCycleView.getIdxBtn()].rawValue
            let newEventIsSeeAll = controller.switchView.isOn()
            
            store.dispatch(changeEventRequestAction(
                eventId: eventId,
                date: newEventDate,
                title: newEventTitle,
                watching: newEventIsSeeAll ? "Y" : "N",
                icon: newEventImgName,
                reminder: newEventRepitCycleRes,
                notify: newEventNotifyForRes,
                notifyId: notifyId,
                cb: {
                    store.dispatch(getEventsListRequestAction(
                        year: store.state.monthFilterState.year,
                        month: store.state.monthFilterState.monthIdx,
                        cb: { [unowned self] in
                            self.controller.navigationController?.popToRootViewController(animated: true)
                        })
                    )
                }
            ))
        }
    }
    
    @objc func handleDeleteEvent() {
        guard
            let openedEventState = controller.openedEventState,
            let eventId = openedEventState.rawEventValues?.event_id
        else { return }
        
        let notifyId = openedEventState.rawEventValues?.notifyId
        
        // Delete event request
        store.dispatch(deleteEventRequestAction(eventId: eventId, notifyId: notifyId) { [unowned self] in
            
            // Get events list request
            store.dispatch(getEventsListRequestAction(
                year: store.state.monthFilterState.year,
                month: store.state.monthFilterState.monthIdx,
                cb: { [unowned self] in
                    
                    // Pop to root vc
                    self.controller.navigationController?.popToRootViewController(animated: true)
                })
            )
        })
    }
}
