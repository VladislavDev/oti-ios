//
//  PairSettingsModel.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

struct PairSettingsModel: Decodable {
    var status: Int
    var message: PSMessage
}

struct PSMessage: Decodable {
    var resolveRequest: [PSGroupsDatas]?
    var senderReuqest: [PSGroupsDatas]?
    var recipientsRequests: [PSGroupsDatas]?
}

struct PSGroupsDatas: Decodable {
    var group_id: Int
    var status: String
    var sender: Int
    var recipient: Int
    var sender_email: String
    var recipient_email: String
}
