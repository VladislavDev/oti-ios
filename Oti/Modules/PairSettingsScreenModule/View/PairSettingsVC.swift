//
//  PairSettingsVC.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

final class PairSettingsVC: UIViewController, PairSettingsVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: PairSettingsPresenterProtocol?
    
    // MARK: - Table groups
    var resolveRequest: [PSGroupsDatas] = []
    var recipientsRequests: [PSGroupsDatas] = []
    var senderRequest: [PSGroupsDatas] = []
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    var refreshControl = UIRefreshControl()
    
    var tableView: UITableView = {
        let t = UITableView()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.backgroundColor = .clear
        
        return t
    }()

    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.subscribe(self) { subscription in
            subscription.select { state in state.pairState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSelfView()
        setupAdditionToNavBar()
        setupTableView()
        setupRefreshControl()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK:: - ReSwift
    func newState(state: PairState) {
        resolveRequest = state.pairList.resolveRequest ?? []
        recipientsRequests = state.pairList.recipientsRequests ?? []
        senderRequest = state.pairList.senderReuqest ?? []
        
        if state.isSuccessGetPairList {
            tableView.reloadData()
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString(
                "Настройки пары",
                comment: "Настройки пары"
            )
        )
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 40
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc private func refresh() {
        presenter?.handleRefresh()
    }
}

// MARK: - Table settings
extension PairSettingsVC: UITableViewDelegate {
    // Style section label
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return } 
        
        header.tintColor = .clear
        header.textLabel?.textColor = Colors.black
        header.textLabel?.font = .systemFont(ofSize: FontSizes.body, weight: .bold)
    }
    
    // Spacing between cell and section label
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 23
    }
    
    // Spacing between sections
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 23
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // Height cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}

extension PairSettingsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = SectionHeader()
        
        switch section {
        case 0: header.titleLabel.text = NSLocalizedString(
            "ТЕКУЩАЯ ПАРА",
            comment: "ТЕКУЩАЯ ПАРА"
        )
        case 1: header.titleLabel.text = NSLocalizedString(
            "ВХОДЯЩИЕ",
            comment: "ВХОДЯЩИЕ"
        )
        case 2: header.titleLabel.text = NSLocalizedString(
            "ИСХОДЯЩИЕ",
            comment: "ИСХОДЯЩИЕ"
        )
        default: break
        }
        
        return header
    }
    
    func numberOfSections(in tableView: UITableView) -> Int { 3 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else { return 0 }
        
        // Set default 1 cell in each section
        let counters = presenter.getRequestsCnt()
        
        switch section {
        case 0: return counters.resolveRequestCnt
        case 1: return counters.recipientsRequestsCnt
        case 2: return counters.senderRequestCnt
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let presenter = presenter else { return UITableViewCell() }
        
        let sectionCnt = indexPath.section
        let idxRowCnt = indexPath.row
        
        switch indexPath.section {
        case 0:
            // Current pair section
            return presenter.resolveSectionLogic(sectionCnt: sectionCnt, idxRowCnt: idxRowCnt)
            
        case 1:
            // Requests sent to you section
            return presenter.sentToYouSectionLogin(sectionCnt: sectionCnt, idxRowCnt: idxRowCnt)
            
        case 2:
            // Requests sent by you section
            return presenter.sentByYouSectionLogin(sectionCnt: sectionCnt, idxRowCnt: idxRowCnt)
            
        default: return UITableViewCell()
        }
    }
}
