//
//  PairSettingsPresenter.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol PairSettingsVCProtocol: class, NavigationControllerProtocol, RefreshControlProtocol, TableViewProtocol {
    var resolveRequest: [PSGroupsDatas] { get set }
    var recipientsRequests: [PSGroupsDatas] { get set }
    var senderRequest: [PSGroupsDatas] { get set }
}

protocol PairSettingsPresenterProtocol {
    init(controller: PairSettingsVCProtocol)
    
    func handleRefresh()
    func handleAddBtn()
    func handleDeleteBtn(groupId: Int)
    
    func getRequestsCnt() -> (
        resolveRequestCnt: Int,
        recipientsRequestsCnt: Int,
        senderRequestCnt: Int
    )
    
    func resolveSectionLogic(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell
    
    func sentToYouSectionLogin(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell
    
    func sentByYouSectionLogin(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell
}

final class PairSettingsPresenter: PairSettingsPresenterProtocol {
    private unowned var controller: PairSettingsVCProtocol
    
    required init(controller: PairSettingsVCProtocol) {
        self.controller = controller
    }
        
    func getRequestsCnt() -> (
        resolveRequestCnt: Int,
        recipientsRequestsCnt: Int,
        senderRequestCnt: Int
    ) {
        let resolveRequestCnt = controller.resolveRequest.count
        let resolveRequestCntRes = resolveRequestCnt == 0 ? 1 : resolveRequestCnt
        
        let recipientsRequestsCnt = controller.recipientsRequests.count
        let recipientsRequestsCntRes = recipientsRequestsCnt == 0 ? 1 : recipientsRequestsCnt
        
        let senderRequestCnt = controller.senderRequest.count
        let senderRequestCntRes = senderRequestCnt == 0 ? 1 : senderRequestCnt
        
        return (
            resolveRequestCntRes,
            recipientsRequestsCntRes,
            senderRequestCntRes
        )
    }
    
    func resolveSectionLogic(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell {
        // Cells
        // Do not use dequeueReusableCell as a small number of cells is expected
        let cellAdd = AddCell()
        cellAdd.titleItem = NSLocalizedString(
            "Добавить пару",
            comment: "Добавить пару"
        )
        
        let noPairCell = EmptyCell()
        let cellDelete = DeleteCell()
        
        // Data
        let resolveRequestCnt = controller.resolveRequest.count
        let resolveReq = sectionCnt == 0
            && resolveRequestCnt > 0 ? controller.resolveRequest[idxRowCnt] : nil
        let senderRequestCnt = controller.senderRequest.count
        
        // Configure
        // If no current pair and no an outgoing,
        // show cell with add button
        if resolveRequestCnt == 0 && senderRequestCnt == 0 {
            cellAdd.funcWhenPressed = handleAddBtn
            return cellAdd
            
        // If no current pair and have an outgoing,
        // show cell with only title
        } else if resolveRequestCnt == 0 && senderRequestCnt > 0 {
            noPairCell.titleItem = NSLocalizedString(
                "Нет текущей пары",
                comment: "Нет текущей пары"
            )
            return noPairCell
            
        // If have a pair, than show delete button
        } else {
            if let resolveReq = resolveReq,
                let pairEmail = PairHelpers.getPairEmail(resolveReq: resolveReq) {
                cellDelete.titleItem = pairEmail
                
                cellDelete.funcWhenPressed = { [unowned self] in
                    self.handleDeleteBtn(groupId: resolveReq.group_id)
                }
                
                return cellDelete
            }
            
            return UITableViewCell()
        }
    }
    
    func sentToYouSectionLogin(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell {
        // Cells
        // Do not use dequeueReusableCell as a small number of cells is expected
        let noResultCell = EmptyCell()
        let toYouCell = ResolveRejectCell()
        
        // Data
        let recipientsRequestsCnt = controller.recipientsRequests.count
        let recipientsReq = sectionCnt == 1
            && recipientsRequestsCnt > 0 ? controller.recipientsRequests[idxRowCnt] : nil
        
        // Configure
        if recipientsRequestsCnt == 0 {
            noResultCell.titleItem = NSLocalizedString(
                "Нет запросов",
                comment: "Нет запросов"
            )
            
            return noResultCell
        } else {
            if let email = recipientsReq?.sender_email {
                toYouCell.titleItem = email
            }
            
            toYouCell.confirmFunc = { [unowned self] in
                self.handleConfirmBtn(groupId: recipientsReq!.group_id)
            }
            
            toYouCell.deleteFunc = { [unowned self] in
                self.handleRejectBtn(groupId: recipientsReq!.group_id)
            }
            
            return toYouCell
        }
    }
    
    func sentByYouSectionLogin(
        sectionCnt: Int,
        idxRowCnt: Int
    ) -> UITableViewCell {
        // Cells
        // Do not use dequeueReusableCell as a small number of cells is expected
        let noResultCell = EmptyCell()
        let fromYouCell = CancelCell()
        
        // Data
        let senderRequestCnt = controller.senderRequest.count
        let senderReq = sectionCnt == 2
            && senderRequestCnt > 0 ? controller.senderRequest[idxRowCnt] : nil
        
        // Configure
        if senderRequestCnt == 0 {
            noResultCell.titleItem = NSLocalizedString(
                "Нет запросов",
                comment: "Нет запросов"
            )
            return noResultCell
        } else {
            if let email = senderReq?.recipient_email {
                fromYouCell.titleItem = email
            }

            fromYouCell.funcWhenPressed = { [unowned self] in
                self.handleRejectBtn(groupId: senderReq!.group_id)
            }
            return fromYouCell
        }
    }
    
    func handleRefresh() {
        store.dispatch(getPairListRequestAction(cb: { _ in
            self.controller.refreshControl.endRefreshing()
        }))
    }
        
    func handleAddBtn() {
        controller.navigationController?.present(
            ModuleBuilder.createAddPairModule(),
            animated: true,
            completion: nil
        )
    }
    
    func handleConfirmBtn(groupId: Int) {
        // Post resolve group
        store.dispatch(resolveGroupRequestAction(groupId: groupId) {
            
            // Get pairs lists requests
            store.dispatch(getPairListRequestAction(cb: { resolveRequests in
                
                // If exist resolve pair
                if let resolveRequests = resolveRequests, resolveRequests.count > 0 {
                    
                    // Get pair gifts lists
                    store.dispatch(getPairGiftsListsRequestAction(cb: {
                        
                        // End refresh controll
                        self.controller.refreshControl.endRefreshing()
                    }))
                }
            }))
        })
    }
    
    func handleDeleteBtn(groupId: Int) {
        store.dispatch(deleteGroupRequestAction(groupId: groupId) {
            store.dispatch(getEventsListRequestAction(
                year: store.state.monthFilterState.year,
                month: store.state.monthFilterState.monthIdx,
                cb: { [unowned self] in
                    self.handleRefresh()
                })
            )
        })
    }
    
    func handleRejectBtn(groupId: Int) {
        store.dispatch(deleteGroupRequestAction(groupId: groupId) {
            self.handleRefresh()
        })
    }
}
