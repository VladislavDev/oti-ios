//
//  PairSettingsRequests.swift
//  Oti
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class PairSettingsRequests {
    private init() {}
    
    static func getPairListRequest(
        userId: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: ((PSMessage?) -> ())?
    ) {
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.getPairList,
            type: .POST,
            body: ["userId": userId],
            cb: { (data, err) in                
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Parse and decode data
                guard let data = data else { return }
                var res = try? JSONDecoder().decode(PairSettingsModel.self, from: data)
                
                if res?.message.resolveRequest == nil {
                    res?.message.resolveRequest = []
                }
                
                if res?.message.senderReuqest == nil {
                    res?.message.senderReuqest = []
                }
                
                if res?.message.recipientsRequests == nil {
                    res?.message.recipientsRequests = []
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async {
                        cbWhenSuccessRequest(res?.message)
                    }
                }
            }
        )
    }
    
    static func addNewPairRequest(
        senderId: String,
        recipientEmail: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.addNewPair,
            type: .POST,
            body: ["senderId": senderId, "recipientEmail": recipientEmail],
            cb: { (_, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
    
    static func resolveGroupRequest(
        groupId: Int,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.resolveGroup,
            type: .POST,
            body: ["groupId": groupId],
            cb: { (_, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
    
    static func deleteGroupRequest(
        groupId: Int,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.deleteGroup,
            type: .POST,
            body: ["groupId": groupId],
            cb: { (_, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
