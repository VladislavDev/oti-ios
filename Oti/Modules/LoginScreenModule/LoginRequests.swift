//
//  LoginRequests.swift
//  Oti
//
//  Created by Vlad on 8/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class LoginRequests {
    static func loginRequest(
        email: String,
        pass: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: ((LoginModel?) -> ())?
    ) {
        // MARK: - Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async {
                cbWhenStartRequest()
            }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: URLs.login,
            type: .POST,
            body: [
                "email": email,
                "pass": pass
            ]
        ) { (data, err) in
            
            // MARK: - Check err
            if err {
                if let cbWhenErrorRequest = cbWhenErrorRequest {
                    DispatchQueue.main.async { cbWhenErrorRequest() }
                }
                return
            }
            
            // MARK: - Parse and decode data
            guard let data = data else { return }
            let res = try? JSONDecoder().decode(LoginModel.self, from: data)
            guard let resUnwrap = res else { return }
            
            // MARK: - Save token
            TokenService.shared.saveAuthUserData(
                token: resUnwrap.message.token,
                userId: resUnwrap.message.user.userId,
                email: resUnwrap.message.user.email
            )
            
            // MARK: - Return result
            if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                DispatchQueue.main.async {
                    cbWhenSuccessRequest(resUnwrap)
                }
            }
        }
    }
}
