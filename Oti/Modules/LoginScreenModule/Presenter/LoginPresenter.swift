//
//  LoginPresenter.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

// MARK: - Protocols
protocol LoginVCProtocol: class, NavigationControllerProtocol, EmailFieldProtocol, PassFieldProtocol {
    
}

protocol LoginPresenterProtocol {
    init(controller: LoginVCProtocol)
    
    func handleSignInBtn()
    func handleCreateAccBtn()
    func handleForgotPassBtn()
}

// MARK: - Presenter
class LoginPresenter: LoginPresenterProtocol {
    private unowned var controller: LoginVCProtocol
    
    required init(controller: LoginVCProtocol) {
        self.controller = controller
    }
    
    func handleSignInBtn() {
        let emailFiled = controller.emailField
        let passField = controller.passField
        
        let emailValidateRes = emailFiled.validateField()
        let passValidateRes = passField.validateField()
        
        if emailValidateRes && passValidateRes {
            store.dispatch(loginRequestAction(
                            email: emailFiled.getText()!,
                            pass: passField.getText()!)
            )
        }
    }
    
    func handleForgotPassBtn() {
        controller.navigationController?.pushViewController(
            ModuleBuilder.createForgotPassModule(),
            animated: true
        )
    }
    
    func handleCreateAccBtn() {
        controller.navigationController?.pushViewController(
            ModuleBuilder.createCreateAccModule(),
            animated: true
        )
    }
}
