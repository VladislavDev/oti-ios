//
//  LoginModel.swift
//  Oti
//
//  Created by Vlad on 8/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

struct LoginModel: Decodable {
    var status: Int
    var message: MessageModel
}

struct MessageModel: Decodable {
    var token: String
    var user: UserModel
}

struct UserModel: Decodable {
    var email: String
    var userId: Int
}
