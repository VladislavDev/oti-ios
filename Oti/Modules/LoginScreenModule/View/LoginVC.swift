//
//  LoginVC.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class LoginVC: UIViewController, LoginVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: LoginPresenterProtocol!
    
    // MARK: - Create UI elements
    private var logoView = LogoView()
    
    private var formWrapper: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            v.heightAnchor.constraint(equalToConstant: 236)
        ])
        
        return v
    }()
    
    private var labelView = LabelView(
        title: NSLocalizedString("Приветствуем", comment: "LoginVC_title"),
        color: Colors.black,
        fontSize: FontSizes.large,
        weight: .bold,
        aligment: .center
    )
    
    var emailField: WrappedTextField = {
        let f = WrappedTextField(
            placeholder: NSLocalizedString(
                "Email",
                comment: "Email"
            ),
            textContentType: .username,
            keyboardType: .emailAddress,
            validationType: EmailValidations()
        )
        return f
    }()
    
    var passField: WrappedTextField = {
        let f = WrappedTextField(
            placeholder: NSLocalizedString(
                "Пароль",
                comment: "Пароль"
            ),
            textContentType: .password,
            keyboardType: .default,
            validationType: PasswordValidations(),
            secure: true
        )
        return f
    }()
    
    private var signInBtnWithShadow = RectBtnWithShadow(btnTitle: NSLocalizedString(
        "Войти",
        comment: "Войти")
    )
    
    private var dontHaveAnAccStackView: UIStackView = {
        let s = UIStackView()
        
        s.translatesAutoresizingMaskIntoConstraints = false
        
        s.axis = .vertical
        s.spacing = 4
        
        return s
    }()
    
    private var dontHaveAnAccLabel: LabelView = {
        let l = LabelView(
            title: NSLocalizedString(
                "У вас нет аккаунта?",
                comment: "У вас нет аккаунта?"
            ),
            color: Colors.grayHard,
            fontSize: FontSizes.footnote,
            weight: .regular,
            aligment: .center
        )
        
        return l
    }()
    
    private var createAccBtn = UnderlineBtn(
        btnTitle: NSLocalizedString(
            "Создать аккаунт",
            comment: "Создать аккаунт"
        )
    )
    private var forgotPassBtn = UnderlineBtn(
        btnTitle: NSLocalizedString(
            "Забыли пароль?",
            comment: "Забыли пароль?"
        )
    )

    // MARK: - VC lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSelfView()
        setupFormWrapper()
        setupLogoView()
        setupLabelView()
        setupFields()
        setupSignInBtn()
        setupDontHaveAnAccElems()
        setupForgotPassBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Subscribe on login state
        store.subscribe(self) { subcription in
            subcription.select { state in state.authState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Unsubscribe
        store.unsubscribe(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    // ReSwift
    func newState(state: AuthState) {
        if state.isStartLoginRequest {
            signInBtnWithShadow.button.showLoading()
        } else {
            signInBtnWithShadow.button.hideLoading()
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 57,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 57
        )
        
        dismissKey()
    }
    
    private func setupFormWrapper() {
        self.view.addSubview(formWrapper)
        
        NSLayoutConstraint.activate([
            formWrapper.centerYAnchor.constraint(
                equalTo: self.view.centerYAnchor,
                constant: 24
            ),
            formWrapper.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            formWrapper.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
    
    private func setupLogoView() {
        self.view.addSubview(logoView)
        
        NSLayoutConstraint.activate([
            logoView.bottomAnchor.constraint(
                lessThanOrEqualTo: formWrapper.topAnchor,
                constant: -22
            ),
            logoView.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            )
        ])
    }
    
    private func setupLabelView() {
        formWrapper.addSubview(labelView)
        
        NSLayoutConstraint.activate([
            labelView.topAnchor.constraint(
                equalTo: formWrapper.topAnchor
            ),
            labelView.centerXAnchor.constraint(
                equalTo: formWrapper.centerXAnchor
            )
        ])
    }
    
    private func setupFields() {
        formWrapper.addSubview(emailField)
        formWrapper.addSubview(passField)
        
        NSLayoutConstraint.activate([
            emailField.topAnchor.constraint(
                equalTo: labelView.bottomAnchor, constant: 37
            ),
            emailField.leadingAnchor.constraint(
                equalTo: formWrapper.leadingAnchor
            ),
            emailField.trailingAnchor.constraint(
                equalTo: formWrapper.trailingAnchor
            ),
            emailField.centerXAnchor.constraint(
                equalTo: formWrapper.centerXAnchor
            ),
            
            passField.topAnchor.constraint(
                equalTo: emailField.bottomAnchor, constant: 16
            ),
            passField.leadingAnchor.constraint(
                equalTo: formWrapper.leadingAnchor
            ),
            passField.trailingAnchor.constraint(
                equalTo: formWrapper.trailingAnchor
            ),
            passField.centerXAnchor.constraint(
                equalTo: formWrapper.centerXAnchor
            ),
        ])
    }
    
    private func setupSignInBtn() {
        signInBtnWithShadow.button.funcWhenPressed = presenter.handleSignInBtn
        
        formWrapper.addSubview(signInBtnWithShadow)
        
        NSLayoutConstraint.activate([
            signInBtnWithShadow.bottomAnchor.constraint(
                equalTo: formWrapper.bottomAnchor,
                constant: 7
            ),
            signInBtnWithShadow.centerXAnchor.constraint(
                equalTo: formWrapper.centerXAnchor
            )
        ])
    }
    
    private func setupDontHaveAnAccElems() {
        createAccBtn.funcWhenPressed = presenter.handleCreateAccBtn
        
        self.view.addSubview(dontHaveAnAccStackView)
        dontHaveAnAccStackView.addArrangedSubview(dontHaveAnAccLabel)
        dontHaveAnAccStackView.addArrangedSubview(createAccBtn)
        
        NSLayoutConstraint.activate([
            dontHaveAnAccStackView.widthAnchor.constraint(equalToConstant: 290),
            dontHaveAnAccStackView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,
                constant: -30
            ),
            dontHaveAnAccStackView.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            )
        ])
    }
    
    private func setupForgotPassBtn() {
        forgotPassBtn.funcWhenPressed = presenter.handleForgotPassBtn
        
        self.view.addSubview(forgotPassBtn)
        
        NSLayoutConstraint.activate([
            forgotPassBtn.bottomAnchor.constraint(
                equalTo: dontHaveAnAccStackView.topAnchor,
                constant: -16
            ),
            forgotPassBtn.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            )
        ])
    }
}
