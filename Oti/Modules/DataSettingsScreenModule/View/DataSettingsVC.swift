//
//  DataSettingsVC.swift
//  Oti
//
//  Created by Vlad on 8/29/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class DataSettingsVC: UIViewController {
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    private let tableView: UITableView = {
        let t = UITableView()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.backgroundColor = .clear
        
        return t
    }()

    // MARK: - VC lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSelfView()
        setupAdditionToNavBar()
        setupTableView()
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString(
                "Изменение данных",
                comment: "Изменение данных"
            )
        )
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 40
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
}

extension DataSettingsVC: UITableViewDelegate {
    
    // Style section label
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        
        header.tintColor = .clear
        header.textLabel?.textColor = Colors.black
        header.textLabel?.font = .systemFont(ofSize: FontSizes.body, weight: .bold)
    }
    
    // Spacing between cell and section label
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 23
    }
    
    // Spacing between sections
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // Height cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    // Tap on cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 1:
            // This is necessary to avoid delay when the modal
            // window appears from the table.
            // Reason for the delay - cell.selectionStyle
            DispatchQueue.main.async {
                self.present(
                    ModuleBuilder.createChangePassModule(),
                    animated: true,
                    completion: nil
                )
            }
        default: break
        }
    }
}

extension DataSettingsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = SectionHeader()
        
        switch section {
        case 0:
            header.titleLabel.text = NSLocalizedString(
                "EMAIL",
                comment: "EMAIL"
            )
        case 1:
            header.titleLabel.text = NSLocalizedString(
                "ПАРОЛЬ",
                comment: "ПАРОЛЬ"
            )
        default: break
        }
        
        return header
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Data in cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BasicCell()
        
        switch indexPath.section {
        case 0:
            if let email = TokenService.shared.getAuthUserData().email {
                cell.titleItem = email
            }
        case 1:
            cell.titleItem = "*******"
            cell.isShowDisclosure = true
        default: break
        }
        
        return cell
    }
}
