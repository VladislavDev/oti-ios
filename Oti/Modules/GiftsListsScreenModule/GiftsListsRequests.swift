//
//  GiftsListsRequests.swift
//  Oti
//
//  Created by Vlad on 9/3/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

class GiftsListsRequests {
    // 1) ModelType is needed so as not to write two almost identical
    // functions to get the desired and not desired gifts.
    
    // 2) cbWhenSuccessRequest will take ModelType and
    // the argument will be cast to the desired type in the reducer.
    
    static func getGiftsListRequest<ModelType: GiftsResponsesProtocol>(
        userId: String,
        modelType: ModelType.Type,
        url: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: ((ModelType) -> ())?
    ) {
        
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: url,
            type: .POST,
            body: ["userId": userId],
            cb: { (data, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }

                // Parse and decode data
                guard let data = data else { return }

                let res = try? JSONDecoder().decode(
                    ModelType.self,
                    from: data
                )
                guard let resUnwrap = res else { return }

                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest(resUnwrap) }
                }
            }
        )
    }
    
    static func addGiftRequest(
        giftType: GiftType,
        title: String,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        guard let userId = TokenService.shared.getAuthUserData().userId else {
            if let cbWhenErrorRequest = cbWhenErrorRequest {
                DispatchQueue.main.async { cbWhenErrorRequest() }
            }
            return
        }
        
        WorkWithNetwork.shared.request(
            urlStr: giftType == .desired ? URLs.addDesiredGift : URLs.addUnwantedGift,
            type: .POST,
            body: [
                "userId": userId,
                "title": title
            ],
            cb: { (_, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
    
    static func deletionGiftRequest(
        giftType: GiftType,
        giftId: Int,
        cbWhenStartRequest: (() -> ())?,
        cbWhenErrorRequest: (() -> ())?,
        cbWhenSuccessRequest: (() -> ())?
    ) {
        
        // Start request
        if let cbWhenStartRequest = cbWhenStartRequest {
            DispatchQueue.main.async { cbWhenStartRequest() }
        }
        
        WorkWithNetwork.shared.request(
            urlStr: giftType == .desired ? URLs.deleteDesiredGit : URLs.deleteUnwantedGift,
            type: .POST,
            body: ["id": giftId],
            cb: { (_, err) in
                // Check err
                if err {
                    if let cbWhenErrorRequest = cbWhenErrorRequest {
                        DispatchQueue.main.async { cbWhenErrorRequest() }
                    }
                    return
                }
                
                // Return result
                if let cbWhenSuccessRequest = cbWhenSuccessRequest {
                    DispatchQueue.main.async { cbWhenSuccessRequest() }
                }
            }
        )
    }
}
