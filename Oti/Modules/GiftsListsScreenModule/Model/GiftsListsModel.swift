//
//  GiftsListsModel.swift
//  Oti
//
//  Created by Vlad on 9/3/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

protocol GiftsResponsesProtocol: Decodable {}
protocol GiftItemProtocol: Decodable {}

// Desired gifts
struct DesiredGiftsListResponse: GiftsResponsesProtocol {
    var status: Int
    var message: [DesiredGiftItem]
}

struct DesiredGiftItem: GiftItemProtocol {
    var wish_id: Int
    var title: String
    var wish_user_id: Int
}

// Unwanted gifts
struct UnwantedGiftsListResponse: GiftsResponsesProtocol {
    var status: Int
    var message: [UnwantedGiftItem]
}

struct UnwantedGiftItem: GiftItemProtocol {
    var stop_id: Int
    var title: String
    var stop_user_id: Int
}
