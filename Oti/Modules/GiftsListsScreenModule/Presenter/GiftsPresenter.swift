//
//  GiftsPresenter.swift
//  Oti
//
//  Created by Vlad on 9/3/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol GiftsVCProtocol: class, TableViewProtocol {
    var desiredGiftsList: [DesiredGiftItem] { get set }
    var unwantedGiftsList: [UnwantedGiftItem] { get set }
}

protocol GiftsPresenterProtocol {
    init(controller: GiftsVCProtocol)
    
    func getGiftsCnt() -> (desiredGitfsCnt: Int, unwantedGiftsCnt: Int)
    func desiredSectionLogin(indexPath: IndexPath) -> UITableViewCell
    func unwantedSectionLogic(indexPath: IndexPath) -> UITableViewCell
}

class GiftsPresenter: GiftsPresenterProtocol {
    private unowned var controller: GiftsVCProtocol
    
    required init(controller: GiftsVCProtocol) {
        self.controller = controller
    }
    
    func getGiftsCnt() -> (desiredGitfsCnt: Int, unwantedGiftsCnt: Int) {
        let desiredGiftsListCnt = controller.desiredGiftsList.count
        let desiredGiftsListCntRes = desiredGiftsListCnt == 0 ? 1 : desiredGiftsListCnt
        
        let unwantedGiftsListCnt = controller.unwantedGiftsList.count
        let unwantedGiftsListCntRes = unwantedGiftsListCnt == 0 ? 1 : unwantedGiftsListCnt
        
        return (
            desiredGiftsListCntRes,
            unwantedGiftsListCntRes
        )
    }
    
    func handleRefresh() {
        store.dispatch(getGiftsListsRequestAction())
    }
    
    func desiredSectionLogin(indexPath: IndexPath) -> UITableViewCell {
        // Cells
        // Here I am not using dequeueReusableCell because I am not using standard cell
        // deletion. If I use dequeueReusableCell, the cell will remain in memory.
        let giftCell = CellWithTrash()
        
        let emptyCell = EmptyCell()
        emptyCell.titleItem = NSLocalizedString(
            "Пусто",
            comment: "Пусто"
        )
        
        // Data
        let desiredGiftsCont = controller.desiredGiftsList.count
        let desiredGiftsData = desiredGiftsCont > 0 ? controller.desiredGiftsList[indexPath.row] : nil
        
        // Configure
        guard
            let desiredGiftsDataUnwrap = desiredGiftsData,
            desiredGiftsCont > 0
        else {
            return emptyCell
        }
        
        giftCell.titleItem = desiredGiftsDataUnwrap.title
        giftCell.funcWhenIconPressed = { [unowned self] in
            self.handleDeleteDesiredGift(giftId: desiredGiftsData!.wish_id)
        }
        
        return giftCell
    }
    
    func unwantedSectionLogic(indexPath: IndexPath) -> UITableViewCell {
        // Cells
        // Here I am not using dequeueReusableCell because I am not using standard cell
        // deletion. If I use dequeueReusableCell, the cell will remain in memory.
        let giftCell = CellWithTrash()
        
        let emptyCell = EmptyCell()
        emptyCell.titleItem = NSLocalizedString(
            "Пусто",
            comment: "Пусто"
        )
        
        let unwantedGiftsCont = controller.unwantedGiftsList.count
        let unwantedGiftsData = unwantedGiftsCont > 0 ? controller.unwantedGiftsList[indexPath.row] : nil
        
        guard
            let unwantedGiftsDataUnwrap = unwantedGiftsData,
            unwantedGiftsCont > 0
        else {
            return emptyCell
        }
        
        giftCell.titleItem = unwantedGiftsDataUnwrap.title
        giftCell.funcWhenIconPressed = { [unowned self] in
            self.handleDeleteUnwantedGift(giftId: unwantedGiftsData!.stop_id)
        }
        
        return giftCell
    }
    
    private func handleDeleteDesiredGift(giftId: Int) {
        store.dispatch(deletionGiftRequestAction(
            giftType: .desired,
            giftId: giftId,
            cb: { [unowned self] in
                self.handleRefresh()
            })
        )
    }
    
    private func handleDeleteUnwantedGift(giftId: Int) {
        store.dispatch(deletionGiftRequestAction(
            giftType: .unwanted,
            giftId: giftId,
            cb: { [unowned self] in
                self.handleRefresh()
            })
        )
    }
}
