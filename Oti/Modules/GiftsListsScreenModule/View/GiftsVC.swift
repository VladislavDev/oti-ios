//
//  CheckTokenVC.swift
//  Oti
//
//  Created by Vlad on 8/7/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class GiftsVC: UIViewController, GiftsVCProtocol, StoreSubscriber {
    
    // MARK: - Presenter
    var presenter: GiftsPresenterProtocol?
    
    // MARK: - Gifts lists
    var desiredGiftsList: [DesiredGiftItem] = []
    var unwantedGiftsList: [UnwantedGiftItem] = []
    
    // MARK: - Create UI elements
    private var additionToNavBar: AdditionToNavBar?
    
    private var smallTitleLabel = LabelView(
        title: NSLocalizedString(
            "Эти пожелания будут отображаться вашей паре в событиях",
            comment: "Эти пожелания будут отображаться вашей паре в событиях"
        ),
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .regular,
        aligment: .center,
        numberOfLines: 0
    )
    
    var tableView: UITableView = {
        let t = UITableView()
        
        t.translatesAutoresizingMaskIntoConstraints = false
        t.backgroundColor = .clear
        
        return t
    }()

    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        store.subscribe(self) { subscription in
            subscription.select { state in state.giftsListsState }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupSelfView()
        setupAdditionToNavBar()
        setupSmallTitleLabel()
        setupTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK: - ReSwift
    func newState(state: GiftsListsState) {
        desiredGiftsList = state.desiredGifstList
        unwantedGiftsList = state.unwantedGifstLists
        
        if state.isSuccessGetGiftsListsAction {
            tableView.reloadData()
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        
        self.tabBarItem = CustomTabBarUtils.createTabBarItem(
            imageNamed: "giftNormal",
            tag: 2
        )
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: NSLocalizedString(
                "Подарки",
                comment: "Подарки"
            )
        )
    }
    
    private func setupSmallTitleLabel() {
        self.view.addSubview(smallTitleLabel)
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            smallTitleLabel.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor
            ),
            smallTitleLabel.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor,
                constant: 36
            ),
            smallTitleLabel.widthAnchor.constraint(
                lessThanOrEqualToConstant: 296
            )
        ])
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.clipsToBounds = false
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(
                equalTo: smallTitleLabel.bottomAnchor,
                constant: 25
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            )
        ])
    }
}

// MARK: - Table settings
extension GiftsVC: UITableViewDelegate {
    // Spacing between cell and section label
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 23
    }
    
    // Spacing between sections
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 23
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // Height cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}

extension GiftsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = SectionHeader()
        
        switch section {
        case 0:
            header.titleLabel.text = NSLocalizedString(
                "ЖЕЛАЕМЫЕ ПОДАРКИ",
                comment: "ЖЕЛАЕМЫЕ ПОДАРКИ"
            )
            header.withButton = true
            header.funcWhenPressed = { [unowned self] in
                self.present(
                    ModuleBuilder.createAddGiftModule(giftType: .desired),
                    animated: true,
                    completion: nil
                )
            }
        case 1:
            header.titleLabel.text = NSLocalizedString(
                "НЕЖЕЛАТЕЛЬНЫЕ ПОДАРКИ",
                comment: "НЕЖЕЛАТЕЛЬНЫЕ ПОДАРКИ"
            )
            header.withButton = true
            header.funcWhenPressed = { [unowned self] in
                self.present(
                    ModuleBuilder.createAddGiftModule(giftType: .unwanted),
                    animated: true,
                    completion: nil
                )
            }
        default: break
        }
        
        return header
    }

    func numberOfSections(in tableView: UITableView) -> Int { 2 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let presenter = presenter else { return 0 }
        
        // Set default 1 cell in each section
        let counters = presenter.getGiftsCnt()
        
        switch section {
        case 0: return counters.desiredGitfsCnt
        case 1: return counters.unwantedGiftsCnt
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let presenter = presenter else { return UITableViewCell() }
        
        switch indexPath.section {
        case 0: return presenter.desiredSectionLogin(indexPath: indexPath)
        case 1: return presenter.unwantedSectionLogic(indexPath: indexPath)
        default: return UITableViewCell()
        }
    }
}
