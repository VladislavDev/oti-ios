//
//  EventScreenVC.swift
//  Oti
//
//  Created by Vlad on 9/18/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

struct OpenedEventSubState {
    var openedEventState: OpenedEventState
    var pairState: PairState
    var pairGiftsState: PairGiftsListsState
    
    init(state: AppState) {
        self.openedEventState = state.openedEventState
        self.pairState = state.pairState
        self.pairGiftsState = state.pairGiftsListsState
    }
}

class EventScreenVC: UIViewController, StoreSubscriber, EventScreenVCProtocol {
    
    //MARK: - State
    var openedEventData: OpenedEvent?
    var pairResolveRequest: PSGroupsDatas?
    var pairDesiredGifts: [DesiredGiftItem] = []
    var pairUnwantedGifts: [UnwantedGiftItem] = []
    
    // MARK: - Presenter
    var presenter: EventScreenPresenterProtocol?
    
    // MARK: - Create UI elements
    var scrollView: UIScrollView = {
        let s = UIScrollView()
        
        s.translatesAutoresizingMaskIntoConstraints = false
        s.clipsToBounds = false
        s.showsHorizontalScrollIndicator = false
        s.showsVerticalScrollIndicator = false
        
        return s
    }()
    
    private var activityView: UIView = {
        let v = UIView()

        v.translatesAutoresizingMaskIntoConstraints = false
        v.clipsToBounds = false

        return v
    }()
    
    var additionToNavBar: AdditionToNavBar?
    var titleLabel = LabelView(
        title: "",
        color: Colors.black,
        fontSize: FontSizes.subhead,
        weight: .bold,
        aligment: .left,
        numberOfLines: 0
    )
    
    var seeAllLabel = LabelView(
        title: "",
        color: Colors.black,
        fontSize: FontSizes.body,
        weight: .regular,
        aligment: .left,
        numberOfLines: 0
    )
    
    var imgView: UIImageView = {
        let i = UIImageView()
        
        i.translatesAutoresizingMaskIntoConstraints = false
        i.contentMode = .scaleAspectFill
        
        Effects.addSmallShadow(layer: i.layer)
        
        return i
    }()
    
    private let addPairCard = AddYourPairCard()
    private var pairCard: PairCard?
    private var tabBarControll = UITabBar()
    
    var vStackView: UIStackView = {
        let s = UIStackView()
        
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .vertical
        s.distribution = .fillProportionally
        
        return s
    }()
    
    
    private let tabBarItemOne = AnotherComponents.createTabBarItem(
        title: NSLocalizedString("Желаемые", comment: "Желаемые подарки"),
        tag: 0
    )
    
    private let tabBarItemTwo = AnotherComponents.createTabBarItem(
        title: NSLocalizedString("Нежелательные", comment: "Нежелательные подарки"),
        tag: 1
    )

    // MARK: - VC lifecycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        store.subscribe(self) { subscription in
            subscription.select(OpenedEventSubState.init)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSelfView()
        setupBarButtonItem()
        setupAdditionToNavBar()
        setupScrollView()
        setupActivityView()
        setupTitleLabel()
        setupSeeAllLabel()
        setupImgView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    // MARK: - ReSwift
    func newState(state: OpenedEventSubState) {
        guard let presenter = presenter else { return }
        guard let openedEvent = state.openedEventState.openedEvent else { return }
        
        openedEventData = openedEvent
        
        presenter.updateAdditionToNavBar()
        presenter.updateTitleLabel()
        presenter.updateSeeAllLabel()
        presenter.updateImg()
        
        // Check pair existing and setup data
        if let resolveRequestUnwrap = state.pairState.pairList.resolveRequest,
            resolveRequestUnwrap.count > 0 {

            pairResolveRequest = resolveRequestUnwrap[0]

            if let pairName = presenter.checkAndGetPairName() {
                if !elementIsContainsSubview(element: pairCard) {
                    setupPairCard(pairName: pairName.capitalizingFirstLetter())
                }
                
                if !elementIsContainsSubview(element: tabBarControll) {
                    setupTabBarControll()
                }
                
                if !elementIsContainsSubview(element: vStackView) {
                    setupVStackView()
                }
                
                pairDesiredGifts = state.pairGiftsState.desiredPairGifstList
                pairUnwantedGifts = state.pairGiftsState.unwantedPairGifstLists
                
                if scrollView.refreshControl == nil {
                    presenter.initRefreshControll()
                }
                
                tabBarControll.selectedItem = tabBarItemOne
                presenter.clearArrangedSubviews()
                presenter.addArrangedSubviewIntoVStack(giftType: .desired)
            }
        } else {
            if !elementIsContainsSubview(element: addPairCard) {
                setupAddYourPairCard()
            }
        }
    }
    
    // MARK: - UI elements ancors
    private func setupSelfView() {
        self.view.backgroundColor = .white
        
        let margin: CGFloat = 32
        
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: margin,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: margin
        )
        
        self.title = NSLocalizedString("Событие", comment: "Событие")
    }
    
    private func setupBarButtonItem() {
        let editBtn = UIBarButtonItem(
            barButtonSystemItem: .edit,
            target: self,
            action: #selector(goToEditEventScreen)
        )
        
        navigationItem.rightBarButtonItem = editBtn
    }
    
    @objc private func goToEditEventScreen() {
        self.navigationController?.pushViewController(
            ModuleBuilder.createEditEventScreenModule(),
            animated: true
        )
    }
    
    private func setupAdditionToNavBar() {
        additionToNavBar = AdditionToNavBar(
            parentViewForGetConstraint: self.view,
            title: ""
        )
    }
    
    private func setupScrollView() {
        self.view.addSubview(scrollView)
        self.view.sendSubviewToBack(scrollView)
        
        guard let additionToNavBar = additionToNavBar else { return }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(
                equalTo: additionToNavBar.bottomAnchor
            ),
            scrollView.bottomAnchor.constraint(
                equalTo: self.view.safeAreaLayoutGuide.bottomAnchor
            ),
            scrollView.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            scrollView.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
    
    private func setupActivityView() {
        scrollView.addSubview(activityView)
        
        NSLayoutConstraint.activate([
            activityView.topAnchor.constraint(
                equalTo: scrollView.topAnchor
            ),
            activityView.bottomAnchor.constraint(
                equalTo: scrollView.bottomAnchor
            ),
            activityView.widthAnchor.constraint(
                equalTo: scrollView.widthAnchor
            )
        ])
    }
    
    private func setupTitleLabel() {
        activityView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.heightAnchor.constraint(
                greaterThanOrEqualToConstant: 60
            ),
            titleLabel.topAnchor.constraint(
                equalTo: activityView.topAnchor,
                constant: 42
            ),
            titleLabel.leadingAnchor.constraint(
                equalTo: activityView.leadingAnchor
            ),
            titleLabel.widthAnchor.constraint(
                equalToConstant: 214
            ),
        ])
    }
    
    private func setupSeeAllLabel() {
        activityView.addSubview(seeAllLabel)
        
        NSLayoutConstraint.activate([
            seeAllLabel.topAnchor.constraint(
                equalTo: titleLabel.bottomAnchor,
                constant: 9
            ),
            seeAllLabel.leadingAnchor.constraint(
                equalTo: activityView.leadingAnchor
            ),
        ])
    }
    
    private func setupImgView() {
        activityView.addSubview(imgView)
        
        NSLayoutConstraint.activate([
            imgView.heightAnchor.constraint(
                equalToConstant: 104
            ),
            imgView.topAnchor.constraint(
                equalTo: activityView.topAnchor,
                constant: 30
            ),
            imgView.widthAnchor.constraint(
                equalToConstant: 90
            ),
            imgView.trailingAnchor.constraint(
                equalTo: activityView.trailingAnchor
            )
        ])
    }
    
    private func setupPairCard(pairName: String) {
        pairCard = PairCard(pairName: pairName)
        
        guard let pairCard = pairCard else { return }
        
        activityView.addSubview(pairCard)
        
        NSLayoutConstraint.activate([
            pairCard.topAnchor.constraint(
                equalTo: seeAllLabel.bottomAnchor,
                constant: 35
            ),
            pairCard.heightAnchor.constraint(
                equalToConstant: 88
            ),
            pairCard.leadingAnchor.constraint(
                equalTo: activityView.leadingAnchor
            ),
            pairCard.trailingAnchor.constraint(
                equalTo: activityView.trailingAnchor
            ),
        ])
    }
    
    private func setupAddYourPairCard() {
        activityView.addSubview(addPairCard)
        
        addPairCard.button.button.funcWhenPressed = { [unowned self] in
            self.present(ModuleBuilder.createAddPairModule(), animated: true, completion: nil)
        }
        
        NSLayoutConstraint.activate([
            addPairCard.topAnchor.constraint(
                equalTo: seeAllLabel.bottomAnchor,
                constant: 70
            ),
            addPairCard.bottomAnchor.constraint(
                equalTo: activityView.bottomAnchor
            ),
            addPairCard.leadingAnchor.constraint(
                equalTo: activityView.leadingAnchor
            ),
            addPairCard.trailingAnchor.constraint(
                equalTo: activityView.trailingAnchor
            )
        ])
    }
    
    private func setupTabBarControll() {
        tabBarControll.translatesAutoresizingMaskIntoConstraints = false
        tabBarControll.delegate = self
        
        tabBarControll.unselectedItemTintColor = Colors.grayHard
        tabBarControll.tintColor = Colors.blue
        tabBarControll.barStyle = .black
        
        tabBarControll.selectionIndicatorImage = UIImage(named: "line")
        tabBarControll.clipsToBounds = false
        
        activityView.addSubview(tabBarControll)
        
        tabBarControll.setItems([tabBarItemOne, tabBarItemTwo], animated: true)
        tabBarControll.selectedItem = tabBarItemOne
        
        guard let pairCard = pairCard else { return }
        
        NSLayoutConstraint.activate([
            tabBarControll.topAnchor.constraint(
                equalTo: pairCard.bottomAnchor,
                constant: 15
            ),
            
            tabBarControll.leadingAnchor.constraint(
                equalTo: activityView.leadingAnchor
            ),
            tabBarControll.trailingAnchor.constraint(
                equalTo: activityView.trailingAnchor
            ),
        ])
    }
    
    private func setupVStackView() {
        activityView.addSubview(vStackView)
        
        NSLayoutConstraint.activate([
            vStackView.topAnchor.constraint(equalTo: tabBarControll.bottomAnchor),
            vStackView.bottomAnchor.constraint(equalTo: activityView.bottomAnchor),
            vStackView.trailingAnchor.constraint(equalTo: activityView.trailingAnchor),
            vStackView.leadingAnchor.constraint(equalTo: activityView.leadingAnchor),
        ])
    }
    
    private func elementIsContainsSubview(element: UIView?) -> Bool {
        guard let element = element else { return false }
        return self.activityView.subviews.contains(element)
    }
}

extension EventScreenVC: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        // Clear arranged subviews
        presenter?.clearArrangedSubviews()
        
        // Add arranged subviews
        switch item.tag {
        case 0: presenter?.addArrangedSubviewIntoVStack(giftType: .desired)
        case 1: presenter?.addArrangedSubviewIntoVStack(giftType: .unwanted)
        default: break
        }
    }
}
