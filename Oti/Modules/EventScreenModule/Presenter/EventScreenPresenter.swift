//
//  EventScreenPresenter.swift
//  Oti
//
//  Created by Vlad on 9/21/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol EventScreenVCProtocol: class, AdditionToNavBarProtocol {
    var openedEventData: OpenedEvent? { get set }
    var pairResolveRequest: PSGroupsDatas? { get set }
    
    var pairDesiredGifts: [DesiredGiftItem] { get set }
    var pairUnwantedGifts: [UnwantedGiftItem] { get set }
    
    var scrollView: UIScrollView { get set }
    var vStackView: UIStackView { get set }
    var titleLabel: LabelView { get set }
    var seeAllLabel: LabelView { get set }
    var imgView: UIImageView { get set }
}

protocol EventScreenPresenterProtocol {
    init(controller: EventScreenVCProtocol)
    
    func updateAdditionToNavBar()
    func initRefreshControll()
    func updateTitleLabel()
    func updateSeeAllLabel()
    func updateImg()
    func checkAndGetPairName() -> String?
    func clearArrangedSubviews()
    func addArrangedSubviewIntoVStack(giftType: GiftType)
}

final class EventScreenPresenter: EventScreenPresenterProtocol {
    
    // MARK: - Resolve Requests
    var pairResolveRequest: PSGroupsDatas?
    
    // MARK: - Init
    private unowned var controller: EventScreenVCProtocol
    
    init(controller: EventScreenVCProtocol) {
        self.controller = controller
    }
    
    // MARK: - Update methods
    func updateAdditionToNavBar() {
        guard let additionToNavBar = controller.additionToNavBar else { return }
        guard let openedEventData = controller.openedEventData else { return }
        
        additionToNavBar.titleLabel.text = "\(openedEventData.day) \(openedEventData.month)"
    }
    
    func updateTitleLabel() {
        guard let openedEventData = controller.openedEventData else { return }
        
        controller.titleLabel.text = openedEventData.rawEventValues?.title
    }
    
    func updateSeeAllLabel() {
        guard let openedEventData = controller.openedEventData else { return }
        
        if openedEventData.rawEventValues?.seeEveryoneInThePair == "Y" {
            controller.seeAllLabel.text = NSLocalizedString(
                "Видят: вы, партнер",
                comment: "Видят: вы, партнер"
            )
        } else {
            controller.seeAllLabel.text = NSLocalizedString(
                "Видят: вы",
                comment: "Видят: вы"
            )
        }
    }
    
    func updateImg() {
        guard let openedEventData = controller.openedEventData else { return }
        guard let imgName = openedEventData.rawEventValues?.iconPath else { return }
        
        if let url = URL(string: "\(URLs.api)/static/images_pack2/\(imgName)") {
            controller.imgView.sd_setImage(
                with: url,
                placeholderImage: UIImage(named: "defaultEventImg")
            )
        }
    }
    
    func checkAndGetPairName() -> String? {
        if
            let pairResolveRequest = controller.pairResolveRequest,
            let pairEmail = PairHelpers.getPairEmail(resolveReq: pairResolveRequest),
            let pairNameUnwrap = PairHelpers.getPairName(email: pairEmail) {

            return pairNameUnwrap
        } else {
            return nil
        }
    }
    
    func initRefreshControll() {
        controller.scrollView.refreshControl = UIRefreshControl()
        controller.scrollView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc func refresh() {
        store.dispatch(getPairGiftsListsRequestAction { [unowned self] in
            if let refreshControl = self.controller.scrollView.refreshControl {
                refreshControl.endRefreshing()
            }
        })
    }
    
    func clearArrangedSubviews() {
        let arrangedSubviews = controller.vStackView.arrangedSubviews
        
        if arrangedSubviews.count > 0 {
            for item in arrangedSubviews {
                controller.vStackView.removeArrangedSubview(item)
                item.removeFromSuperview()
            }
        }
    }
    
    func addArrangedSubviewIntoVStack(giftType: GiftType) {
        switch giftType {
        case .desired:
            if controller.pairDesiredGifts.count > 0 {
                for desiredGiftDataItem in controller.pairDesiredGifts {
                    controller.vStackView.addArrangedSubview(PairGiftItem(titleItem: desiredGiftDataItem.title))
                }
            } else {
                let emptyItem = PairGiftItem(
                    titleItem: NSLocalizedString(
                        "Пусто",
                        comment: "Пусто"
                    ),
                    type: .empty
                )
                
                controller.vStackView.addArrangedSubview(emptyItem)
            }
        
        case .unwanted:
            if controller.pairUnwantedGifts.count > 0 {
                for unwantedGiftDataItem in controller.pairUnwantedGifts {
                    controller.vStackView.addArrangedSubview(PairGiftItem(titleItem: unwantedGiftDataItem.title))
                }
            } else {
                let emptyItem = PairGiftItem(
                    titleItem: NSLocalizedString(
                        "Пусто",
                        comment: "Пусто"
                    ),
                    type: .empty
                )
                
                controller.vStackView.addArrangedSubview(emptyItem)
            }
        }
    }
}
