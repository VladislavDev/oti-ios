//
//  TestVC.swift
//  Oti
//
//  Created by Vlad on 9/24/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class TestVC: UIViewController {
    
    private var element: CustomDatePicker?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.view.directionalLayoutMargins = NSDirectionalEdgeInsets(
            top: self.view.directionalLayoutMargins.top,
            leading: 32,
            bottom: self.view.directionalLayoutMargins.bottom,
            trailing: 32
        )
        
        setupElementView()
    }
    
    private func setupElementView() {
        element = CustomDatePicker(title: "DATE", date: "2020-09-24")
        
        guard let element = element else { return }
        
        self.view.addSubview(element)
        
        NSLayoutConstraint.activate([
            element.topAnchor.constraint(
                equalTo: self.view.topAnchor,
                constant: 43
            ),
            element.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor
            ),
            element.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor
            ),
        ])
    }
}
