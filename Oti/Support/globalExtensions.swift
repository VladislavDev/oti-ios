//
//  globalExtensions.swift
//  Oti
//
//  Created by Vlad on 8/29/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

extension UIViewController {
    func dismissKey() {
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard)
        )
        
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

public extension UIControl {
    @objc static var debounceDelay: Double = 0.5
    @objc func debounce(delay: Double = UIControl.debounceDelay, siblings: [UIControl] = []) {
        let buttons = [self] + siblings
        
        buttons.forEach { $0.isEnabled = false }
        
        let deadline = DispatchTime.now() + delay
        
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            buttons.forEach { $0.isEnabled = true }
        }
     }
}

extension String {
    // Capitalize first letter
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    // Check emoji
    func containsOnlyEmojis() -> Bool {
        if count == 0 {
            return false
        }
        
        for character in self {
            if !character.isEmoji {
                return false
            }
        }
        return true
    }

    func containsEmoji() -> Bool {
        for character in self {
            if character.isEmoji {
                return true
            }
        }
        return false
    }
}

extension Character {
    var isEmoji: Bool {
        guard let scalar = unicodeScalars.first else { return false }
        return scalar.properties.isEmoji && (scalar.value > 0x238C || unicodeScalars.count > 1)
    }
}
