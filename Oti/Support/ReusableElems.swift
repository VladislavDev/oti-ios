//
//  ReusableElems.swift
//  Oti
//
//  Created by Vlad on 9/25/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

enum GiftType {
    case desired
    case unwanted
}

// How many items in one row
enum OpenedEventSectionKind: Int, CaseIterable {
    case imgs
    var columnCount: Int {
        switch self {
        case .imgs: return 3
        }
    }
}

enum NotifyFor: String, CaseIterable {
    case day, week
    
    static func getIdx(str: String) -> Int {
        for (idx, param) in self.allCases.enumerated() {
            if str == param.rawValue {
                return idx
            }
        }
        
        return 0
    }
}

enum RepetitionCycle: String, CaseIterable {
    case year, month, off
    
    static func getIdx(str: String) -> Int {
        for (idx, param) in self.allCases.enumerated() {
            if str == param.rawValue {
                return idx
            }
        }
        
        return 0
    }
}

struct StateForOpenedEventVC {
    var iconsPackNames: IconsPackState
    var eventsListsState: EventsListsState
    var pairState: PairState
    var openedEventState: OpenedEventState

    init(state: AppState) {
        self.iconsPackNames = state.iconsPackNames
        self.pairState = state.pairState
        self.eventsListsState = state.eventsListsState
        self.openedEventState = state.openedEventState
    }
}

struct IconPackResponse: Decodable {
    var status: Int
    var message: [String]
}
