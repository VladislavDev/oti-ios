//
//  ModuleBuilder.swift
//  Oti
//
//  Created by Vlad on 8/20/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit
import ReSwift

class ModuleBuilder {
    
    // MARK: - Auth modules
    static func checkTokenAndRedirect() -> UIViewController {
        if let _ = TokenService.shared.getAuthUserData().token {
            return createAppAfterLogin()
        }
        
        return CustomNavController(rootViewController: createLoginModule())
    }
    
    static func createLoginModule() -> UIViewController {
        let vc = LoginVC()
        let presenter = LoginPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createCreateAccModule() -> UIViewController {
        let vc = CreateAccVC()
        let presenter = CreateAccPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createForgotPassModule() -> UIViewController {
        let vc = ForgotPassVC()
        let presenter = ForgotPassPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createCompletedVCModule() -> UIViewController {
        let vc = CompletedRegVC()
        let presenter = CompletedRegPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    // MARK: - Events modules
    static func createEventListModule() -> UIViewController {
        let vc = EventsListVC()
        let presenter = EventListPresenter(controller: vc)
        
        vc.presenter = presenter
        
        vc.loadViewIfNeeded()
        
        return vc
    }
    
    static func createCreateNewEventModule() -> UIViewController {
        let vc = CreateNewEventVC()
        let presenter = CreateNewEventPresenter(controller: vc)
        
        vc.presenter = presenter

        return vc
    }
    
    static func createEventScreenModule() -> UIViewController {
        let vc = EventScreenVC()
        let presenter = EventScreenPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createEditEventScreenModule() -> UIViewController {
        let vc = ChangeEventVC()
        let presenter = ChangeEventPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    // MARK: - Gifts modules
    static func createGiftsListModule() -> UIViewController {
        let vc = GiftsVC()
        let presenter = GiftsPresenter(controller: vc)
        
        vc.presenter = presenter
        vc.loadViewIfNeeded()
        
        return vc
    }
    
    // MARK: - Account modules
    static func createAccSettingsModule() -> UIViewController {
        let vc = AccountSettingsVC()
        let presenter = AccSettingsPresenter(controller: vc)
        
        vc.presenter = presenter
        vc.loadViewIfNeeded()
        
        return vc
    }
    
    static func createDataSettingsModule() -> UIViewController {
        let vc = DataSettingsVC()
        
        return vc
    }
    
    static func createPairSettingsModule() -> UIViewController {
        let vc = PairSettingsVC()
        let presenter = PairSettingsPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    // MARK: - Modals
    static func createEmailUSModule() -> UIViewController {
        let vc = EmailUsModal()
        let presenter = EmailUsPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createChangePassModule() -> UIViewController {
        let vc = ChangePassModal()
        let presenter = ChangePassPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createAddPairModule() -> UIViewController {
        let vc = AddPairModal()
        let presenter = AddPairPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createAddGiftModule(giftType: GiftType) -> UIViewController {
        let vc = AddGiftModal()
        vc.giftType = giftType
        let presenter = AddGiftPresenter(controller: vc)
        
        vc.presenter = presenter
        
        return vc
    }
    
    static func createPairNotifyModule() -> UIViewController {
        let vc = PairNotifyModalVC()
        
        return vc
    }
    
    // MARK: - TabBar controller
    static func createAppAfterLogin() -> UIViewController {
        // Show rating controller
        RatingService.showRatingController()
        
        // Get pair lists
        store.dispatch(getPairListRequestAction() { resolveRequests in
            // Get events list
            store.dispatch(getEventsListRequestAction(
                year: WorkWithDate.getFormettedDate(format: .year),
                month: WorkWithDate.getFormettedDate(format: .monthCount))
            )
            
            if let resolveRequests = resolveRequests, resolveRequests.count > 0 {
                // Get pair gifts lists
                store.dispatch(getPairGiftsListsRequestAction())
            }
        })
        
        // Get self gifts lists
        store.dispatch(getGiftsListsRequestAction())
        
        // Get icons pack names
        store.dispatch(getIconsPackRequestAction())
        
        // Setup tab bar and screens
        let tabBarController = CustomTabBarController()
        
        let eventsListsWithNavController = CustomNavController(rootViewController: createEventListModule())
        
        let giftsWithNavController = CustomNavController(rootViewController: createGiftsListModule())
        
        let accSettingsWithNacController =
            CustomNavController(rootViewController: createAccSettingsModule())
        
        tabBarController.viewControllers = [
            eventsListsWithNavController,
            giftsWithNavController,
            accSettingsWithNacController
        ]
        
        return tabBarController
    }
}
