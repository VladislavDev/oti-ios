//
//  ReusableProtocols.swift
//  Oti
//
//  Created by Vlad on 9/27/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol NavigationControllerProtocol {
    var navigationController: UINavigationController? { get }
}

protocol TableViewProtocol {
    var tableView: UITableView { get set }
}

protocol CollectionViewProtocol {
    var collectionView: UICollectionView? { get set }
}

protocol RefreshControlProtocol {
    var refreshControl: UIRefreshControl { get set }
}

protocol AdditionToNavBarProtocol {
    var additionToNavBar: AdditionToNavBar? { get set }
}

protocol EmailFieldProtocol {
    var emailField: WrappedTextField { get set }
}

protocol PassFieldProtocol {
    var passField: WrappedTextField { get set }
}

protocol DismissModalProtocol {
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
}

protocol TitleViewProtocol {
    var titleView: WrappedTextField { get set }
}

protocol NotifyForViewProtocol {
    var notifyForView: SelectField { get set }
}

protocol DatePickerViewProtocol {
    var datePickerView: CustomDatePicker { get set }
}

protocol RepetitionCycleViewProtocol {
    var repetitionCycleView: SelectField { get set }
}

protocol SwitchViewProtocol {
    var switchView: CustomSwitch { get set }
}

protocol IconsPackNamesListProtocol {
    var iconsPackNamesList: [String] { get set }
}
