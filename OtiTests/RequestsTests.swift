//
//  RequestsTests.swift
//  OtiTests
//
//  Created by Vlad on 8/30/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import XCTest
@testable import Oti

class RequestsTests: XCTestCase {
    // commented out so as not to load the mail server
//    func testEmailUs() {
//        let expectation = XCTestExpectation(description: "Success")
//
//        EmailUsRequests.emailUsRequest(
//            from: "studio_kseven@mail.ru",
//            subject: "Test scope. Subject",
//            text: "Text message",
//            cbWhenStartRequest: nil,
//            cbWhenErrorRequest: { XCTFail("EmailUs fail") },
//            cbWhenSuccessRequest: {
//                expectation.fulfill()
//            }
//        )
//
//        wait(for: [expectation], timeout: 10.0)
//    }
}
