//
//  ValidationsTests.swift
//  OtiTests
//
//  Created by Vlad on 8/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import XCTest
@testable import Oti

class ValidationsTests: XCTestCase {
    func testIsEmptyValue() {
        // body
        let resIsEmpty = EmailValidations().validate(text: "")
        
        // assert
        XCTAssertEqual(resIsEmpty.res, false)
    }
    
    func testEmailValue() {
        // body
        let resp1 = EmailValidations().validate(text: "ufuijwa")
        let resp2 = EmailValidations().validate(text: "lala@")
        let resp3 = EmailValidations().validate(text: "lala@mai")
        let resp4 = EmailValidations().validate(text: "lala@mail.ru")
        let resp5 = EmailValidations().validate(text: "lala@mail.r")
        let resp6 = EmailValidations().validate(text: "lala@gmail.com")
        let resp7 = EmailValidations().validate(text: "v.lalalala@yandex.ru")
        let resp8 = EmailValidations().validate(text: "v.la.laala@yandex.ru")
        
        let resp9 = EmailValidations().validate(text: "matmeaova.rrra2020@yandex.ru")
        let resp10 = EmailValidations().validate(text: "tema_tip@mail.ru")
        let resp11 = EmailValidations().validate(text: "irisha.kuznetsova.0101@mail.ru")
        let resp12 = EmailValidations().validate(text: "alia252001@mail.ru")
        
        // assert
        XCTAssertEqual(resp1.res, false)
        XCTAssertEqual(resp2.res, false)
        XCTAssertEqual(resp3.res, false)
        XCTAssertEqual(resp4.res, true)
        XCTAssertEqual(resp5.res, false)
        XCTAssertEqual(resp6.res, true)
        XCTAssertEqual(resp7.res, true)
        XCTAssertEqual(resp8.res, true)
        
        XCTAssertEqual(resp9.res, true)
        XCTAssertEqual(resp10.res, true)
        XCTAssertEqual(resp11.res, true)
        XCTAssertEqual(resp12.res, true)
    }
    
    func testTitlesValue() {
        // body
        let resp1 = TextValidations().validate(text: "one")
        let resp2 = TextValidations().validate(text: "onetw")
        let resp3 = TextValidations().validate(text: "02rrw")
        let resp4 = TextValidations().validate(text: "+fewr")
        let resp5 = TextValidations().validate(text: "fe+wr")
        let resp6 = TextValidations().validate(text: "fewr+")
        let resp7 = TextValidations().validate(text: "0fr*45")
        let resp8 = TextValidations().validate(text: "0fr45")
        let resp9 = TextValidations().validate(text: "0f_r45")
        let resp10 = TextValidations().validate(text: "0f,r45")
        let resp11 = TextValidations().validate(text: "aAvr5r")
        let resp12 = TextValidations().validate(text: "aAvr5rtrc")
        let resp13 = TextValidations().validate(text: "aAvr5rtrc5")
        let resp14 = TextValidations().validate(text: "i")
        let resp15 = TextValidations().validate(text: "New year 💎")
        let resp16 = TextValidations().validate(text: "P6p?A'D(N&rC@'F[x$Rr~xte-~V.d2x-fujy=ewG^/")
        let resp17 = TextValidations().validate(text: "P6p?A'D(N&rC@'F[x$Rr~xte-~V.d2x-fu")
        
        // assert
        XCTAssertEqual(resp1.res, true)
        XCTAssertEqual(resp2.res, true)
        XCTAssertEqual(resp3.res, true)
        XCTAssertEqual(resp4.res, true)
        XCTAssertEqual(resp5.res, true)
        XCTAssertEqual(resp6.res, true)
        XCTAssertEqual(resp7.res, true)
        XCTAssertEqual(resp8.res, true)
        XCTAssertEqual(resp9.res, true)
        XCTAssertEqual(resp10.res, true)
        XCTAssertEqual(resp11.res, true)
        XCTAssertEqual(resp12.res, true)
        XCTAssertEqual(resp13.res, true)
        XCTAssertEqual(resp14.res, false)
        XCTAssertEqual(resp15.res, false)
        XCTAssertEqual(resp16.res, false)
        XCTAssertEqual(resp17.res, true)
    }
    
    func testPassValue() {
        // body
        let resp1 = PasswordValidations().validate(text: "one")
        let resp2 = PasswordValidations().validate(text: "onetw")
        let resp3 = PasswordValidations().validate(text: "02rrw")
        let resp4 = PasswordValidations().validate(text: "+fewr")
        let resp5 = PasswordValidations().validate(text: "fe+wr")
        let resp6 = PasswordValidations().validate(text: "fewr+")
        let resp7 = PasswordValidations().validate(text: "0fr*45")
        let resp8 = PasswordValidations().validate(text: "0fr45")
        let resp9 = PasswordValidations().validate(text: "0f_r45")
        let resp10 = PasswordValidations().validate(text: "0f,r45")
        let resp11 = PasswordValidations().validate(text: "aAvr5r")
        let resp12 = PasswordValidations().validate(text: "aAvr5rtrc")
        let resp13 = PasswordValidations().validate(text: "aAvr5rtrc5")
        let resp14 = PasswordValidations().validate(text: "i")
        let resp15 = PasswordValidations().validate(text: "B*3DLp+M")
        let resp16 = PasswordValidations().validate(text: "~Vv{7CT#")
        
        // assert
        XCTAssertEqual(resp1.res, false)
        XCTAssertEqual(resp2.res, true)
        XCTAssertEqual(resp3.res, true)
        XCTAssertEqual(resp4.res, true)
        XCTAssertEqual(resp5.res, true)
        XCTAssertEqual(resp6.res, true)
        XCTAssertEqual(resp7.res, true)
        XCTAssertEqual(resp8.res, true)
        XCTAssertEqual(resp9.res, true)
        XCTAssertEqual(resp10.res, true)
        XCTAssertEqual(resp11.res, true)
        XCTAssertEqual(resp12.res, true)
        XCTAssertEqual(resp13.res, false)
        XCTAssertEqual(resp14.res, false)
        XCTAssertEqual(resp15.res, true)
        XCTAssertEqual(resp16.res, true)
    }
}
