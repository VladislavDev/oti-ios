//
//  MonthFilterLogicTests.swift
//  OtiTests
//
//  Created by Vlad on 8/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import XCTest
@testable import Oti

class MonthFilterLogicTests: XCTestCase {
    func testGetCurrentMonthIdx() {
        // prepare
        let march = "MARCH"
        let september = "September"
        let december = "december"
        let nonExistent = "NonExistent"
        
        // body
        let resMarch = WorkWithDate.getMonthIdx(month: march)
        let resSept = WorkWithDate.getMonthIdx(month: september)
        let resDec = WorkWithDate.getMonthIdx(month: december)
        let resNone = WorkWithDate.getMonthIdx(month: nonExistent)
        
        // asserts
        if let res = resMarch { XCTAssertEqual(res, 2) }
        else { XCTFail("resMarch is nil") }
        
        if let res = resSept { XCTAssertEqual(res, 8) }
        else { XCTFail("resSept is nil") }
        
        if let res = resDec { XCTAssertEqual(res, 11) }
        else { XCTFail("resDec is nil") }
        
        XCTAssertNil(resNone)
    }
    
    func testGetMonthOnDirection() {
        // body
        // direction next
        let resAfterJenuary = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 0,
            direction: .next
        )
        
        let resAfterMarch = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 2,
            direction: .next
        )
        
        let resAfterDecember = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 11,
            direction: .next
        )
        
        let resAfterBigNum = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 37,
            direction: .next
        )
        
        let resAfterSmallNum = MonthFilterLogic.getMonthOnDirection(
            monthIdx: -4,
            direction: .next
        )
        
        // direction prev
        let resBeforeJenuary = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 0,
            direction: .prev
        )
        
        let resBeforeMarch = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 2,
            direction: .prev
        )
        
        let resBeforeDecember = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 11,
            direction: .prev
        )
        
        let resBeforeBigNum = MonthFilterLogic.getMonthOnDirection(
            monthIdx: 44,
            direction: .prev
        )
        
        let resBeforeSmallNum = MonthFilterLogic.getMonthOnDirection(
            monthIdx: -4,
            direction: .prev
        )
        
        // asserts
        // direction next
        XCTAssertEqual(resAfterJenuary.capitalized, "February")
        XCTAssertEqual(resAfterMarch.capitalized, "April")
        XCTAssertEqual(resAfterDecember.capitalized, "January")
        XCTAssertEqual(resAfterBigNum.capitalized, "January")
        XCTAssertEqual(resAfterSmallNum.capitalized, "January")
        
        // direction prev
        XCTAssertEqual(resBeforeJenuary.capitalized, "December")
        XCTAssertEqual(resBeforeMarch.capitalized, "February")
        XCTAssertEqual(resBeforeDecember.capitalized, "November")
        XCTAssertEqual(resBeforeBigNum.capitalized, "December")
        XCTAssertEqual(resBeforeSmallNum.capitalized, "December")
    }
    
    func testGetNewYear() {
        // body
        // direction next
        let nextYearNormalNum = MonthFilterLogic.getNewYear(
            year: "2020",
            direction: .next
        )
        
        let nextYearBigNum = MonthFilterLogic.getNewYear(
            year: "3000",
            direction: .next
        )
        
        let nextYearSmallNum = MonthFilterLogic.getNewYear(
            year: "-4",
            direction: .next
        )
        
        // direction prev
        let prevYearNormalNum = MonthFilterLogic.getNewYear(
            year: "2020",
            direction: .prev
        )
        
        let prevYearBigNum = MonthFilterLogic.getNewYear(
            year: "3000",
            direction: .prev
        )
        
        let prevYearSmallNum = MonthFilterLogic.getNewYear(
            year: "-4",
            direction: .prev
        )
        
        // asserts
        // direction next
        XCTAssertEqual(nextYearNormalNum, "2021")
        XCTAssertEqual(nextYearBigNum, "3001")
        XCTAssertEqual(nextYearSmallNum, "-3")
        
        // direction prev
        XCTAssertEqual(prevYearNormalNum, "2019")
        XCTAssertEqual(prevYearBigNum, "2999")
        XCTAssertEqual(prevYearSmallNum, "-5")
    }
    
    func testGetYearOnDirection() {
        // body
        // direction next
        let afterMay2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "May",
            direction: .next
        )
        
        let afterDecember2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "December",
            direction: .next
        )
        
        let afterNone2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "None",
            direction: .next
        )
        
        let afterBigYear = MonthFilterLogic.getYearOnDirection(
            year: "3000",
            month: "None",
            direction: .next
        )
        
        // direction prev
        let beforeMay2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "May",
            direction: .prev
        )
        
        let beforeJanuary2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "January",
            direction: .prev
        )
        
        let beforeNone2020 = MonthFilterLogic.getYearOnDirection(
            year: "2020",
            month: "None",
            direction: .prev
        )
        
        let beforeBigYear = MonthFilterLogic.getYearOnDirection(
            year: "3000",
            month: "None",
            direction: .prev
        )
        
        // asserts
        // direction next
        XCTAssertEqual(afterMay2020, "2020")
        XCTAssertEqual(afterDecember2020, "2021")
        XCTAssertEqual(afterNone2020, "2020")
        XCTAssertEqual(afterBigYear, "3000")
        
        // direction prev
        XCTAssertEqual(beforeMay2020, "2020")
        XCTAssertEqual(beforeJanuary2020, "2019")
        XCTAssertEqual(beforeNone2020, "2020")
        XCTAssertEqual(beforeBigYear, "3000")
    }
}
