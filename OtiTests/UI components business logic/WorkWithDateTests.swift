//
//  WorkWithDateTests.swift
//  OtiTests
//
//  Created by Vlad on 8/19/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import XCTest
@testable import Oti

class WorkWithDateTests: XCTestCase {
    func testsRangeDate() {
        // prepare
        let minimumDate = WorkWithDate.rangeDate(minimum: true)
        let maximumDate = WorkWithDate.rangeDate(minimum: false)
        
        // body
        let minumumDateStr = WorkWithDate.getFormettedDate(format: .mdy, date: minimumDate)
        let maximumDateStr = WorkWithDate.getFormettedDate(format: .mdy, date: maximumDate)
        
        // asserts
        XCTAssertEqual(minumumDateStr, "12/01/2000")
        XCTAssertEqual(maximumDateStr, "01/01/2050")
    }
    
    func testСomparisonData() {
        // MARK: prepare
        let initialEventDateStr = "2020-09-11T21:00:00.000Z"
        let currentDateStr = "2020-09-12"
        
        // MARK: body
        // Parse date from backend iso string
        if let isoDateFromEvent = WorkWithDate.transformStrDateToDate(dateStr: initialEventDateStr) {
            // Transform iso date to yyy-MM-dd string
            let eventDateStr = WorkWithDate.getFormettedDate(
                format: .fullFromBackend,
                date: isoDateFromEvent
            )
            
            // MARK: asserts
            XCTAssertEqual(eventDateStr, currentDateStr)
        } else {
            XCTFail()
        }
    }
}
